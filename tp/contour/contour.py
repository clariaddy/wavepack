
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

def func(x,y):
    return y*np.sqrt(y)*x*np.sin(x)

shape = (128,128)

range = [0,2*np.pi]
x = np.linspace(range[0], range[1], shape[1])
y = np.linspace(range[0], range[1], shape[0])
y=y[::-1] #reverse y

F = np.zeros(shape,dtype=float)
for i in xrange(shape[0]):
    for j in xrange(shape[1]):
        F[i,j] = func(x[j],y[i])


plt.figure()
plt.imshow(F, extent=[range[0],range[1],range[0],range[1]], interpolation='bicubic',cmap=cm.bwr)
CS = plt.contour(x,y,F,10,colors='black')
plt.clabel(CS, inline=1, fontsize=10)
plt.title('Simplest default with labels')
plt.show()
