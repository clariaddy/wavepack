
#MODULE 1

def aaa():
    return "aaa from module1.py"

def bbb():
    return "bbb from module1.py"

if __name__ == "__main__":
    print aaa()
    print bbb()
else: 
    print "THIS WAS IMPORTED AS A MODULE (from "+__name__+")"
