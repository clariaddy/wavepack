
import module1 as m1

#MODULE 2

def aaa():
    return "aaa from module2.py"

def bbb():
    return m1.bbb()+" but called from module2 !"

def ccc():
    return 3.14


if __name__ == "__main__":
    print aaa()
    print bbb()
    print ccc()
else: 
    print "THIS WAS IMPORTED AS A MODULE (from "+__name__+")"

