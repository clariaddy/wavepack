% This file was created with JabRef 2.10b2.
% Encoding: UTF-8


@TechReport{Reference13,
  Title                    = {The Continuous Wavelet Transform: A Primer},
  Author                   = {Aguiar, Luís Francisco and Soares, Maria Joana},
  Institution              = {NIPE - Universidade do Minho},
  Year                     = {2011},
  Number                   = {16/2011},
  Type                     = {NIPE Working Papers},

  Keywords                 = {Continuous Wavelet Transform; Cross-Wavelet Transform; Wavelet Coherency; Partial Wavelet Coherency; Multiple Wavelet Coherency; Wavelet Phase-Difference; Economic fluctuations},
  Url                      = {http://EconPapers.repec.org/RePEc:nip:nipewp:16/2011}
}

@Article{Reference10,
  Title                    = {Cross Wavelet Transform Based Analysis of Electrocardiogram Signals},
  Author                   = {Swati Banerjee and M. Mitra},
  Journal                  = {International Journal of Electrical, Electronics and Computer Engineering},
  Year                     = {2012},
  Number                   = {2},
  Pages                    = {88-92},
  Volume                   = {1}
}

@Book{Reference14,
  Title                    = {Wavelets in Physics},
  Author                   = {J.C. Van Der Berg},
  Publisher                = {Cambridge University press},
  Year                     = {1999},

  Doi                      = {ISBN 0 521 59311 5 hardback}
}

@TechReport{Reference25,
  Title                    = {A Survey and Comparison of Time-Delay Estimation Methods in Linear Systems},
  Author                   = {Svante Bjorklund},
  Institution              = {Linkopings university, Sweden},
  Year                     = {2003},

  Doi                      = {ISBN 91-7373-870-0}
}

@Manual{Reference7,
  Title                    = {Lectures on a Theory of Computation and Complexity over the Reals},

  Address                  = {Coloquio Brasilieiro de Matematica},
  Author                   = {Lenora Blum},
  Edition                  = {IMPA},
  Year                     = {1991}
}

@PhdThesis{Reference28,
  Title                    = {{L'air pi{\'e}g{\'e} dans les glaces polaires: Contraintes chronologiques et caract{\'e}risation de la variabilit{\'e} climatique rapide}},
  Author                   = {Capron, Emilie},
  School                   = {{Universit{\'e} de Versailles-Saint Quentin en Yvelines}},
  Year                     = {2010},
  Month                    = Sep,
  Type                     = {Theses},

  Hal_id                   = {tel-00579600},
  Hal_version              = {v1},
  Keywords                 = {air isotopes ; Antarctica ; bipolar seesaw pattern ; climatic variability at millennial and sub-millennial scale ; dating ; firn ; glacial period ; Greenland ; last glacial inception ; ice core ; methane. ; Antarctique ; carotte de glace ; Dansgaard-Oeschger ; datation ; entr{\'e}e en glaciation ; Groenland ; instabilit{\'e}s climatiques abruptes ; isotopes de l'air ; m{\'e}thane ; n{\'e}v{\'e} ; p{\'e}riode glaciaire ; bascule bipolaire ; pal{\'e}oclimat.},
  Pdf                      = {https://tel.archives-ouvertes.fr/tel-00579600/file/memoire-these-CAPRON.pdf},
  Url                      = {https://tel.archives-ouvertes.fr/tel-00579600}
}

@Article{Reference18,
  Title                    = {A statistical study temporally smoothed wavelet coherence},
  Author                   = {E.A.K. Cohen and A. T. Walden},
  Journal                  = {IEE Transaction on signal processing},
  Year                     = {2010},

  Month                    = {June},
  Number                   = {6},
  Volume                   = {58}
}

@Article{Reference2,
  Title                    = {Quantifying the consensus on anthropogenic global warming in the scientific literature},
  Author                   = {John Cook and Dana Nuccitelli and Sarah A Green and Mark Richardson and Bärbel Winkler and Rob Painting and Robert Way and Peter Jacobs and Andrew Skuce},
  Journal                  = {Environmental Research Letters},
  Year                     = {2013},
  Number                   = {2},
  Pages                    = {024024},
  Volume                   = {8},

  Url                      = {http://stacks.iop.org/1748-9326/8/i=2/a=024024}
}

@Misc{Reference24,
  Author                   = {George Dallas},
  HowPublished             = {\url{https://georgemdallas.wordpress.com/2014/05/14/wavelets-4-dummies-signal-processing-fourier-transforms-and-heisenberg/}},
  Year                     = {2014}
}

@Manual{Reference5,
  Title                    = {From Fourier Analysis to Wavelets},

  Address                  = {Instituto de Matematica Pura e Aplicada, IMPA Rio de Janeiro, Brazil},
  Author                   = {Jonas Gomes and Luiz Velho},
  Edition                  = {IMPA},
  Note                     = {Course Notes { SIGGRAPH 99}},
  Year                     = {1997}
}

@Article{Reference3,
  Title                    = {Application of the cross wavelet transform and wavelet coherence to geophysical time series},
  Author                   = {Grinsted, A. and Moore, J. C. and Jevrejeva, S.},
  Journal                  = {Nonlinear Processes in Geophysics},
  Year                     = {2004},
  Number                   = {5/6},
  Pages                    = {561--566},
  Volume                   = {11},

  Doi                      = {10.5194/npg-11-561-2004},
  Url                      = {http://www.nonlin-processes-geophys.net/11/561/2004/}
}

@Article{Reference15,
  Title                    = {Wavelet Transform and atmospheric turbulence},
  Author                   = {Lonnie Hudgins and Carl A; Friehe and Meinhard E. Mayer},
  Journal                  = {Physical Review letters},
  Year                     = {1993},

  Month                    = {November},
  Number                   = {20},
  Volume                   = {71}
}

@Book{Reference8,
  Title                    = {A student's guide to Fourier Transform},
  Author                   = {J. F. James},
  Editor                   = {2^{nd}},
  Publisher                = {Cambridge},
  Year                     = {2002}
}

@Book{Reference9,
  Title                    = {A Friendly Guide to Wavelets},
  Author                   = {Kaiser, Gerald},
  Publisher                = {Birkhauser Boston Inc.},
  Year                     = {1994},

  Address                  = {Cambridge, MA, USA},

  ISBN                     = {0-8176-3711-7}
}

@Article{Reference12,
  Title                    = {The Cross-Wavelet Transform and Analysis of Quasi-periodic Behavior in the Pearson-Readhead VLBI Survey Sources},
  Author                   = {Brandon C. Kelly and Philip A. Hughes and Hugh D. Aller and Margo F. Aller},
  Journal                  = {The Astrophysical Journal},
  Year                     = {2003},
  Number                   = {2},
  Pages                    = {695},
  Volume                   = {591},

  Url                      = {http://stacks.iop.org/0004-637X/591/i=2/a=695}
}

@Article{Reference11,
  Title                    = {Cross wavelet analysis: significance testing and pitfalls},
  Author                   = {Maraun, D. and Kurths, J.},
  Journal                  = {Nonlinear Processes in Geophysics},
  Year                     = {2004},
  Number                   = {4},
  Pages                    = {505--514},
  Volume                   = {11},

  Doi                      = {10.5194/npg-11-505-2004},
  Url                      = {http://www.nonlin-processes-geophys.net/11/505/2004/}
}

@Article{Reference19,
  Title                    = {White noise testing using wavelets},
  Author                   = {Guy P. Nason and Delyan Savchev},
  Journal                  = {Wiley},
  Year                     = {2014},
  Volume                   = {3}
}

@Article{Reference1,
  Title                    = {\textit{Atmospheric carbon dioxide, methane, deuterium, and calculated Antarctic temperature of EPICA Dome C ice core}},
  Author                   = {Fr{\'e}d{\'e}ric {Parrenin} and Valerie {Masson-Delmotte} and Peter {K{\"o}hler} and Dominique {Raynaud} and Didier {Paillard} and Jakob {Schwander} and Carlo {Barbante} and Amaelle {Landais} and Anna {Wegner} and Jean {Jouzel}},
  Year                     = {2013},

  Doi                      = {10.1594/PANGAEA.810199},
  Url                      = {https://doi.pangaea.de/10.1594/PANGAEA.810199}
}

@Article{Reference21,
  Title                    = {Synchronous Change of Atmospheric CO2 and Antarctic Temperature During the Last Deglacial Warming},
  Author                   = {F. Parrenin and V. Masson-Delmotte and P. Kohler and D. Raynaud and D. Paillard and J. Schwander and C. Barbante and A. Landais and A. Wegner and J. Jouzel},
  Journal                  = {Science},
  Year                     = {2013},

  Month                    = {March},
  Volume                   = {339}
}

@Book{Reference16,
  Title                    = {Wavelet methods for time series analysis},
  Author                   = {D. B. Percival and A. T. Walden},
  Publisher                = {Cambridge},
  Year                     = {2000},

  Url                      = {http://www.staff.washington.edu/dbp/wmtsa.html}
}

@PhdThesis{Reference27,
  Title                    = {{Role of the ice sheets in the climatic system: Interactions analysis between an Antarctic ice sheet model and a climate model}},
  Author                   = {Philippon, Gwena{\"e}lle},
  School                   = {{Universit{\'e} Pierre et Marie Curie - Paris VI}},
  Year                     = {2007},
  Month                    = Mar,
  Type                     = {Theses},

  Hal_id                   = {tel-00328184},
  Hal_version              = {v1},
  Keywords                 = {Climate ; ice sheet Interactions ; Antarctic ; Feedbacks ; climatic model ; ice sheet model ; paleoclimate ; last deglaciation ; derni{\`e}re d{\'e}glaciation ; Interactions climat ; calotte de glace ; Antarctique ; R{\'e}troactions ; mod{\`e} le de climat ; mod{\`e}le de calotte de glace ; pal{\'e}oclimat ; derni{\`e}re d{\'e}glaciation.},
  Pdf                      = {https://tel.archives-ouvertes.fr/tel-00328184/file/ThesePhilippon_poidsplume.pdf.pdf},
  Url                      = {https://tel.archives-ouvertes.fr/tel-00328184}
}

@Manual{Reference20,
  Title                    = {WaveletComp: Computational Wavelet Analysis},
  Author                   = {Angi Roesch and Harald Schmidbauer},
  Note                     = {R package version 1.0},
  Year                     = {2014},

  Url                      = {http://www.hs-stat.com/projects/WaveletComp/WaveletComp_guided_tour.pdf}
}

@Manual{Reference17,
  Title                    = {Wavelet Analysis on stochastic time series},
  Author                   = {Tobias Setz and PD Diethelm Wurtz},
  Note                     = {A visual introduction with an examination of long term financial time series},
  Year                     = {2011},

  School                   = {ETH Zurich}
}

@Article{Reference4,
  Title                    = {A Practical Guide to Wavelet Analysis},
  Author                   = {Christopher Torrence and Gilbert P. Compo},
  Journal                  = {Bulletin of the American Meteorological Society},
  Year                     = {1998},
  Pages                    = {61--78},
  Volume                   = {79}
}

@Article{Reference23,
  Title                    = {Audio Signal Processing Using Time-Frequency Approaches: Coding, Classification, Fingerprinting, and Watermarking},
  Author                   = {K Umapathy and B Ghoraani and S Krishnan},
  Journal                  = {EURASIP Journal on Advances in signal processing},
  Year                     = {2010},

  Doi                      = {10.1155/2010/451695}
}

@Article{Reference6,
  Title                    = {Wavelets and filter banks : Theory and design.},
  Author                   = {Vetterl,M. and Herley, C.},
  Journal                  = {IEE Trans. Acoust. Speech Signal Process},
  Year                     = {1992},
  Number                   = {9},
  Volume                   = {40}
}

@PhdThesis{Reference26,
  Title                    = {Complex Directional Wavelet Transforms: Representation, Statistical Modeling and Applications},
  Author                   = {Vo, An Phuoc Nhu},
  Year                     = {2008},

  Address                  = {Arlington, TX, USA},
  Note                     = {AAI3339226},

  Advisor                  = {Oraintara, Soontorn},
  ISBN                     = {978-0-549-94982-4},
  Publisher                = {University of Texas at Arlington}
}

@Book{Reference22,
  Title                    = {The spectral analysis of time series},
  Publisher                = {Science Direct},
  Year                     = {1995},

  Doi                      = {doi:10.1016/B978-0-12-419251-5.50016-8}
}

