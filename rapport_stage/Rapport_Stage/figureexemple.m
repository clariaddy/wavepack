% Exemple d'exportation d'une figure matlab en eps et pdf

% Trac� initial
figure
plot((0:0.001:1),0.8*sin((0:0.001:1)*2*pi*10),'r')

% d�finition des valeurs limites sur les axes
axis([0 1 -1 1])

% affichage d'une quadrillage
grid

% Il est parfois utile de fixer la position des graduations sur les axes
set(gca,'XTick',0:0.2:1)
set(gca,'YTick',-1:0.5:1)

% On fixe le nom et la taille de la police pour les axes
set(gca,'FontName','Times New Roman','FontSize',11)

% On fixe la taille de la figure export�e
set(gcf,'units','centimeters','papersize',[8 5],'paperposition',[0 0 8 5])

% �tiquetage des axes (par simplicit�, on choisit une version courte ici
% dans l'optique d'utiliser ensuite le package psfrag)
xlabel('t')
ylabel('U')

% On exporte la figure en eps (niveau 2)
print -depsc2 bellefigure.eps

% pour un export en pdf, on explicite l'�tiquetage des axes
xlabel('Temps t/(s)')
ylabel('Tension U/(V)')

% On exporte la figure en pdf
print -dpdf bellefigure.pdf

xlabel('t')
ylabel('U')

% exemple de mauvais param�tres d'exportation (figure trop grande, bitmap)
set(gcf,'papersize',[20 12.5],'paperposition',[0 0 20 12.5],'renderer','zbuffer')
print -depsc contreexemple.eps

% cr�ation d'un exemple de figure bitmap avec une r�solution adapt�e
figure
x=-2:0.01:2;
pcolor(x,x,exp(-((x'*ones(size(x))).^2+((ones(size(x')))*x).^2)/2))
shading interp
colormap gray
colorbar
axis square
colormap(ones(64,1)*[1 0 0]+[63:-1:0]'*[0 1 1]/63)

set(gca,'XTick',-2:2,'TickDir','Out')
set(gca,'YTick',-2:2)

% On fixe le nom et la taille de la police pour les axes
set(gca,'FontName','Times New Roman','FontSize',11)

% On fixe la taille de la figure export�e
set(gcf,'units','centimeters','papersize',[8 6],'paperposition',[0 0 8 6])

% �tiquetage vide des axes (pour laisser de la place pour l'annotation via
% \drawat
xlabel(' ')
ylabel(' ')

% On exporte la figure en eps (niveau 2) avec une r�solution de 600ppp
print -dpdf -r600 intensite.pdf
%print -depsc2 -r600 intensite.eps

