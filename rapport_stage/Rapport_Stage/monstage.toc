\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Review on signal transform}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Reconstruction, sampling function and interpolation}{1}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Point sampling and Fourier series}{2}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Fourier transform and Filtering}{2}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Windowed Fourier transform}{3}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}The Uncertainty Principle}{4}{subsubsection.2.1.4}
\contentsline {subsection}{\numberline {2.2}Function representation using Wavelets}{4}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Wavelet functions}{6}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}The continuous wavelet transform}{7}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Inversion of continuous wavelet transform}{8}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}From continuous wavelet transform to discrete wavelet transform}{8}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}Cross-wavelet transform and wavelet coherence}{9}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}The Uncertainty Principle}{10}{subsubsection.2.2.6}
\contentsline {subsubsection}{\numberline {2.2.7}Wavelet significance levels}{11}{subsubsection.2.2.7}
\contentsline {section}{\numberline {3}CO2 and temperature time series from wavelet transform perceptive}{12}{section.3}
\contentsline {subsection}{\numberline {3.1}Data processing steps}{13}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Wavelet data analysis}{13}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Morlet wavelet power spectrum in time and frequency domain}{13}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Paul wavelet power in time and frequency domain}{15}{subsubsection.3.2.2}
\contentsline {subsection}{\numberline {3.3}Phasing analysis}{16}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Phasing analysis with existing method}{16}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Contribution on the time delay analysis}{16}{subsubsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3}Discussing the results}{17}{subsubsection.3.3.3}
\contentsline {section}{\numberline {4}Conclusion}{18}{section.4}
\contentsline {section}{\numberline {5}Appendix A}{20}{section.5}
\contentsline {subsection}{\numberline {5.1}Data pre-processing: de-trending time series}{20}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Discrete computations: Reconstructed data from wavelet transfrom}{20}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Mean circular time delay representation}{21}{subsection.5.3}
\contentsline {section}{\numberline {6}Toolbox}{21}{section.6}
