
import os

user = os.environ.get('USER')
home = os.environ.get('HOME')

if user=='keddy':
    project_dir = home + '/Documents/st/stage_ens'
else:
    project_dir = home + '/Documents/stage_ens'

data_dir = project_dir + '/data'
plot_script_dir = project_dir + '/src/python/plots'
