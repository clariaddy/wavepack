
fig = plt.figure()
figname = 'interpolation'
fig.suptitle('Interpolation on same timesteps', **plottitle_kargs)

s = plt.subplot2grid((2,2),(0,0), colspan=2)
s.set_title('Interpolation of ' +fn0)
p0, = s.plot(interp['t'],interp[fn0],data0_color)
p1, = s.plot(t0,data0,'o'+data1_color,markersize=2.0)
p2, = s.plot(t0,[(min(data0)+max(data0))/2.0]*data0.size,'.g',markersize=2.0)
s.set_xlabel(time_label)
s.set_ylabel(data0_label)
s.set_xlim(tmin,tmax)
s.legend([p0,p1,p2], ['Interpolation', 'Original data', 'Original data sampling'], markerscale=4.0)

s = plt.subplot2grid((2,2),(1,0), colspan=2)
s.set_title('Interpolation of ' +fn1)
p0, = s.plot(interp['t'],interp[fn1],data1_color)
p1, = s.plot(t1,data1,'o'+data0_color,markersize=2.0)
p2, = s.plot(t1,[(min(data1)+max(data1))/2.0]*data1.size,'.g',markersize=2.0)
s.set_xlabel(time_label)
s.set_ylabel(data1_label)
s.set_xlim(tmin,tmax)
s.legend([p0,p1,p2], ['Interpolation', 'Original data', 'Original data sampling'], markerscale=4.0)

wp.plot(fig,save_dir,figname,plot_ext,show_plots,save_plots)
