from matplotlib.ticker import MultipleLocator, FormatStrFormatter
fig = plt.figure()
figname = 'time_delay'
fig.suptitle('Time Delay Estimation', **plottitle_kargs)

# s = plt.subplot(211)
# s.xaxis.set_major_locator(MultipleLocator(1.0))
# s.xaxis.set_minor_locator(MultipleLocator(0.25))
# s.xaxis.set_major_formatter(FormatStrFormatter('%d'))
# s.grid(True, which='major', linestyle='-',  linewidth=1.0)
# s.grid(True, which='minor', linestyle='--', linewidth=0.5)
# s.plot(times, MCA/np.pi * 180.0)
# plt.xlim([tmin,tmax])
# plt.ylim([-180.0,+180.0])
# plot_deg = [-180.0 + k*30.0 for k in xrange(13)]
# plt.yticks(plot_deg, [str(x) for x in plot_deg], color='r')

# s, ax= plt.subplots(nrows=1, ncols=1, sharex=True)
# #ax = axs[0,0]
# ax.errorbar(times, time_delays*1000, yerr=yerr*1000, fmt='o')
# ax.set_title('lol')

#s, axs= plt.subplots(nrows=2, ncols=1, sharex=True)
#ax = axs[0,]
#ax.errorbar(times, time_delays_diff*1000, yerr=yerr2*1000, fmt='o')
#ax.set_title('Vert. symmetric')
s = plt.subplot(211)
s.xaxis.set_major_locator(MultipleLocator(1.0))
s.xaxis.set_minor_locator(MultipleLocator(0.25))
s.xaxis.set_major_formatter(FormatStrFormatter('%d'))
s.grid(True, which='major', linestyle='-',  linewidth=1.0)
s.grid(True, which='minor', linestyle='--', linewidth=0.5)
# s.fill_between(times, time_delays_min*1000, time_delays_max*1000, alpha=0.2, color='g')
# s.plot(times, time_delays_max*1000, 'g--')
# s.plot(times, time_delays_min*1000, 'g--')
p1, =s.plot(times, time_delays2*1000, 'r')
p2, =s.plot(times, time_delays*1000, 'b')
#p3, =s.plot(times, time_diff*1000, 'g')
s.legend([p1,p2],['weighted_timelay','mean_circular_time_delays'])
s.set_xlim(tmin,tmax)
plt.xlabel('times($kyrs$)')
wp.plot(fig,save_dir,figname,plot_ext,show_plots,save_plots)
