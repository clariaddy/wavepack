
fig = plt.figure()
figname = 'normalization'
fig.suptitle('Input Normalization', **plottitle_kargs)

s  = plt.subplot(111)
s.set_xlabel(time_label)

p0, = s.plot(times,data0,data0_color)
s.set_ylabel(udata0_label)
s.set_xlim(tmin,tmax)

s1 = s.twinx()
p1, = s1.plot(times,data1,data1_color)
s1.set_ylabel(udata1_label)
s1.set_xlim(tmin,tmax)

s.legend([p0,p1], [fn0,fn1])

wp.plot(fig,save_dir,figname,plot_ext,show_plots,save_plots)
