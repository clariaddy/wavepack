from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from scipy import interpolate

fig = plt.figure()
figname = 'signal_phaseshift'
fig.suptitle('Timedelays', **plottitle_kargs)

s  = plt.subplot(211)

s.xaxis.set_major_locator(MultipleLocator(1.0))
s.xaxis.set_minor_locator(MultipleLocator(0.25))
s.xaxis.set_major_formatter(FormatStrFormatter('%d'))
s.grid(True, which='major', linestyle='-',  linewidth=1.0)
s.grid(True, which='minor', linestyle='--', linewidth=0.5)

#Tmax = np.max(time_delays2)
#Tmin = np.min(time_delays2)
ids = ~np.isnan(time_delays2)
time_delays1 = sp.ndimage.filters.gaussian_filter1d(time_delays, sigma=6.0)
td = times[ids] + time_delays1[ids]

p0, = s.plot(times,data0s,'b')
p1, = s.plot(times,data1s,'r')
p2, = s.plot(td,data0s[ids],'k')
s.set_xlabel(time_label)
s.set_ylabel('Normalized Units')
s.set_xlim(tmin,tmax)
s.set_ylim(min(np.min(data0s),np.min(data1s)),max(np.max(data0s),np.max(data1s)))
s.legend([p0,p1], [fn0+' smoothed',fn1+' smoothed'])

s  = plt.subplot(212)
s.xaxis.set_major_locator(MultipleLocator(1.0))
s.xaxis.set_minor_locator(MultipleLocator(0.25))
s.xaxis.set_major_formatter(FormatStrFormatter('%d'))
s.grid(True, which='major', linestyle='-',  linewidth=1.0)
s.grid(True, which='minor', linestyle='--', linewidth=0.5)

s.fill_between(times, time_delays_min*1000, time_delays_max*1000, alpha=0.2, color='g')
s.plot(times, time_delays_max*1000, 'g--')
s.plot(times, time_delays_min*1000, 'g--')
s.plot(times, time_delays*1000, 'g')
s.plot(times, time_delays1*1000, 'r')
s.plot(times, time_delays2*1000, 'b')
s.set_xlim(tmin,tmax)

wp.plot(fig,save_dir,figname,plot_ext,show_plots,save_plots)
