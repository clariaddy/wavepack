
fig = plt.figure()
figname = 'raw_inputs'
fig.suptitle('Raw Data Inputs', **plottitle_kargs)

s = plt.subplot2grid((2,2),(0,0))
s.set_title('Raw input data ' + fn0)
s.plot(t0,data0,data0_color)
s.set_xlabel(time_label)
s.set_ylabel(data0_label)

s = plt.subplot2grid((2,2),(0,1))
s.set_title('Raw input data ' + fn1)
s.plot(t1,data1,data1_color)
s.set_xlabel(time_label)
s.set_ylabel(data1_label)

s  = plt.subplot2grid((2,2),(1,0),colspan=2)
s.set_title('Input data restricted to common intervals')
s.set_xlabel(time_label)

p0, = s.plot(t0,data0,data0_color)
s.set_ylabel(data0_label)
s.set_xlim(tmin,tmax)

s1 = s.twinx()
p1, = s1.plot(t1,data1,data1_color)
s1.set_ylabel(data1_label)
s1.set_xlim(tmin,tmax)

s.legend([p0,p1], [fn0,fn1])

wp.plot(fig,save_dir,figname,plot_ext,show_plots,save_plots)
