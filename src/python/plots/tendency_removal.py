
fig = plt.figure()
figname = 'tendency'
fig.suptitle('Tendency Removal', **plottitle_kargs)

s = plt.subplot2grid((2,2),(0,0))
s.set_title(fn0 + ' Tendency')
p0, = s.plot(times,olddata0,data0_color)
p1, = s.plot(times,predicted,'g')
p2, = s.plot(times,predicted0,'k--')
s.set_xlabel(time_label)
s.set_ylabel(udata0_label)
s.legend([p0,p1,p2],['Normalized '+fn1, 'Mean tendency', fn1+' Tendency'])

s = plt.subplot2grid((2,2),(0,1))
s.set_title(fn1 + ' Tendency')
p0, = s.plot(times,olddata1,data1_color)
p1, = s.plot(times,predicted,'g')
p2, = s.plot(times,predicted1,'k--')
s.set_xlabel(time_label)
s.set_ylabel(udata1_label)
s.legend([p0,p1,p2],['Normalized '+fn0, 'Mean tendency', fn0+' Tendency'])


s  = plt.subplot2grid((2,2),(1,0),colspan=2)
s.set_title('Data inputs with mean tendency removed')
s.set_xlabel(time_label)

p0, = s.plot(times,data0,data0_color)
s.set_ylabel(udata0_label)
s.set_xlim(tmin,tmax)

s1 = s.twinx()
p1, = s1.plot(times,data1,data1_color)
s1.set_ylabel(udata1_label)
s1.set_xlim(tmin,tmax)
s.legend([p0,p1], [fn0,fn1])

wp.plot(fig,save_dir,figname,plot_ext,show_plots,save_plots)
