fig = plt.figure()
figname = 'global_spectrum'
fig.suptitle('Wavelet global spectrum', **plottitle_kargs)

s = plt.subplot(211)
s.set_title('global power spectrum ')
p0,=s.plot(WP0_g,periods,data0_color)
s.set_ylabel('periods')
s.set_xlabel(fn0 + 'global power spectrum')
s = plt.subplot(212)
p1, =s.plot(WP1_g,periods,data1_color)
s.set_xlabel(fn1 + 'global power spectrum')
s.set_ylabel('periods')
wp.plot(fig,save_dir,figname,plot_ext,show_plots,save_plots)
