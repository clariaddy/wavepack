import scipy.signal as signal

# First, design the Buterworth filter
N  = 2    # Filter order
Wn = 0.001 # Cutoff frequency
B, A = signal.butter(N, Wn, output='ba')
# Second, apply the filter
tempf = signal.filtfilt(B,A, fn1)
CO2f = signal.filtfilt(B,A, fn0)
# Make plots
fig = plt.figure()
figname = 'filtering'
fig.suptitle('signals', **plottitle_kargs)
fig = plt.figure()
ax1 = fig.add_subplot(211)
plt.plot(times,fn0, 'b-')
plt.plot(times,CO2f, 'r-',linewidth=2)
plt.ylabel("CO2 in unity")
plt.ylabel('Unity Normalized Units')
plt.xlim(tmin,tmax)
plt.legend(['Original','Filtered'])
ax1.axes.get_xaxis().set_visible(False)
 
ax1 = fig.add_subplot(212)
plt.plot(times,fn1, 'b-')
plt.plot(times,tempf, 'r-',linewidth=2)
plt.ylabel("Temperature in unity")
plt.xlim(tmin,tmax)
plt.legend(['Original','Filtered'])
ax1.axes.get_xaxis().set_visible(False)
plt.legend(['Residuals'])
