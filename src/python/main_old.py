import matplotlib.pyplot as plt
import numpy as np
import sys
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy import signal, fftpack
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

#---------------------------
""" local includes """
#-----------------------------------

import wavepack as wp
import env
#import AR1 as ar1
# import crosswavelet_angle as ca
# import cwt_smoothing as smooting
# import statistical_test as statest

#--------------------------
""" Used wavelet families  and their plot """
#----------------------------------

morlet = wp.WaveletFamily('cmor')
paul   = wp.WaveletFamily('paul')
dog    = wp.WaveletFamily('dog')


paul = wp.WaveletFamily('paul')
#morlet.plot(count=3)
paul.plot(count=5,plotFreq=True)
#dog.plot(count=4,plotFreq=True,plotTime=True)
plt.show()
#


#---------------------------------------
""" Chosen wavelet mother """
#------------------------------------------

#wave = morlet.wavelet({'omega0':6.0})
#wave = paul.wavelet({'m':10.0})

sys.exit(1)

flambda  = wave.fourier_wavelength()
efolding = wave.efolding(attenuation=np.exp(-2))

data_folder = env.data_folder
file_names = [ 'CO2.txt', 'ATS.txt' ]

data = wp.DataSeries(dtype=float)
data.loadFromTxt(data_folder,file_names[0], {'t0':0,'CO2':1})
data.loadFromTxt(data_folder,file_names[1], {'t1':0,'temp':2})

data['t0'] = data['t0']/1000.
data['t1'] = data['t1']/1000.
tmin = max(data['t0'][0], data['t1'][0])
tmax = min(data['t0'][-1], data['t1'][-1])
N = 2**8
temp = data['temp']
CO2 = data['CO2']

#-------------------------------------------
""" Plot originl data """
#---------------------------------------------

#fig0, ax1 = plt.subplots()
#ax1.plot(data['t0'], CO2, 'b-')
# ax1.set_xlabel('time (s)')
# plt.grid()
# #Make the y-axis label and tick labels match the line color.
# ax1.set_ylabel('CO2', color='b')
# for tl in ax1.get_yticklabels():
#     tl.set_color('b')


# ax2 = ax1.twinx()
# ax2.plot(data['t1'], temp, 'r')
# ax2.set_ylabel('temp', color='r')
# plt.xlim([8.,20.])
# fig0.savefig('wavelet_analysis_AR(1).pdf')
#plt.show()

#-------------------------------------
""" normilized CO2 time series """
#-----------------------------------------------
 
data['CO2'] = (CO2 - np.mean(CO2)) / np.std(CO2,ddof=1)
data['temp'] = (temp - np.mean(temp)) / np.std(temp,ddof=1)

#------------------------------------------
"""interpolation procedure """
#--------------------------------------------------

interp = wp.TimeSeries(tmin, tmax, N, dtype=float)
interp.interpolate('CO2', data['t0'], data['CO2'])
interp.interpolate('temp', data['t1'], data['temp'])
print interp
ext = interp.extend(ext='zeros')
print ext

#-------------------------------------
""" Input initialization  after interpolation"""
#------------------------------------------

t = interp['t']
dt = ext.dt
L = ext.L
C = ext.C
smin = 2.0*dt
smax = L /(2.0*efolding) * (1.0/2.0)

CO2_=interp['CO2']
temp_ =interp['temp']

#---------------------------
"""removing trend""" 
#-------------- -----------------

model1 = np.polyfit(t, CO2_, 1)
predicted1 = np.polyval(model1, t)
CO21=CO2_ - predicted1


model2 = np.polyfit(t, temp_, 1)
predicted2 = np.polyval(model2, t)
temp1 = temp_ - predicted2

#------------------------------------------------
""" ploting time series without trend"""
#----------------------------------------------------

""" CO2"""

# fig1, axes = plt.subplots(nrows=2, sharex=True)
# axes[0].plot(t, CO2_, 'b')
# axes[0].plot(t, predicted1, 'k-')
# axes[0].set(title='CO2 and 1st Order Polynomial Trend')
# axes[1].plot(t, CO21, 'b')
# axes[1].set(title='Detrended Residual')

""" temperature"""


# fig2, axes = plt.subplots(nrows=2, sharex=True)
# axes[0].plot(t, temp_, 'r')
# axes[0].plot(t, predicted2, 'k-')
# axes[0].set(title='Temperature and 1st Order Polynomial Trend')
# axes[1].plot(t, temp1, 'r')
# axes[1].set(title='Detrended Residual')

# plt.show()

CO2=CO21
temp=temp1

#----------------------------------
"""PLOT CHOSEN WAVELETS"""
#------------------------------------

#plt.figure(1)
#wave.plotTime(subplot=(2,1,1))
#wave.plotFreq(subplot=(2,1,2))
#plt.show()

#----------------------------
"""PLOT DATA"""
#----------------------------------------

# plt.figure(2)
# plt.plot(data['t0'],data['CO2'])
# plt.plot(interp['t'],interp['CO2'])
# plt.plot(data['t1'],data['temp'])
# plt.plot(interp['t'],interp['temp'])
# plt.xlim([tmin,tmax])
# plt.show()

#----------------------------------------
"""PLOT MIN AND MAX WAVE"""
#-------------------------------------------------

#plt.figure(3)
#plt.xlim(tmin,tmax)
#plt.plot(t, np.abs( wave.psi_0( (t-(tmax+tmin)/2.0) / smin )))
#plt.plot(t, np.real( wave.psi_0( (t-(tmax+tmin)/2.0) / smin )))
#plt.plot(t, np.imag( wave.psi_0( (t-(tmax+tmin)/2.0) / smin )))
#plt.plot(t, np.abs( wave.psi_0( (t-(tmax+tmin)/2.0) / smax )))
#plt.plot(t, np.real( wave.psi_0( (t-(tmax+tmin)/2.0) / smax )))
#plt.plot(t, np.imag( wave.psi_0( (t-(tmax+tmin)/2.0) / smax )))
#plt.show()



#------------------------------------------------------------------------
#-----------Wavelet transform of two time series-----------------------
#----------------------------------------------------------------

dj = 0.01
jmax = np.log2(smax/smin)/dj
j = np.arange(jmax+1)
scales = smin * 2.0**(j*dj)
print scales.size

periods = wave.periods(scales)

pmin = wave.periods(smin)
pmax = wave.periods(smax)

#continuous wavelet transforms and power spectrum

W_temp = wp.swt(ext['temp'], wave, scales, dt=dt, normalization='Kaiser')
W_temp = W_temp[:,N/2:N+N/2]

W_CO2 = wp.swt(ext['CO2'], wave, scales, dt=dt, normalization='Kaiser')
W_CO2 = W_CO2[:,N/2:N+N/2]

#----------------------------------------------------------------------------
# cross wavelet analysis 
#-------------------------------------------------------------------------------------------
#------------------------------------------------------------
"""Power spectrum  and cross coherence"""
#------------------------------------------------------

CWT = W_CO2*np.conj(W_temp)         #cross wavelet spectrum
WP_temp = np.abs(W_temp)**2
WP_CO2 = np.abs(W_CO2)**2
XWT  = np.abs(CWT)**2                   #amplitude of cross wavelet spectrum
XWTC = XWT/(np.sqrt(WP_temp*WP_CO2)) #cross wavelet coherence

#|----------------------------------------------------------
"""Smoothing procedure"""
#-----------------------------------------------------------

shape=np.array(CWT.shape, dtype=float)
window_size=np.ceil(0.1*shape)
window_size +=(window_size%2==0)

XWTC=smooting.cwt_smooth(XWTC, window_size, F_filter=np.mean)

# cone of influence
coi = dt*flambda/efolding * np.concatenate( ( [1e-15], np.arange((N+1)/2-1), np.flipud(np.arange(0, N/2-1)), [1e-15]) )
freqs= 1./periods


#--------------------------------------------
#Statistical test
#-------------------------------------------------------------

alpha_1,mu2_2,sigma_e1 =ar1.estimate_AR1_parameters(CO2)
alpha_2,mu2_2,sigma_e2 =ar1.estimate_AR1_parameters(temp)

AR1 =ar1.gen_AR1(alpha_2,N,M=1,m=0.0,sigma_e=1.0)
print AR1
ch2=ar1.WP_significant_level(W_temp,p=0.95)
print ch2

Pk1 = ar1._Pk(alpha_1,freqs,dt)
Pk2 = ar1._Pk(alpha_2,freqs,dt)

plt.figure()
plt.plot(Pk1,'r', Pk2, 'b')
plt.show()
# Ps1=ar1._Ps(scales,WP_CO2,alpha_1,freqs,dt)
# Ps2=ar1._Ps(scales,WP_temp,alpha_2,freqs,dt)



# estimate parameters


phi,MCA,MCSD=ca.crosswavelet_angle(CWT, deg=True)

#-------------------------------------
""" extraction """
#----------------------------------------------------
arr= periods
print min(arr),max(arr)
condition1=(periods<1.5)
condition2=(periods>1.5)
ext1=np.extract(condition1,arr)
ext2=np.extract(condition2, arr)

periods1=arr[condition1]
periods2=arr[condition2]
delay_time_period = phi/(2.0*np.pi)*(condition2*arr)[:, None]*1000


# B, A=np.zeros(N) , np.zeros(N)
# for i in xrange(N):
# 	B[i]=np.sum(freqs*XWT[:,i])
# 	A[i]=np.sum(XWT[:,i])

# t=B/A

# t=1./t

# time_delay=t*MCA/2*np.pi

# plt.plot(t,MCA+MCSD)

# plt.plot(t,MCA-MCSD)
# plot_deg = [-180.0 + k*30.0 for k in xrange(N/2)]
#plt.yticks(plot_deg, [str(x) for x in plot_deg], color='red')

# fig2=plt.figure(3)
# plt.subplot(211)
# plt.plot(t,time_delay, 'green')
# plt.grid()
# plt.xlim([min(t),max(t)])
# plt.ylim([-180.0,+180.0])
# # plot_deg = time_delay
# # plt.yticks(plot_deg, [str(x) for x in plot_deg], color='red')
# plt.subplot(212)
# plt.plot(t,MCA)
# plt.grid()
# plt.xlim([min(t),max(t)])
# plt.ylim([-180.0,+180.0])
# #plot_deg = MCA
#plt.yticks(plot_deg, [str(x) for x in plot_deg], color='red')
# plt.title('circular mean angle over all scales')
#fig2.savefig('mean_circular_angle.pdf')





#T,SCALES = np.meshgrid(t,scales)
#ax = plt.subplot(111, projection='3d')
#ax.plot_surface(T,SCALES,time_delay)

#H = time_delay.shape[0]
#td = time_delay[-H/10::int(0.1/dj),:]



#fig4=plt.figure(5)
#for tds in td:
#	plt.plot(tds)
#plt.grid()
#plt.title('btw scales')
#fig4.savefig('btw_scale.pdf')	

#plt.show()

phase_shift_pond_s = np.sum(phi*XWTC,axis=0)/np.sum(XWTC, axis=0)*180./np.pi      # axis=0 ----> scales
phase_shift_pond_t = np.sum(phi*XWTC,axis=1)/np.sum(XWTC, axis=1)*180./np.pi      # axis = 1 ----> time
time_shift_pond_s  = np.sum(delay_time_period*XWTC,axis=0)/np.sum(XWTC, axis=0) # axis=0 ----> scales
time_shift_pond_t  = np.sum(delay_time_period*XWTC,axis=1)/np.sum(XWTC, axis=1)        # axis = 1 ----> time


# fig6=plt.figure()
# plt.subplot(211)
# plt.plot(t,time_shift_pond_s, 'b')
# plt.ylabel('weighted delay time period shift')
# plt.grid()
# plt.subplot(212)
# plt.plot(t,phase_shift_pond_s, 'g')
# plt.ylabel('weighted phase shift')
# plt.grid()
# fig6.savefig('weighted_time&phase_shift.pdf')


#---------------------------------------------------
""" Data recontsuction """
#--------------------------------------------------------------------
# n_dilation=scales.size
# CO2_filtre=np.sum(np.real(W_CO2), axis=0)
# CO2_filtre=(CO2_filtre - np.mean(CO2_filtre))/np.std(CO2_filtre)

# temp_filtre=np.sum(np.real(W_temp), axis=0)
# temp_filtre=(temp_filtre - np.mean(temp_filtre))/np.std(temp_filtre)

# Correl_filtre=np.sum(np.real(CWT) , axis=0)/(np.std(temp_filtre)*np.std(CO2_filtre))
# #Correl_filtre=(Correl_filtre - np.mean(Correl_filtre))/np.std(Correl_filtre)

# data_new=[CO2_filtre, temp_filtre, Correl_filtre]

# ccV0=np.corrcoef(temp,CO2);
# print ccV0
# ccV1=np.corrcoef(CO2_filtre,CO2);
# print ccV1
# ccV2=np.corrcoef(temp,temp_filtre);
# print ccV2
# ccV3=np.corrcoef(temp_filtre,CO2_filtre);
# print ccV3

#print data_new
# plt.figure()
# plt.subplot(311)
# plt.plot(t,CO2 , 'r')
# plt.plot(t,temp, 'b')
# plt.grid()
# plt.ylabel('data with no trend')
# plt.xlabel('time')
# plt.subplot(312)
# plt.plot(t,CO2_filtre , 'r')
# plt.plot(t,temp_filtre , 'b')
# plt.grid()
# plt.xlabel('time')
# plt.ylabel('standadized data')
# plt.subplot(313)
# plt.plot(t,Correl_filtre)
#plt.show()


# #fig7.savefig('wavelet_power_spectra.pdf')
#fig8=plt.figure(9)
#wp.plotWaveletPower(t, periods, WP_CO2s,tmin,tmax, smin ,smax,pmin,pmax, coi=coi,title='CO2 power spectrum sm', subplotid=221)
#wp.plotWaveletPower(t, periods, WP_temps,tmin,tmax, smin ,smax,pmin,pmax, coi=coi,title='temperature power spectrum sm', subplotid=222)
#wp.plotWaveletPower(t, periods, XWTs,tmin,tmax, smin ,smax,pmin,pmax, coi=coi, title='Crosswavelet after sm', subplotid=223)
#wp.plotWaveletPower(t, periods, XWTCs,tmin,tmax, smin ,smax,pmin,pmax, coi=coi, title='cross wavelet coherence after sm', subplotid=224)
#fig8.savefig('wavelet_power_spectra_AR(1).pdf')

#plt.show()
#plt.legend( (' Time_CO2', 'Time_ATC'), loc='upper left')
