
import copy
import numpy as np
import scipy as sp

import scipy.interpolate as interp
import scipy.optimize
import numpy.polynomial.polynomial as poly

class FunctorTensor():

    _valid_modes = ['normal','zeros','clamp','periodic','mirror']

    def __init__(self,fs,nvars=1,shape=None):
        assert len(fs)>=1, 'fs is empty!'
        
        for f in fs:
            if not  hasattr(f,'__call__'):
                raise ValueError('{} is not callable!'.format(f))

        F = np.asarray(fs,dtype=object)
        if shape is not None:
            if (len(shape)==1 and shape[0] != F.size) or (len(shape)>1 and reduce(lambda x,y: x*y, shape) != F.size):
                raise ValueError('Shape {} is not compatible with input size {}!'.format(shape,F.size))
        else:
            shape = (len(fs),)
    
        self.nvars      = nvars
        self.shape      = shape
        self.F          = F

        #load default configuration
        self.configure()

    def check_configuration(self,xmin,xmax,mode):
        assert mode in self._valid_modes, 'Bad mode!'
        assert (xmin is None and mode is 'normal') or (isinstance(xmin,np.ndarray) and xmin.ndim==1 and xmin.size==self.ndim), 'Bad xmin value!'
        assert (xmax is None and mode is 'normal') or (isinstance(ymin,np.ndarray) and ymin.ndim==1 and ymin.size==self.ndim), 'Bad xmax value!'
    
    def configure(self,xmin=None,xmax=None,mode='normal'):
        self.check_configuration(xmin,xmax,mode)
        self.mode = mode
        self.xmin = xmin
        self.xmax = xmax
        self.configured = True
   

    op_template = \
"""
def {fname}(self,rhs):
    res = copy.deepcopy(self)
    if np.isscalar(rhs):
        for i,f in enumerate(res.F):
            res.F[i] = lambda *args: ((f(*args)) {op} (rhs))
        return res
    
    if isinstance(rhs,FunctorTensor):
        assert res.mode == rhs.mode
        if rhs.F.size == 1:
            if res.nvars == rhs.nvars:
                for i,(f,g) in enumerate(zip(res.F,rhs.F)):
                    res.F[i] = lambda *args: ((f(*args)) {op} (g(*args)))
            else:
                n = res.nvars
                for i,(f,g) in enumerate(zip(res.F,rhs.F)):
                    res.F[i] = lambda *args: ((f(*args[:n])) {op} (g(*args[n:])))
                res.nvars = n+rhs.nvars
            return res
    raise ValueError('Bad operands!')
"""
    fops = ['__add__','__sub__','__mul__','__div__','__pow__']
    ops = ['+','-','*','/','**']
    for (fname,op) in zip(fops,ops):
        exec op_template.format(fname=fname,op=op)

    
    def __call__(self,*args):
        assert self.configured,    'ObjectiveFunction should be configured before any __call__ !'
        
        if len(args)==1 and isinstance(args[0],np.ndarray) and args[0].ndim==2:
            if args[0].shape[0] != self.nvars:
                raise ValueError('Passed a unique ndarray but shape[0] != nvars !')
            X = ( args[0][i,:] for i in xrange(args[0].shape[0]) )
            n = args[0].shape[1]
        elif len(args)==self.nvars: 
            n = 1
            X = args
            for x in X:
                if isinstance(x,tuple):
                    x = x[0]
                if isinstance(x,np.ndarray) and x.shape[0]>n:
                    assert x.ndim==1
                    n = x.shape[0]
        else:
            raise ValueError('Bad args passed in FunctorTensor.__apply__(args={}) !'.format(args))

        F = self.F
        mode = self.mode
        xmin = self.xmin
        xmax = self.xmax
        
        out = np.empty(shape=(F.size,n))
        for i,f in enumerate(self.F):
            if mode == 'normal':
                out[i,:] = f(*X)
            elif mode == 'zeros':
                mid = np.logical_and(x>=xmin,x<=xmax)
                out[i,:] = np.piecewise(x, [mid, ~mid], [f,0.0])
            elif mode == 'clamp':
                inf = x<xmin
                sup = x>xmax
                mid = ~np.logical_or(inf,sup)
                out[i,:] = np.piecewise(x, [inf,mid,sup], [f(xmin),f,f(xmax)])
            elif mode == 'periodic':
                L = xmax - xmin
                x = xmin + np.mod(x-xmin,L)
                out[i,:] = f(x)
            elif mode == 'mirror':
                assert np.min(x) <= 2*xmin
                assert np.max(x) <= 2*xmax
                inf = x<xmin
                sup = x>xmax
                mid = ~np.logical_or(inf,sup)
                x = inf*(2*xmin-x) + mid*x + sup*(2*xmax-x)
                out[i,:] = f(x)
            else:
                raise NotImplemented('Mode \'{}\' not implemented yet!'.format(mode))
        
        if n==1:
            out = out.reshape(self.shape)
        else:
            out = out.reshape(self.shape+(n,))

        return np.squeeze(out)


class ObjectiveFunction():

    def __init__(self,dfs):
        self.dfs    = dfs
        if not self.good():
            raise ValueError('Bad functors parameters!')
        self.check()

        self.F   = self.dfs[0]
        self.nvars = self.F.nvars
        if self.has_grad():
            self.dF  = self.dfs[1]
        if self.has_hessian():
            self.d2F = self.dfs[2]
    def gradF(self):
        return self.dF(1)
    def hessianF(self):
        return self.dF(2)
    
    def take_derivative(self,i):
        assert self.dlevel>=1
        if i>=self.nvars:
            raise ValueError('i>=nvars!')
        ndfs = []
        for j,df in enumerate(self.dfs):
            if j==0: 
                assert df.F.size==1
                continue
            nvars = df.nvars
            count = nvars**(j-1)
            dF = copy.deepcopy(df.F[i*count:(i+1)*count])
            shape = df.shape[:-1]
            ndfs.append(FunctorTensor(dF, shape=shape, nvars=nvars))
        return ObjectiveFunction(ndfs)

    def good(self):
        return self.dlevel() >= 0
    def has_grad(self):
        return self.dlevel() >= 1
    def has_hessian(self):
        return self.dlevel() >= 2
    def dlevel(self):
        return len(self.dfs)-1

    def check(self):
        dfs = self.dfs
        for df in dfs:
            if not isinstance(df,FunctorTensor):
                raise ValueError('df {} is not a FunctorTensor !'.format(df))
        nvars = dfs[0].nvars
        for d in xrange(1,self.dlevel()):
            if dfs[d].nvars != nvars:
                raise ValueError('Mismatch in the number of variables between {} and {}, {} vars vs. {} vars !'.format(dfs[0],dfs[d],nvars,dfs[d].nvars))
            if nvars>1 and dfs[d].shape != (self.dfs[d-1].shape+(nvars,)):
                raise ValueError('Shape mismatch in the derivatives {} and {}, {} vs. {} !'.format(dfs[d-1],dfs[d],dfs[d-1].shape+(nvars,),dfs[d].shape))
    
    
    def DF(self,d):
        if self.dlevel()<d:
            raise ValueError('Requesting out of bound derivative!')
        return self.dfs[d]
    
    op_template = \
"""
def {fname}(self,rhs):
    res = copy.deepcopy(self)
    if np.isscalar(rhs):
        for i,df in enumerate(self.dfs):
            res.dfs[i] = res.dfs[i] {op} (rhs)
        return res
    else: 
        raise ValueError('Bad operands!')
"""
    fops = ['__mul__','__div__']
    ops = ['*','/']
    for (fname,op) in zip(fops,ops):
        exec op_template.format(fname=fname,op=op)



class PiecewisePoly():
    def __init__(self,ppoly):
        assert isinstance(ppoly, interp.PPoly)
        self.ppoly = ppoly

    def __call__(self,x):
        return self.ppoly(x)

    @staticmethod
    def _OP(op,lhs,rhs=None,**kargs):
        if isinstance(lhs,PiecewisePoly) and isinstance(rhs,PiecewisePoly):
            assert np.allclose(lhs.x, rhs.x)
            R = copy.deepcopy(lhs)
            if op in ['+','-']:
                assert lhs.ppoly.c.shape == rhs.c.shape
                R.ppoly.c = eval('lhs.c'+op+'rhs.c')
            elif op == '*':
                s0 = lhs.ppoly.c.shape
                s1 = rhs.ppoly.c.shape
                assert s0[1]==s1[1]
                s = (s0[0]+s1[0]-1, s0[1])
                R.ppoly.c = np.zeros(shape=s,dtype=lhs.c.dtype)
                for i in xrange(s[1]):
                    R.ppoly.c[:,i] = poly.polymul(lhs.c[:,i], rhs.c[:,i])
            else:
                raise RuntimeError('Not implemented yet!')
            return R
        elif isinstance(lhs,PiecewisePoly) and (rhs is None or np.isscalar(rhs)):
            R = copy.deepcopy(lhs)
            s = lhs.ppoly.c.shape
            if op == '**':
                R.ppoly.c = np.zeros(shape=((s[0]-1)*int(rhs)+1, s[1]), dtype=lhs.c.dtype)
                for i in xrange(s[1]):
                    R.ppoly.c[:,i] = poly.polypow(lhs.c[:,i], rhs)
            else:
                R.ppoly.c = eval('lhs.c'+op+'rhs')
            return R
        else:
            raise RuntimeError('OP only between PP and PP or PP and scalar!')
    
    @staticmethod    
    def from_spline(f,extrapolate=True):
        return PiecewisePoly(interp.PPoly.from_spline(f,extrapolate))

    def __add__(self,other):
        return PiecewisePoly._OP('+',self,other)
    def __sub__(self,other):
        return PiecewisePoly._OP('-',self,other)
    def __mul__(self,other):
        return PiecewisePoly._OP('*',self,other)
    def __div__(self,other):
        return PiecewisePoly._OP('/',self,other)
    def __pow__(self,p):
        return PiecewisePoly._OP('**',self,p)

    def derivative(self,m=1):
        return PiecewisePoly(self.ppoly.derivative(m))

    def roots(self,discontinuity=False,extrapolate=None):
        return self.ppoly.roots(discontinuity,extrapolate)

    def as_objective_function(self):
        xmin = xmin if xmin is not None else self.xmin()
        xmax = xmax if xmax is not None else self.xmax()
        dF = self
        derivatives = [FunctorTensor(dF)]
        for k in xrange(3):
            dF = dF.derivative()
            derivatives.append(FunctorTensor(dF))
        return ObjectiveFunction(derivatives)


    def xmin(self):
        return self.ppoly.x[0]
    def xmax(self):
        return self.ppoly.x[-1]

    def plot(self,xmin=None,xmax=None,N=1000):
        xmin = self.xmin() if xmin is None else xmin
        xmax = self.xmax() if xmax is None else xmax
        x = np.linspace(xmin,xmax,N)
        plt.plot(x,self.ppoly.__call__(x), 'r')


if __name__ == '__main__':
    f0   = lambda x: np.cos(x)
    df0  = lambda x: -np.sin(x)
    d2f0 = lambda x: -np.cos(x)

    f1   = lambda x: np.sin(x)
    df1  = lambda x: np.cos(x)
    d2f1 = lambda x: -np.sin(x)

    f2 = lambda x: np.tan(x)
    f3 = lambda x: np.exp(x)

    F0     = lambda x,y: f0(x)*f1(y)
    dF0_x  = lambda x,y: df0(x)*f1(y)
    dF0_y  = lambda x,y: f0(x)*df1(y)    
    dF0_xx = lambda x,y: d2f0(x)*f1(y)
    dF0_yy = lambda x,y: df0(x)*d2f1(y)
    dF0_xy = lambda x,y: df0(x)*df1(y)
    dF0_yx = dF0_xy
    
    ft0 = FunctorTensor([f0])
    ft1 = FunctorTensor([f0,f1])
    ft2 = FunctorTensor([f0,f1,f2,f3],shape=(2,2))

    FT0   = FunctorTensor([F0],nvars=2)
    dFT0  = FunctorTensor([dF0_x,dF0_y],nvars=2,shape=(1,2))
    d2FT0 = FunctorTensor([dF0_xx, dF0_xy, dF0_yx, dF0_yy],shape=(2,2),nvars=2)

    O0 = ObjectiveFunction([FT0,dFT0,d2FT0])
    dO0_x = O0.take_derivative(0)
    dO0_y = O0.take_derivative(1)

    X = np.linspace(0.0,1.0,4)

    print ft0(0.0)
    print ft1(0.5)
    print ft2(1.0)
    print
    print ft0(X)
    print ft1(X)
    print ft2(X)
    print
    print (FT0*5.0+FT0)(X,X)
    print (FT0*4+ft0/4+1)(X,X,X)
    print
    print FT0(X,X)   - O0.F(X,X)
    print dFT0(X,X)  - O0.dF(X,X)
    print d2FT0(X,X) - O0.d2F(X,X)
    print
    print dO0_x.F(X,X)  - dF0_x(X,X)
    print dO0_y.F(X,X)  - dF0_y(X,X)
    print dO0_x.dF(X,X) - [dF0_xx(X,X),dF0_xy(X,X)]
    print dO0_y.dF(X,X) - [dF0_xy(X,X),dF0_yy(X,X)]

