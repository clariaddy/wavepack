
import copy

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

import scipy.interpolate as interp
import scipy.optimize
import numpy.polynomial.polynomial as poly

class PiecewisePoly:

    def __init__(self,ppoly):
        assert isinstance(ppoly, interp.PPoly)
        self.ppoly = ppoly

    def __call__(self,x,mode='extend',xmin=None,xmax=None):
        xmin = self.ppoly.x[0]  if xmin is None else xmin
        xmax = self.ppoly.x[-1] if xmax is None else xmax
        assert mode in ['extend','zeros','periodic']
        assert xmax > xmin
        
        f = self.ppoly.__call__
        if mode == 'extend':
            Px = f(x)
        elif mode == 'zeros':
            cond = np.logical_and(x>=xmin,x<=xmax)
            Px = np.piecewise(x, [cond, ~cond], [f,0.0])
        elif mode == 'periodic':
            L = xmax - xmin
            x = xmin + np.mod(x-xmin,L)
            Px = f(x)
        else:
            assert False
        return Px

    @staticmethod
    def _OP(op,lhs,rhs=None,**kargs):
        if isinstance(lhs,PiecewisePoly) and isinstance(rhs,PiecewisePoly):
            assert np.allclose(lhs.x, rhs.x)
            R = copy.deepcopy(lhs)
            if op in ['+','-']:
                assert lhs.ppoly.c.shape == rhs.c.shape
                R.ppoly.c = eval('lhs.c'+op+'rhs.c')
            elif op == '*':
                s0 = lhs.ppoly.c.shape
                s1 = rhs.ppoly.c.shape
                assert s0[1]==s1[1]
                s = (s0[0]+s1[0]-1, s0[1])
                R.ppoly.c = np.zeros(shape=s,dtype=lhs.c.dtype)
                for i in xrange(s[1]):
                    R.ppoly.c[:,i] = poly.polymul(lhs.c[:,i], rhs.c[:,i])
            else:
                raise RuntimeError('Not implemented yet!')
            return R
        elif isinstance(lhs,PiecewisePoly) and (rhs is None or np.isscalar(rhs)):
            R = copy.deepcopy(lhs)
            s = lhs.ppoly.c.shape
            if op == '**':
                R.ppoly.c = np.zeros(shape=((s[0]-1)*int(rhs)+1, s[1]), dtype=lhs.c.dtype)
                for i in xrange(s[1]):
                    R.ppoly.c[:,i] = poly.polypow(lhs.c[:,i], rhs)
            else:
                R.ppoly.c = eval('lhs.c'+op+'rhs')
            return R
        else:
            raise RuntimeError('OP only between PP and PP or PP and scalar!')
    
    @staticmethod    
    def from_spline(f,extrapolate=True):
        return PiecewisePoly(interp.PPoly.from_spline(f,extrapolate))

    def __add__(self,other):
        return PiecewisePoly._OP('+',self,other)
    def __sub__(self,other):
        return PiecewisePoly._OP('-',self,other)
    def __mul__(self,other):
        return PiecewisePoly._OP('*',self,other)
    def __div__(self,other):
        return PiecewisePoly._OP('/',self,other)
    def __pow__(self,p):
        return PiecewisePoly._OP('**',self,p)

    def derivative(self,m=1):
        return PiecewisePoly(self.ppoly.derivative(m))

    def roots(self,discontinuity=True,extrapolate=None):
        return self.ppoly.roots(discontinuity,extrapolate)

    def plot(self,xmin=None,xmax=None,N=1000):
        xmin = self.xmin if xmin is None else xmin
        xmax = self.xmax if xmax is None else xmax
        x = np.linspace(xmin,xmax,N)
        plt.plot(x,self.ppoly.__call__(x), 'r')


class ObjectiveFunction():
    def __init__(self, xmin,xmax,f,df=None,d2f=None):
        self.xmin = xmin
        self.xmax = xmax

        self.f   = f
        self.df  = df
        self.d2f = d2f

        assert self.good(), 'Bad parameters!'
    
    @staticmethod
    def spline_interp(X,Y,deg=3):
        xmin = X[0]
        xmax = X[-1]
        f = interp.splrep(X,Y,s=0,k=deg)
        return ObjectiveFunction.from_spline(xmin,xmax,f)
    
    @staticmethod
    def from_spline(xmin,xmax,f):
        P   = PiecewisePoly.from_spline(f, extrapolate=True)
        dP  = P.derivative()
        d2P = dP.derivative()
        return ObjectiveFunction(xmin,xmax,P,dP,d2P)

    def good(self):
        return (self.xmin is not None and self.xmax is not None and self.xmax>self.xmin and self.f is not None)
    def has_grad(self):
        return self.df is not None
    def has_hessian(self):
        return self.d2f is not None
    def dlevel(self):
        return int(self.good()) + int(self.good() and self.has_grad()) + int(self.good() and self.has_grad() and self.has_hessian())



class TimedelayOptimizer:

    def __init__(self,F0,F1,N=200,alpha=1.0,W=None):
        assert isinstance(F0, ObjectiveFunction) and isinstance(F1,ObjectiveFunction), 'F0 and F1 should be of type ObjectiveFunction !'
        assert W is None or isinstance(W, ObjectiveFunction), 'The weight function W should be of type ObjectiveFunction !'
        assert np.allclose([F0.xmin,F0.xmax], [F1.xmin, F1.xmax])

        self.F0   = F0
        self.F1   = F1
        self.N    = N

        if W is None:
            self.W = ObjectiveFunction(0.0,1.0,lambda x,*a: 1.0, lambda x,*a: 0.0, lambda x,*a: 0.0)
        else:
            self.W = W
        assert self.W.good(), 'W is not initialized!'

        self.dlevel = min(self.F0.dlevel(), self.F1.dlevel(), self.W.dlevel())
        assert self.dlevel >=1, 'dlevel == 0...'
        
        self.f0   = self.F0.f
        self.df0  = self.F0.df
        self.d2f0 = self.F0.d2f
        
        self.f1   = self.F1.f
        self.df1  = self.F1.df
        self.d2f1 = self.F1.d2f
        
        self.w   = self.W.f
        self.dw  = self.W.df
        self.d2w = self.W.d2f

    def mse(self,t,tau,*a):
        tp = t+0.5*tau
        tm = t-0.5*tau
        return ( self.f1(tp,*a)-self.f0(tm,*a) )**2
    def dmse(self,t,tau,*a):
        tp = t+0.5*tau
        tm = t-0.5*tau
        return (self.df1(tp,*a)+self.df0(tm,*a))*(self.f1(tp,*a)-self.f0(tm,*a))
    def d2mse(self,t,tau,*a):
        tp = t+0.5*tau
        tm = t-0.5*tau
        return 0.5*( (self.d2f1(tp,*a)-self.d2f0(tm,*a)) * (self.f1(tp,*a)-self.f0(tm,*a)) + self.df1(tp,*a)**2 - self.df0(tm,*a)**2)
    def phi(self,tau,*a):
        return self.w(tau,*a)*self.I(self.mse,tau,*a)
    def dphi(self,tau,*a):
        return self.dw(tau,*a)*self.I(self.mse,tau,*a) + self.w(tau,*a)*self.I(self.dmse,tau,*a)
    def d2phi(self,tau,*a):
        return self.d2w(tau,*a)*self.I(self.mse,tau,*a) + 2.0*self.dw(tau,*a)*self.I(self.dmse,tau,*a) + self.w(tau,*a)*self.I(self.d2mse,tau,*a)
    def I(self,f,tau,*a):
        return np.asarray([sp.integrate.quad(func=lambda t: f(t,T,*a), a=self.Ixmin, b=self.Ixmax, limit=5*self.N)[0] for T in tau])

    def optimize(self, tol, method=0, tau0=0.0, max_tau=None, options={'disp':True}, fkargs={}, Ixmin=None, Ixmax=None, callback=None):
        normal_methods = ['Nelder-Mead','BFGS','Newton-CG']
        bounded_methods = ['L-BFGS-B','TNC','SLSQP']
        dmethods = normal_methods if max_tau is None else bounded_methods
        
        msg = 'Method should be one of the following:'+ ', '.join(['{}={}'.format(i,dmethods[i]) for i in xrange(len(dmethods))])
        assert method >= 0 and method <len(dmethods), msg
        
        kargs = {}

        bounded = (max_tau is not None)
        if bounded:
            assert max_tau > 0.0
            assert self.dlevel >=2, 'Cannot optimize with method {} because it needs at least gradient informations !'.format(dmethods[method])
            kargs['bounds'] = [(-max_tau,max_tau)]
            kargs['jac']    = self.dphi
        else:
            assert self.dlevel > method, 'Cannot optimize with method {} because it needs more derivatives informations !'.format(dmethods[method])
            if method >= 2:
                kargs['jac']    = self.dphi
            if method >= 3:
                kargs['hess']   = self.d2phi

        self.Ixmin = fkargs['xmin'] if Ixmin is None else Ixmin
        self.Ixmax = fkargs['xmax'] if Ixmax is None else Ixmax
        assert np.isscalar(self.Ixmin)
        assert np.isscalar(self.Ixmax)
        assert np.isscalar(self.Ixmin < self.Ixmax)
        
        args = (fkargs['mode'],fkargs['xmin'],fkargs['xmax'])
        assert isinstance(args[0],str)
        assert np.isscalar(args[1])
        assert np.isscalar(args[2])

        kargs['fun']      = self.phi
        kargs['args']     = args
        kargs['x0']       = tau0
        kargs['tol']      = tol
        kargs['method']   = dmethods[method]
        kargs['options']  = options
        kargs['callback'] = callback
        
        return sp.optimize.minimize(**kargs)

    @staticmethod
    def _W(alpha,xmin,xmax):
        return lambda tau: 1.0 + 0.5*(tau/(alpha*(xmax-xmin)))**2
    @staticmethod
    def _dW(alpha,xmin,xmax):
        return lambda tau: (alpha*(xmax-xmin))**(-2) * tau
    @staticmethod
    def _d2W(alpha,xmin,xmax):
        return lambda tau: (alpha/(xmax-xmin))**(-2)
    @staticmethod
    def _default_W(alpha,xmin,xmax):
        return ObjectiveFunction(f  =TimedelayOptimizer._W(alpha,xmin,xmax), 
                                 df =TimedelayOptimizer._dW(alpha,xmin,xmax),
                                 d2f=TimedelayOptimizer._d2W(alpha,xmin,xmax),
                                 xmin=xmin, xmax=xmax)



if __name__ == '__main__':
    
    f0 = lambda t: np.cos(2.0*np.pi*t)
    f1 = lambda t: np.sin(2.0*np.pi*t)
    f0 = np.vectorize(f0)
    f1 = np.vectorize(f1)
    
    Tmin = 0.0
    Tmax = 1.0
    
    #spline nodes
    n    = 20
    t,dt  = np.linspace(Tmin, Tmax, n, retstep=True)
    
    #plot nodes
    N    = 1000
    T    = np.linspace(Tmin, Tmax, N)
    
    #sampled data
    X0    = f0(t)
    X1    = f1(t)
    
    #interpolate with spline and compute first and second derivatives
    deg=3
    F0 = ObjectiveFunction.spline_interp(t,X0,deg)
    F1 = ObjectiveFunction.spline_interp(t,X1,deg)
    
    #build objective function phi(tau) = W(tau) * integral(Tmin,Tmax) ||f1(t+0.5*tau)-f0(t-0.5*tau)||^2 dt
    #default penality function: W(tau) = (1+alpha*tau**2) with alpha=1
    optimizer = TimedelayOptimizer(F0,F1,alpha=1.0)

    #find offset tau wich minimizes the objective func
    # level=1 estimate gradient locally
    # level=2 use real gradient
    # level=3 include second derivative
    level = 2
    tau = optimizer.optimize(level).x
    

    #plot everything
    fig = plt.figure()
    
    p=410
    plt.subplot(p+1)
    plt.plot(t,X0,'ro')
    plt.plot(t,X1,'bo')
    plt.plot(T,f0(T),'r',alpha=0.25)
    plt.plot(T,f1(T),'b',alpha=0.25)

    plt.subplot(p+2)
    plt.plot(T,F0.f(T),'r')
    plt.plot(T,F1.f(T),'b')
    plt.ylim([-1.0,1.0])

    plt.subplot(p+3)
    plt.plot(T,F0.f(T),'r',alpha=0.25)
    plt.plot(T,F1.f(T),'b',alpha=0.25)
    plt.plot(T,F0.f(T-0.5*tau),'r--')
    plt.plot(T,F1.f(T+0.5*tau),'b--')

    plt.subplot(p+4)
    plt.plot(T,F1.f(T+0.5*tau)-F0.f(T-0.5*tau),'g')

    plt.show()
