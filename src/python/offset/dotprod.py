
import sys, copy
import numpy as np
import scipy as sp

import functional

class BilinearObjectiveFunctionGenerator(object):
        
    def _B(self,f,g,merge_vars):
        raise NotImplementedError("Class {} doesn't implement _B(f,g)".format(self.__class__.__name__))

    def B(self,f,g,merge_vars=False):
        self._check(f,g)
        return self._B(f,g,merge_vars)

    def _B_require_dlevel(self):
        return 0
    def _check(self,f,g):
        d0 = self._B_require_dlevel()
        if d0<0:
            raise RuntimeError('Required derivative level is negative...')
        else:
            if not isinstance(f,functional.ObjectiveFunction):
                raise ValueError('f in not an functional.ObjectiveFunction !')
            elif f.dlevel() < d0:
                raise ValueError('f has not enough dlevel !')
            if not isinstance(g,functional.ObjectiveFunction):
                raise ValueError('g in not an functional.ObjectiveFunction !')
            elif g.dlevel() < d0:
                raise ValueError('f has not enough dlevel !')
    
    def gen_objective_function(self,F,G,merge_vars=True):
        if not isinstance(F,functional.ObjectiveFunction):
            raise ValueError('F in not an functional.ObjectiveFunction !')
        if not isinstance(G,functional.ObjectiveFunction):
            raise ValueError('G in not an ObjectiveFunction !')

        k0 = self._B_require_dlevel()
        kmax = min(F.dlevel(),G.dlevel())
        if kmax<k0:
            raise ValueError('kmax<k0...')
        else:
            kk=kmax-k0

        if merge_vars:
            if F.nvars != G.nvars:
                raise ValueError('Trying to merge arguments but number of arguments differs!')
            nvars=F.nvars
            b = lambda f,g: self.B(f,g,merge_vars=True)
        else:
            if F.nvars != G.nvars:
                raise ValueError('Trying to merge arguments but number of arguments differs!')
            nvars=F.nvars
            b = lambda f,g: self.B(f,g,merge_vars=False)

        dB0 = lambda *args: b(F,G)(*args)
        dFs = [[F]]
        dGs = [[G]]
        for k in xrange(kk):
            dFk = []
            dGk = []
            for dFi in dFs[-1]:
                for j in xrange(F.nvars):
                    dFk.append(dFi.take_derivative(j))
            for dGi in dGs[-1]:
                for j in xrange(G.nvars):
                    dGk.append(dGi.take_derivative(j))
            dFs.append(dFk)
            dGs.append(dGk)
        
        if ((merge_vars and nvars!=1) or (not merge_vars and nvars!=1)):
            raise NotImplementedError('Not implemented yet for nvars n>=1!')
       
        dBs = []
        for k in xrange(0,kk+1):
            dBk = []
            base = 'lambda *args: '
            dBkn = base
            for i in xrange(0,k+1):
                j = k-i
                dBkn  += '+ {Ck}*b(dFs[{i}][0],dGs[{j}][0])(*args)'.format(Ck=sp.special.binom(k,i),i=i,j=j)
            dBkn = eval(dBkn, locals())
            dBk.append(dBkn)
            dBs.append(dBk)
        
        #for i in xrange(nvars):
            #if kk<1:break
            #dF_i = F.take_derivative(i)
            #dG_i = G.take_derivative(i)
            #dBi = lambda *args: b(dF_i,G)(*args) + b(F,dG_i)(*args)
            #for j in xrange(nvars):
                #if kk<2:break
                #dF_j  = F.take_derivative(j)
                #dG_j  = G.take_derivative(j)
                #dF_ij = dF_i.take_derivative(j)
                #dG_ij = dG_i.take_derivative(j)
                #dBij = lambda *args: b(dF_ij,G   )(*args) + b(dF_i,dG_j )(*args) \
                                   #+ b(dF_j ,dG_i)(*args) + b(F,   dG_ij)(*args)
                #for k in xrange(nvars):
                    #if kk<3:break
                    #assert kk==3,'Not implemented yet!'
                    #dF_k  = F.take_derivative(k)
                    #dG_k  = G.take_derivative(k)
                    #dF_ik  = dF_i.take_derivative(k)
                    #dG_ik  = dG_i.take_derivative(k)
                    #dF_jk  = dF_j.take_derivative(k)
                    #dG_jk  = dG_j.take_derivative(k)
                    #dF_ijk = dF_ij.take_derivative(k)
                    #dG_ijk = dG_ij.take_derivative(k)
                    #dBijk = lambda *args: b(dF_ijk,G    )(*args) + b(dF_ij,dG_k  )(*args) \
                                        #+ b(dF_ik ,dG_j )(*args) + b(dF_i ,dG_jk )(*args) \
                                        #+ b(dF_jk ,dG_i )(*args) + b(dF_j ,dG_ik )(*args) \
                                        #+ b(dF_k  ,dG_ij)(*args) + b(F    ,dG_ijk)(*args)
                    #dBijks.append(dBijk)
                #dBijs.append(dBij)
            #dBis.append(dBi)
       
        def shape(i):
            assert i>=0
            if   i==0: return None
            elif i==1: return (1,nvars)
            else:      return (nvars,)*i
        dB = [ functional.FunctorTensor(dBi, nvars=nvars, shape=shape(i)) for i,dBi in enumerate(dBs)]
        return functional.ObjectiveFunction(dB)


    def __call__(self,F,G,merge_vars=True):
        return self.gen_objective_function(F,G,merge_vars=merge_vars)


class DotProduct(BilinearObjectiveFunctionGenerator):
    pass

class Product(DotProduct):

    def _B(self, F,G,merge_vars):

        if merge_vars:
            return lambda *args: F.F(*args)*G.F(*args)
        else:
            def Args(i,args): return (arg[i] for arg in args)
            return lambda *args: F.F(*Args(0,args))*G.F(*Args(1,args))

class SobolevDotProduct(DotProduct):
    
    def __init__(self,k):
        self.configure(k)

    def configure(self,k,weights=None):
        if not isinstance(k,int) or k<0:
            raise ValueError('k int not an int or k<1 !')
        self.k=k
        if weights is None:
            self.weights = np.ones(k+1)
        else:
            self.weights = weights

    def _B_require_dlevel(self):
        return self.k

    def _B(self,F,G,merge_vars=True):
                
        def args(i): return '*(arg[{}] for arg in args)'.format(i)
        
        def df(fun,i):
            if merge_vars:
                return '(({}.DF({i}))(*args))'.format(fun,i=i)
            else:
                if fun=='F':
                    return '((F.DF({}))({}))'.format(i,args(0))
                elif fun=='G':
                    return '((G.DF({}))({}))'.format(i,args(1))

        def sdot(i):
            return ' self.weights[{i}] * {dfi} * {dgi} '.format(i=i,dfi=df('F',i),dgi=df('G',i))
        
        slambda = 'lambda *args: ' + '+'.join([sdot(i) for i in xrange(self.k+1)])
        return eval(slambda,locals())


    
if __name__ == '__main__':
   
    f   = lambda x: x**3
    df  = lambda x: 3*x**2
    d2f = lambda x: 6*x
    d3f = lambda x: 6
    F    = functional.ObjectiveFunction([functional.FunctorTensor([ff]) for ff in [f,df,d2f,d3f]])   

    g   = lambda y: y**3   -y**2
    dg  = lambda y: 3*y**2 -2*y
    d2g = lambda y: 6*y    -2
    d3g = lambda y: 6
    G    = functional.ObjectiveFunction([functional.FunctorTensor([ff]) for ff in [g,dg,d2g,d3g]])   

    x = np.linspace(0,10,100)
    y = np.linspace(10,20,100)

    B = Product()
    b = B(F,G)
    print np.allclose(b. F(x) , f(x)*g(x))
    print np.allclose(b.dF(x) , df(x)*g(x) + f(x)*dg(x))
    print np.allclose(b.d2F(x), f(x)*d2g(x) + 2.0*df(x)*dg(x) + d2f(x)*g(x))
    print np.allclose(b.DF(3)(x), f(x)*d3g(x) + 3.0*df(x)*d2g(x) + 3.0*d2f(x)*dg(x) + d3f(x)*g(x))
    print

    b = B(F,G,merge_vars=False)
    print np.allclose(b. F((x,y)) , f(x)*g(y))
    print np.allclose(b.dF((x,y)) , df(x)*g(y) + f(x)*dg(y))
    print np.allclose(b.d2F((x,y)), f(x)*d2g(y) + 2.0*df(x)*dg(y) + d2f(x)*g(y))
    print np.allclose(b.DF(3)((x,y)), f(x)*d3g(y) + 3.0*df(x)*d2g(y) + 3.0*d2f(x)*dg(y) + d3f(x)*g(y))
    print

    B = SobolevDotProduct(0)
    b = B(F,G)
    print np.allclose(b. F(x) , f(x)*g(x))
    print np.allclose(b.dF(x) , df(x)*g(x) + f(x)*dg(x))
    print np.allclose(b.d2F(x), f(x)*d2g(x) + 2.0*df(x)*dg(x) + d2f(x)*g(x))
    print np.allclose(b.DF(3)(x), f(x)*d3g(x) + 3.0*df(x)*d2g(x) + 3.0*d2f(x)*dg(x) + d3f(x)*g(x))
    print
    b = B(F,G,merge_vars=False)
    print np.allclose(b. F((x,y)) , f(x)*g(y))
    print np.allclose(b.dF((x,y)) , df(x)*g(y) + f(x)*dg(y))
    print np.allclose(b.d2F((x,y)), f(x)*d2g(y) + 2.0*df(x)*dg(y) + d2f(x)*g(y))
    print np.allclose(b.DF(3)((x,y)), f(x)*d3g(y) + 3.0*df(x)*d2g(y) + 3.0*d2f(x)*dg(y) + d3f(x)*g(y))
    print
    
    B = SobolevDotProduct(1)
    b = B(F,G,merge_vars=False)
    print np.allclose(b. F((x,y)) , f(x)*g(y) + df(x)*dg(y))
    print np.allclose(b.dF((x,y)) , df(x)*g(y) + f(x)*dg(y) + d2f(x)*dg(y) + df(x)*d2g(y))
    print np.allclose(b.d2F((x,y)), d2f(x)*g(y) + df(x)*dg(y) + df(x)*dg(y) + f(x)*d2g(y) + d3f(x)*dg(y) + d2f(x)*d2g(y)+ d2f(x)*d2g(y) + df(x)*d3g(y))
    print
    
    B = SobolevDotProduct(2)
    b = B(F,G)
    print np.allclose(b. F(x) , f(x)*g(x) + df(x)*dg(x) + d2f(x)*d2g(x))
    print np.allclose(b.dF(x) , df(x)*g(x) + d2f(x)*dg(x) + d3f(x)*d2g(x) + f(x)*dg(x) + df(x)*d2g(x) + d2f(x)*d3g(x))
    print


    #f     = lambda x,y: x*x*y*(1-y)
    #df_x  = lambda x,y: 2*x*y*(1-y)
    #df_y  = lambda x,y: x*x*(1-2*y)
    #df_xx = lambda x,y: 2*y*(1-y)
    #df_xy = lambda x,y: 2*x*(1-2*y)
    #df_yx = lambda x,y: 2*x*(1-2*y)
    #df_yy = lambda x,y: -2*x*x
    #ff = []
    #ff.append(functional.FunctorTensor([f],nvars=2))
    #ff.append(functional.FunctorTensor([df_x,df_y],nvars=2,shape=(1,2)))
    #ff.append(functional.FunctorTensor([df_xx,df_xy,df_yx,df_yy],nvars=2,shape=(2,2)))
    #F = functional.ObjectiveFunction(ff)
    
    #g     = lambda y,x: x*x*y*(1-y)
    #dg_x  = lambda y,x: 2*x*y*(1-y)
    #dg_y  = lambda y,x: x*x*(1-2*y)
    #dg_xx = lambda y,x: 2*y*(1-y)
    #dg_xy = lambda y,x: 2*x*(1-2*y)
    #dg_yx = lambda y,x: 2*x*(1-2*y)
    #dg_yy = lambda y,x: -2*x*x
    #gg = []
    #gg.append(functional.FunctorTensor([g],nvars=2))
    #gg.append(functional.FunctorTensor([dg_x,dg_y],nvars=2,shape=(1,2)))
    #gg.append(functional.FunctorTensor([dg_xx,dg_xy,dg_yx,dg_yy],nvars=2,shape=(2,2)))
    #G = functional.ObjectiveFunction(gg)

    #b = B(F,G)
    #print np.allclose(b. F(x,y) , f(x,y)*g(x,y))
    

    #print df_x(1,2)*g(1,2) + f(1,2)*dg_x(1,2)
    #print df_y(1,2)*g(1,2) + f(1,2)*dg_y(1,2)
    #print b.dF(1,2)







