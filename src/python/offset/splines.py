
import os
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import scipy.signal as signal
import numpy.polynomial.polynomial as poly

#local includes
import env
import wavepack as wp
import offset

### input variables and configuration ###
# 
# source data files and names
data_dir        = env.data_dir
plot_script_dir = env.plot_script_dir

data_src  = [ ('CO2.txt','CO2'        ,0,1),    # format = (file_name, arbitrary_data_name, time_column_id, data_column_id)
              ('ATS.txt','Temperature',0,2) ]   # warning: column ids begin from 0

# interpolation parameters
normalize       = True
remove_tendency = False
tendency_order  = 1
N               = 2**10       #resampled points count
timescaling     = 1.0/1000.0 #to kyrs

# CWT parameters

wave_family = wp.WaveletFamily('cmor')
wave        = wave_family.wavelet({'omega0':6.0}) 
#wave_family = wp.WaveletFamily('paul')
#wave        = wave_family.wavelet({'m':10.0})

dj     = 0.01    # 1/dj suboctave per octave
jmin   = None    # if None, default jmin value is computed
jmax   = None    # if None, default jmax value is computed

# XWT parameters
smoothing_kernel = np.mean
smoothing_window = (7,11)
sigma=10.0

# confidense levels
p_level = 0.80   # confidense level probability
monte_carlo_batch_size  = 16
monte_carlo_batch_count = 32

# plot parameters
show_plots = False
save_plots = True
save_dir   = os.getcwd() + '/splines' 
plot_ext = 'png'

time_label  = r'time $(kyrs)$'
data0_label = r'CO2 $(ppmv)$'
data1_label = r'Temperature $(^{\circ}C)$'
udata0_label = r'Unity Normalized CO2'
udata1_label = r'Unity Normalized Temperature'

timedelay_label=r'weighted time delay'
phaseshift_label=r'weighted phaseshift'
data0_color = 'r'
data1_color = 'b'
plottitle_kargs = { 'fontsize':18, 'color':'r' }

plot_scaling = 3.0
plt.rcParams['figure.figsize'] = [plot_scaling*x for x in plt.rcParams['figure.figsize']]
#
### end of configuration ###


### print config ###
print \
"""
=== CONFIGURATION ===
    Inputs:
        data_dir -> {}
        data_src    -> {}
    Interpolation:
        remove_tendency  -> {}
        tendency_order   -> {}
        N (resampled pts)-> {}
        timescaling      -> {}
    CWT parameters:
        wave_family -> {}
        wavelet     -> {}
        dj          -> {}
        jmin        -> {}
        jmax        -> {}
    XWT parameters:
        smoothing_kernel -> {}
        smoothing_window -> {}
    Confidense levels:
        condidense level -> {}
        MT batch size    -> {}
        MT batch  count  -> {}
        MT sample count  -> {}
    Plotting:
        show_plots  -> {}
        save_plots  -> {}
        save_dir    -> {}
====================
""".format(data_dir, data_src, 
           remove_tendency, tendency_order, N, timescaling, 
           wave_family, wave, dj, jmin, jmax,
           smoothing_kernel, smoothing_window,
           p_level, monte_carlo_batch_count, monte_carlo_batch_size, monte_carlo_batch_count*monte_carlo_batch_size,
           show_plots, save_plots, save_dir)

monte_carlo_batch_size*monte_carlo_batch_count

### load input data ###
print '=== DATA PREPROCESSING ==='
print 'Loading data...',
fn0 = data_src[0][1]
fn1 = data_src[1][1]
assert fn0 != fn1, 'Data names should be differents! {} == {}'.format(fn0,fn1)

data = wp.DataSeries(dtype=float)
data.loadFromTxt(data_dir,data_src[0][0], {'t0':data_src[0][2], fn0:data_src[0][3]})
data.loadFromTxt(data_dir,data_src[1][0], {'t1':data_src[1][2], fn1:data_src[1][3]})
data0 = data[fn0]
data1 = data[fn1]
print 'done.'

# scale time and compute tmin and tmax
print 'Scaling time...',
t0 = data['t0']*timescaling
t1 = data['t1']*timescaling
tmin = max(t0[0],  t1[0])
tmax = min(t0[-1], t1[-1])
#wp.plot_script(plot_script_dir,'raw_inputs', globals(), locals())
print 'done'

# interpolation
print 'Interpolating on same timescales...'
interp = wp.TimeSeries(tmin, tmax, N, dtype=float)
interp.interpolate(fn0, t0, data0)
interp.interpolate(fn1, t1, data1)
wp.plot_script(plot_script_dir,'interpolation', globals(), locals())
times = interp['t']
data0 = interp[fn0]
data1 = interp[fn1]
dt=interp.dt
print interp
print

# normalize inputs
if normalize:
    print 'Normalizing inputs...',
    data0 = (data0 - np.mean(data0)) / np.std(data0)
    data1 = (data1 - np.mean(data1)) / np.std(data1)

    #data0s_ = sp.ndimage.filters.gaussian_filter1d(data0, sigma=sigma)
    #data1s_ = sp.ndimage.filters.gaussian_filter1d(data1, sigma=sigma)
    #wp.plot_script(plot_script_dir,'normalization', globals(), locals())
    #wp.plot_script(plot_script_dir,'renormalization_smoothed', globals(), locals())

# remove tendency
if remove_tendency:
    print 'Removing tendency...',
    model0     = np.polyfit(times, data0, tendency_order)
    predicted0 = np.polyval(model0, times)
        
    model1     = np.polyfit(times, data1, tendency_order)
    predicted1 = np.polyval(model1, times)

    model = (model0 + model1)/2.0
    predicted = np.polyval(model, times)
    
    olddata0   = data0
    data0      = data0 - predicted

    olddata1   = data1
    data1      = data1 - predicted
    
    wp.plot_script(plot_script_dir,'tendency_removal', globals(), locals())
    print 'done'

    print '''Tendency fitting with polynomial of order {}:
    {:12s}: fitted polynomial {}
    {:12s}: fitted polynomial {}
    => mean tendency = {}'''.format(tendency_order, fn0, model0, fn1, model1, model)

    if normalize:
        print 'Renormalizing inputs...',
        data0 = (data0 - np.mean(data0)) / np.std(data0)
        data1 = (data1 - np.mean(data1)) / np.std(data1)
        data0s = sp.ndimage.filters.gaussian_filter1d(data0, sigma=sigma)
        data1s = sp.ndimage.filters.gaussian_filter1d(data1, sigma=sigma)
        wp.plot_script(plot_script_dir,'renormalization', globals(), locals())
        print 'done'

print '=========================='


def time_delay(t,X0,X1,sigma,deg=3,level=2,alpha=1.0):
    assert X0.size == X1.size and t.size==X0.size, 'size mismatch'
    
    s = wp.TimeSeries.fromT(t)
    s.dic['X0'] = X0
    s.dic['X1'] = X1
    s = s.extend(ext='zeros') 
    #s = s.extend(ext='repeat') 
    t  = s['t']
    X0 = s['X0']
    X1 = s['X1']

    N = t.size

    F0 = offset.ObjectiveFunction.spline_interp(t,X0,deg)
    F1 = offset.ObjectiveFunction.spline_interp(t,X1,deg)

    X0s = sp.ndimage.filters.gaussian_filter1d(X0, sigma=sigma)
    X1s = sp.ndimage.filters.gaussian_filter1d(X1, sigma=sigma)
    F0s = offset.ObjectiveFunction.spline_interp(t,X0s,deg)
    F1s = offset.ObjectiveFunction.spline_interp(t,X1s,deg)

    optimizer = offset.TimedelayOptimizer(F0s,F1s,N=N,alpha=alpha)
    res = optimizer.optimize(level,options={})
    tau = res.x

    return (tau, t, F0, F1, F0s, F1s)

nfigs = 2
figs = [plt.figure() for i in xrange(nfigs)]

deg=3
level = 2
maxlevel = 6
max_plot_level = min(maxlevel,3)

t  = [times]
X0 = [data0/np.mean(np.abs(data0))]
X1 = [data1/np.mean(np.abs(data1))]
N  = t[0].size
dL = t[0][-1]-t[0][0]

sigma=N/16.0
alpha=1.0

l = 0
taus = np.empty(shape=(maxlevel+1,2**maxlevel),dtype=t[0].dtype)
while l<=maxlevel:
    print '== Level == {}:'.format(l)
    P = 2**l
    
    if l<= max_plot_level:
        subplots = []
        for fig in figs:
            plt.figure(fig.number)
            subplots.append([plt.subplot2grid((max_plot_level+1,2**max_plot_level), (l,i*2**(max_plot_level-l)), colspan=2**(max_plot_level-l)) for i in xrange(P)])

    tn  = [] 
    X0n = []
    X1n = []
    x_nodes = []
    tau_nodes = []
    for i in xrange(P):
        print '\tsubsample {}/{}: '.format(i+1,P),
        sys.stdout.flush()
        tau,tt,F0,F1,F0s,F1s = time_delay(t[i],X0[i],X1[i],sigma,deg,level,alpha)
        x_nodes.append((t[i][0]+t[i][-1])/2.0)
        tau_nodes.append(tau)
        print 'tau = {}'.format(tau)

        if l<= max_plot_level:
            subplots[1][i].plot(t[i],X0[i],'r')
            subplots[1][i].plot(t[i],X1[i],'b')
            subplots[1][i].set_xlim([t[i][0],t[i][-1]])

        P0 = F0.f - F0s.f
        P1 = F1.f - F1s.f
        X0[i] = P0(t[i]-tau*0.5)
        X1[i] = P1(t[i]-tau*0.5)
        #X0[i] = F0.f(t[i]-tau*0.5)
        #X1[i] = F1.f(t[i]-tau*0.5)

        tn  += np.split(t[i],2)
        X0n += np.split(X0[i],2)
        X1n += np.split(X1[i],2)

        if l<= max_plot_level:
            subplots[0][i].plot(tt,F0s.f(tt),'r')
            subplots[0][i].plot(tt,F1s.f(tt),'b')
            subplots[0][i].plot(tt,F0s.f(tt-0.5*tau),'r--')
            subplots[0][i].plot(tt,F1s.f(tt+0.5*tau),'b--')
            subplots[0][i].set_xlim([tt[0],tt[-1]])
        
            for fig in figs:
                fig.show()
                plt.figure(fig.number)
                plt.draw()
    print
    if l==0:
        taus[l,:] = tau
    elif l==1:
        ftau = sp.interpolate.splrep(x_nodes,tau_nodes,k=1)
        taus[l,:] = sp.interpolate.splev(np.linspace(tmin,tmax,taus.shape[1]), ftau)
    else:
        ftau = sp.interpolate.splrep(x_nodes,tau_nodes,k=3)
        taus[l,:] = sp.interpolate.splev(np.linspace(tmin,tmax,taus.shape[1]), ftau)
    
    sigma /= 2.0
    alpha /= 2.0
    N     /= 2.0
    dL    /= 2.0
    
    t  = tn
    X0 = X0n
    X1 = X1n
    l += 1

time_delay = taus.sum(axis=0)*1000
ftd = sp.interpolate.splrep(np.linspace(tmin,tmax,taus.shape[1]),time_delay,k=3)

fig = plt.figure()
plt.plot(times, sp.interpolate.splev(times,ftd))

plt.hold(True)
plt.show()


#plot everything

#p=410
#plt.subplot(p+1)
#plt.plot(tt,F0.f(tt),'r')
#plt.plot(tt,F1.f(tt),'b')

#plt.subplot(p+2)
#plt.plot(tt,F0s.f(tt),'r',alpha=0.25)
#plt.plot(tt,F1s.f(tt),'b',alpha=0.25)
#plt.plot(tt,F0s.f(tt-0.5*tau),'r--')
#plt.plot(tt,F1s.f(tt+0.5*tau),'b--')

#plt.subplot(p+3)
#plt.plot(tt,F0.f(tt-0.5*tau),'r--')
#plt.plot(tt,F1.f(tt+0.5*tau),'b--')

#plt.subplot(p+4)
#plt.plot(tt,P0(tt-0.5*tau),'r--')
#plt.plot(tt,P1(tt+0.5*tau),'b--')

#plt.show()
