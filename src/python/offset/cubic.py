
import numpy as np
import matplotlib.pyplot as plt

import scipy.sparse.linalg
import sktensor as skt

import scipy as sp
import scipy.interpolate

import functional

#csr_ prefix mandatory for scipy...
class csc_cubic_spline_matrix(sp.sparse.csc_matrix):

    valid_modes = ['periodic','clamped','elastic']

    def __init__(self,arg1=None,shape=None,dtype=None,copy=False,  X=None,mode=None,gen_grad_X=False):
        if X is not None or mode is not None:
            assert X is not None and mode is not None
            assert X.ndim==1
            assert mode in self.valid_modes
            N     = X.size
            dtype = X.dtype

            self.N = N
            self.mode = mode
            self.DX = 1.0 / (X[1:]-X[0:N-1]) #(1 / [X_(i+1) - X_i])
            self.DX2 = self.DX*self.DX       #(1 / [X_(i+1) - X_i])^2
            
            A = self._build_system(X, gen_grad_X)

            super(self.__class__,self).__init__(arg1=A)
        else:
            super(self.__class__,self).__init__(arg1=arg1,shape=shape,dtype=dtype,copy=copy)

    def _build_system(self,X,gen_grad_X):
        N  = self.N
        DX  = self.DX
        DX2 = self.DX2
        DX3 = DX*DX2
        mode = self.mode

        A_data = []
        A_idx  = []
        if gen_grad_X:
            dA_data = []
            dA_idx  = []
        
        mode = self.mode
        if mode == 'elastic' or mode == 'periodic':
            Dx0 = DX[0]
            A_idx   += [(0,0),(0,1)]
            A_data  += [2.0*Dx0,Dx0]
            if gen_grad_X:
                Dx0d = -1.0*DX2[0]  
                dA_idx  += [(0,0,0),(0,0,1),
                            (0,1,0),(0,1,1)]
                dA_data += [-2.0*Dx0d,+2.0*Dx0d,
                                -Dx0d,    +Dx0d]
        elif mode == 'clamped':
            Dxx0 = DX2[0]
            Dxx1 = DX2[1]
            A_idx   += [(0,0),(0,1),(0,2)]
            A_data  += [Dxx0,(Dxx0-Dxx1),-Dxx1]
            if gen_grad_X:
                Dxx0d = -2.0*DX3[0]
                Dxx1d = -2.0*DX3[1]
                dA_idx  += [(0,0,0),(0,0,1),
                            (0,1,0),(0,1,1),(0,1,2),
                            (0,2,1),(0,2,2)]
                dA_data += [-Dxx0d,+Dxx0d,
                            -Dxx0d,Dxx0d+Dxx1d,-Dxx1d,
                            +Dxx1d,-Dxx1d]
        else:
            raise NotImplemented('This mode has not been implemented yet!')
        
        for i in xrange(1,N-1):
            Dx0 = DX[i-1]  
            Dx1 = DX[i]  
            A_idx += [(i,i-1), (i,i) ,(i,i+1)]
            A_data    += [Dx0, 2.0*(Dx0+Dx1), Dx1]
            if gen_grad_X:
                Dx0d = -1.0*DX2[i-1]  
                Dx1d = -1.0*DX2[i]  
                dA_idx  += [(i,i-1,i-1),(i,i-1,i),
                            (i,i,i-1),(i,i,i),(i,i,i+1),
                            (i,i+1,i),(i,i+1,i+1)]
                dA_data += [-Dx0d,+Dx0d,
                            -2.0*Dx0d,2.0*(Dx0d-Dx1d),+2.0*Dx1d,
                            -Dx1d,+Dx1d]
    
        if mode == 'elastic' or mode == 'periodic':
            Dx0 = DX[-1]
            A_idx += [(N-1,N-2),(N-1,N-1)]
            A_data    += [Dx0,2.0*Dx0]
            if gen_grad_X:
                Dx0d = -1.0*DX2[-1]  
                dA_idx  += [(N-1,N-2,N-2),(N-1,N-2,N-1),
                            (N-1,N-1,N-2),(N-1,N-1,N-1)]
                dA_data += [    -Dx0d,    +Dx0d,
                            -2.0*Dx0d,+2.0*Dx0d]
        elif mode == 'clamped':
            Dxx0 = DX2[-2]
            Dxx1 = DX2[-1]
            A_idx  += [(N-1,N-3),(N-1,N-2),(N-1,N-1)]
            A_data += [Dxx0,(Dxx0-Dxx1),-Dxx1]
            if gen_grad_X:
                Dxx0d = -2.0*DX3[-2]
                Dxx1d = -2.0*DX3[-1]
                dA_idx  += [(N-1,N-3,N-3),(N-1,N-3,N-2),
                            (N-1,N-2,N-3),(N-1,N-2,N-2),(N-1,N-2,N-1),
                            (N-1,N-1,N-2),(N-1,N-1,N-1)]
                dA_data += [-Dxx0d,+Dxx0d,
                            -Dxx0d,Dxx0d+Dxx1d,-Dxx1d,
                            +Dxx1d,-Dxx1d]
    
        # build sparse matrix A
        assert len(A_idx) == len(A_data)
        shape = (N,N)
        arg1 = (np.asarray(A_data,dtype=X.dtype),np.asarray(A_idx,dtype=X.dtype).transpose())
        A = sp.sparse.coo_matrix(arg1=arg1,shape=shape, dtype=X.dtype)
        
        # build sparse 3D tensor dA
        if gen_grad_X:
            assert len(dA_idx) == len(dA_data)
            shape = (N,N,N)
            dA_idx = np.asarray(dA_idx,dtype=int).transpose()
            dA = skt.sptensor(subs=(dA_idx[0],dA_idx[1],dA_idx[2]), vals=dA_data, shape=shape, dtype=X.dtype)
            self.dA = dA

        return A
        
    def build_rhs(self, Y, gen_grad_X=False, gen_grad_Y=False):
        assert Y.size  == self.N
        assert Y.dtype == self.dtype
        N   = self.N
        DX  = self.DX
        DX2 = self.DX2
        DX3 = DX2*DX
        DX4 = DX3*DX
        DY  = Y[1:]-Y[0:N-1]
        rhs = np.empty(shape=(N), dtype=self.dtype)
        
        #Jacobian d(rhs[i])/dYj
        Jij   = []
        Jdata = []
        
        mode = self.mode
        if mode == 'elastic' or mode == 'periodic':
            D0 = DX2[0]
            Jij   += [(0,0),(0,1)]
            Jdata += [3.0*x for x in [-D0,+D0]]
            rhs[0]  = 3.0*DY[0]*D0
        elif mode == 'clamped':
            D0 = DX3[0]
            D1 = DX3[1]
            Jij   += [(0,0),(0,1),(0,2)]
            Jdata += [2.0*x for x in [-D0,D0+D1,-D1]]
            rhs[0]  = 2.0*(DY[0]*D0 - DY[1]*D1)
=======
        #Jacobians d(rhs[i])/dYj and d(rhs[i]/dXj)
        if gen_grad_Y:
            Jy_ij   = []
            Jy_data = []
        if gen_grad_X:
            Jx_ij   = []
            Jx_data = []
        
        mode = self.mode
        if mode == 'elastic' or mode == 'periodic':
            Dy0   = DY[0]
            Dxx0  = DX2[0]
            rhs[0]  = 3.0*Dy0*Dxx0
            if gen_grad_X:
                Dxx0d = -2.0*DX3[0]
                Jx_ij   += [(0,0),(0,1)]
                Jx_data += [3.0*x for x in [-Dy0*Dxx0d,+Dy0*Dxx0d]]
            if gen_grad_Y:
                Jy_ij   += [(0,0),(0,1)]
                Jy_data += [3.0*x for x in [-Dxx0,+Dxx0]]
        elif mode == 'clamped':
            Dy0    = DY[0]
            Dy1    = DY[1]
            Dxxx0  = DX3[0]
            Dxxx1  = DX3[1]
            rhs[0]  = 2.0*(Dy0*Dxxx0 - Dy1*Dxxx1)
            if gen_grad_X:
                Dxxx0d = -3.0*DX4[0]
                Dxxx1d = -3.0*DX4[1]
                Jx_ij   += [(0,0),(0,1),(0,2)]
                Jx_data += [2.0*x for x in [-Dy0*Dxxx0d,Dy0*Dxxx0d+Dy1*Dxxx1d,-Dy1*Dxxx1d]]
            if gen_grad_Y:
                Jy_ij   += [(0,0),(0,1),(0,2)]
                Jy_data += [2.0*x for x in [-Dxxx0,Dxxx0+Dxxx1,-Dxxx1]]
        else:
            raise NotImplemented('This mode has not been implemented yet!')

        for i in xrange(1,N-1):
            Dxx0  = DX2[i-1]
            Dxx1  = DX2[i]
            Dy0   = DY[i-1]
            Dy1   = DY[i]
            rhs[i] = 3.0*(Dy0*Dxx0 + Dy1*Dxx1)
            if gen_grad_X:
                Dxx0d = -2.0*DX3[i-1]
                Dxx1d = -2.0*DX3[i]
                Jx_ij   += [(i,i-1),(i,i),(i,i+1)]
                Jx_data += [3.0*x for x in [-Dy0*Dxx0d,+Dy0*Dxx0d-Dy1*Dxx1d,Dy1*Dxx1d]]
            if gen_grad_Y:
                Jy_ij   += [(i,i-1),(i,i),(i,i+1)]
                Jy_data += [3.0*x for x in [-Dxx0,Dxx0-Dxx1,+Dxx1]]

        mode = self.mode
        if mode == 'elastic' or mode == 'periodic':
            Dy0  = DY[-1]
            Dxx0 = DX2[-1]
            rhs[-1] = 3.0*Dy0*Dxx0
            if gen_grad_X:
                Dxx0d = -2.0*DX3[-1]
                Jx_ij   += [(N-1,N-2),(N-1,N-1)]
                Jx_data += [3.0*x for x in [-Dy0*Dxx0d,+Dy0*Dxx0d]]
            if gen_grad_Y:
                Jy_ij   += [(N-1,N-2),(N-1,N-1)]
                Jy_data += [3.0*x for x in [-Dxx0,+Dxx0]]
        elif mode == 'clamped':
            Dy0   = DY[-2]
            Dy1   = DY[-1]
            Dxxx0 = DX3[-2]
            Dxxx1 = DX3[-1]
            rhs[-1] = 2.0*(Dy0*Dxxx0 - Dy1*Dxxx1)
            if gen_grad_X:
                Dxxx0d = -3.0*DX4[-2]
                Dxxx1d = -3.0*DX4[-1]
                Jx_ij   += [(N-1,N-3),(N-1,N-2),(N-1,N-1)]
                Jx_data += [2.0*x for x in [-Dy0*Dxxx0d,Dy0*Dxxx0d+Dy1*Dxxx1d,-Dy1*Dxxx1d]]
            if gen_grad_Y:
                Jy_ij   += [(N-1,N-3),(N-1,N-2),(N-1,N-1)]
                Jy_data += [2.0*x for x in [-Dxxx0,Dxxx0+Dxxx1,-Dxxx1]]

        shape = (N,N)

        if gen_grad_X:
            assert len(Jx_ij) == len(Jx_data)
            Jx_id   = np.asarray(Jx_ij,dtype=int).transpose()
            Jx_rhs  = sp.sparse.csc_matrix((Jx_data,Jx_id),shape=shape,dtype=self.dtype)
        else:
            Jx_rhs = None

        for i in xrange(1,N-1):
            D0 = DX2[i-1]
            D1 = DX2[i]
            Jij   += [(i,i-1),(i,i),(i,i+1)]
            Jdata += [3.0*x for x in [-D0,D0-D1,+D1]]
            rhs[i] = 3.0*(DY[i-1]*D0 + DY[i]*D1)

        mode = self.mode
        if mode == 'elastic' or mode == 'periodic':
            D0 = DX2[-1]
            Jij   += [(N-1,N-2),(N-1,N-1)]
            Jdata += [3.0*x for x in [-D0,+D0]]
            rhs[-1] = 3.0*DY[-1]*D0
        elif mode == 'clamped':
            D0 = DX3[-2]
            D1 = DX3[-1]
            Jij   += [(N-1,N-3),(N-1,N-2),(N-1,N-1)]
            Jdata += [2.0*x for x in [-D0,D0+D1,-D1]]
            rhs[-1] = 2.0*(DY[-2]*D0 - DY[-1]*D1)

        assert len(Jij) == len(Jdata)
        shape = (N,N)
        Jrhs = sp.sparse.csc_matrix((Jdata,np.asarray(Jij,dtype=self.dtype).transpose()),shape=(N,N),dtype=self.dtype)
=======
        if gen_grad_Y:
            assert len(Jy_ij) == len(Jy_data)
            Jy_id   = np.asarray(Jy_ij,dtype=int).transpose()
            Jy_rhs  = sp.sparse.csc_matrix((Jy_data,Jy_id),shape=shape,dtype=self.dtype)
        else:
            Jy_rhs = None
        
        return rhs, Jx_rhs, Jy_rhs, DY


    def _inv(self):
        return np.asarray(sp.sparse.linalg.inv(self).todense(), dtype=self.dtype)

    def _gen_matrix(self):
        return self.todense()

    def __str__(self):
        np.set_printoptions(formatter={'float': '{: 0.2f}'.format})
        return self._gen_matrix().__str__()


class CubicSplineInterpolator(object):
            
    _k = 3

    def __init__(self,X=None,mode='elastic'):
        if X is not None:
            self.configure(X,mode)
        else:
            self.configured = False

    def configure(self,X,mode,gen_grad_X=False):
        assert X.ndim == 1
        assert X.size >= 4
        N     = X.size
        dtype = X.dtype

        if mode=='periodic':
            _k = self._k
            L = X[-1]-X[0]
            X = np.concatenate((X[-_k:-1]-L,X,X[1:_k]+L))
            N = X.size
        
        A = csc_cubic_spline_matrix(X=X,mode=mode,gen_grad_X=gen_grad_X)

        self.N = N
        self.dtype = dtype
        self.mode = mode
        
        self.X  = X
        self.A  = A
        self.Ainv = None
        
        self.configured = True

    def interpolate(self,X,Y,mode='elastic',solver=sp.sparse.linalg.cgs,tol=1e-8,gen_grad_Y=False):
        if not self.configured or mode != self.mode or not np.allclose(X,self.X):
            self.configure(X,mode)
=======
        self.gen_grad_X = gen_grad_X
        
        if gen_grad_X:
            self.Ainv = A._inv()
            self.dA   = A.dA
        else:
            self.Ainv = None
            self.dA   = None
        
        self.configured = True

    def interpolate(self,X,Y,mode='elastic',solver=sp.sparse.linalg.cgs,tol=1e-8,gen_grad_X=False,gen_grad_Y=False):
        self.configure(X,mode,gen_grad_X)
        return self.reinterpolate(Y,solver,tol,gen_grad_Y)

    def reinterpolate(self,Y,solver=sp.sparse.linalg.cgs,tol=1e-8,gen_grad_Y=False):
        assert self.configured, 'CubicSplineInterpolator has not been configured!'
        N     = self.N
        dtype = self.dtype
        gen_grad_X = self.gen_grad_X

        if self.mode=='periodic':
            assert np.allclose(Y[0],Y[-1]), 'Y0 != Yn in periodic mode...'
            _k = self._k
            Y = np.concatenate((Y[-_k:-1],Y,Y[1:_k]))
        assert Y.size == N, 'Input size mismatch!'
         
        b,Jb,DY = self.A.build_rhs(Y)
=======
        b,Jb_dx,Jb_dy,DY = self.A.build_rhs(Y,gen_grad_X=gen_grad_X,gen_grad_Y=gen_grad_Y)

        K,info = solver(A=self.A,b=b,x0=None,tol=tol)
        assert info>=0, 'Solver failed!'

        DX = self.A.DX
        a =  k[0:N-1]/DX - DY
        b = -k[1:N  ]/DX + DY
        
        C = np.empty(shape=(4,N-1), dtype=dtype)
        C[3,:] = Y[0:N-1] 
        C[2,:] = Y[1:N] - Y[0:N-1] + a
        C[1,:] = b - 2.0*a
        C[0,:] = a - b
=======
        A =  K[0:N-1]/DX - DY
        B = -K[1:N  ]/DX + DY

        c3 = Y[0:N-1] 
        c2 = DY + A
        c1 = B - 2.0*A
        c0 = A - B
        
        C = np.empty(shape=(4,N-1), dtype=dtype)
        C[3,:] = c3
        C[2,:] = c2  
        C[1,:] = c1 
        C[0,:] = c0
        for i in xrange(4):
            C[i,:] *= DX**(3-i)

        P = sp.interpolate.PPoly(c=C, x=self.X, extrapolate=True)
        P = functional.PiecewisePoly(P)
        
        if gen_grad_Y:
            grad_Y = []
            
            if self.Ainv is None:
                Ainv = self.A._inv()
                self.Ainv = Ainv
            J_K = np.asarray((Ainv * Jb).todense())
=======
        if not (gen_grad_X or gen_grad_Y):
            return P
          
        if self.Ainv is None:
            self.Ainv = self.A._inv()

        if gen_grad_X:
            assert self.A is not None
            assert self.dA is not None
            grad_X = []
            Jk_dx = np.asarray(self.Ainv*(Jb_dx - self.dA.ttv(K,modes=2).toarray()))
            dc2 = c2
            dc1 = 2.0*c1
            dc0 = 3.0*c0
            for j in xrange(0,N):
                I = np.arange(1,N) # i = 1 ... N
                I0 = np.asarray(I-1==j, dtype=self.dtype)
                I1 = np.asarray(I  ==j, dtype=self.dtype)
                dI = I1-I0
                
                #dKj = np.zeros(shape=(N), dtype=dtype)
                dKj = Jk_dx[:,j]
                dAj = +dKj[0:N-1]/DX + K[0:N-1]*dI
                dBj = -dKj[1:N]  /DX - K[1:N]  *dI

                dC3j =          dc2*I0
                dC2j = dc2*dI + dc1*I0
                dC1j = dc1*dI + dc0*I0
                dC0j = dc0*dI
                
                Cj = np.empty(shape=(4,N-1), dtype=dtype)
                Cj[3,:] = -dC3j*DX 
                Cj[2,:] = -dC2j*DX + dAj
                Cj[1,:] = -dC1j*DX + dBj - 2.0*dAj
                Cj[0,:] = -dC0j*DX + dAj - dBj
                for i in xrange(4):
                    Cj[i,:] *= DX**(3-i)
                
                dPj = sp.interpolate.PPoly(c=Cj, x=self.X, extrapolate=True)
                grad_X.append(functional.PiecewisePoly(dPj))
        else:
            grad_X = None
        
        if gen_grad_Y:
            grad_Y = [] 
            Jk_dy = self.Ainv*Jb_dy
            for j in xrange(0,N):
                Cj = np.empty(shape=(4,N-1), dtype=dtype)

                I = np.arange(1,N) # i = 1 ... N
                I0 = np.asarray(I-1==j, dtype=self.dtype)
                I1 = np.asarray(I  ==j, dtype=self.dtype)
                
                dKj = J_K[:,j]
                dAj = +dKj[0:N-1]/DX - (I1-I0)
                dBj = -dKj[1:N]  /DX + (I1-I0)

                Cj[3,:] = I0
                Cj[2,:] = I1-I0 + dAj
=======
                dI = I1-I0
                
                dKj = Jk_dy[:,j]
                dAj = +dKj[0:N-1]/DX - dI
                dBj = -dKj[1:N]  /DX + dI

                Cj[3,:] = I0
                Cj[2,:] = dI + dAj
                Cj[1,:] = dBj - 2.0*dAj
                Cj[0,:] = dAj - dBj
                for k in xrange(4):
                    Cj[k,:] *= DX**(3-k)

                dPj = sp.interpolate.PPoly(c=Cj, x=self.X, extrapolate=True)
                grad_Y.append(functional.PiecewisePoly(dPj))

            return P, grad_Y
        else:
            return P
=======
        else:
            grad_Y = None
   
        #if gen_grad_X ^ gen_grad_Y:
            #if gen_grad_X:
                #return (P, grad_X)
            #else:
                #return (P, grad_Y)
        #else:
        return (P, grad_X, grad_Y)

    def xmin(self):
        return self.X[0]
    def xmax(self):
        return self.X[-1]
         

if __name__ == '__main__':

<<<<<<< HEAD
    n = 9
=======
    n = 21
    N = 1000
    f0 = lambda x: np.cos(2*np.pi*x)
    f1 = lambda x: np.exp(x)
    f2 = lambda x: np.sin(2*np.pi*x)
    
    xmin, xmax = 0.0, 2.0
    L = xmax-xmin
    x,dx = np.linspace(xmin,xmax,n,retstep=True)
    xx = np.concatenate(([xmin], np.sort(x[1:-1] + L/5.0*np.random.random(n-2)), [xmax]))
=======
    xx = x#np.concatenate(([xmin], np.sort(x[1:-1] + L/10.0*np.random.random(n-2)), [xmax]
    X0 = np.linspace(xmin,xmax,N)
    X    = np.linspace(xmin-2*dx,xmax+2*dx,N)

    y0 = f0(x)
    y1 = f1(x)
    y2 = f2(x)
    
    interp = CubicSplineInterpolator()
    Pp = interp.interpolate(x,y0,mode='periodic')
    Pe = interp.interpolate(x,y0,mode='elastic')
    Pc = interp.interpolate(x,y0,mode='clamped')
    
    P1 = interp.interpolate(x,y1)
    P2 = interp.reinterpolate(2*y1)
    P3 = interp.reinterpolate(3*y1)
    
    y4 = f0(xx)
    P,P_grad = interp.interpolate(xx,y4,gen_grad_Y=True)
=======
    (P,P_x,P_y) = interp.interpolate(xx,y4,gen_grad_X=True,gen_grad_Y=True)

    #eval_modes = ['normal','zeros','clamp','periodic']#,'mirror']
    #P  = interp.interpolate(x,y2,mode='periodic')
    #F = [ P.as_objective_function(xmin=0.40, xmax=1.25, mode=m) for m in eval_modes ]

    plt.figure()
    plt.subplot(411)
=======
    plt.subplot(511)
    plt.plot(X,Pp(X),'r')
    plt.plot(X,Pe(X),'g')
    plt.plot(X,Pc(X),'b')
    plt.plot(X,f0(X),'c--')
    plt.plot(x,y0,'or')
    plt.plot((X[0],X[-1]),(y0[0],y0[0]),'r.-')
    plt.plot((X[0],X[-1]),(y0[-1],y0[-1]),'b.-')
    plt.xlim(X[0],X[-1])
    plt.ylim(-1.5,+1.5)

=======
    plt.subplot(422)
    plt.subplot(512)
    plt.plot(X,f1(X), 'c--')
    plt.plot(X,2*f1(X),'m--')
    plt.plot(X,3*f1(X),'k--')
    plt.plot(X,P1(X),'r',alpha=0.5)
    plt.plot(X,P2(X),'g',alpha=0.5)
    plt.plot(X,P3(X),'b',alpha=0.5)
    plt.plot(x,y1,'or')
    plt.plot(x,2*y1,'og')
    plt.plot(x,3*y1,'ob')
    

    plt.subplot(413)
    plt.plot(X0,P(X0),'b')
    plt.plot(xx,y4,'or')

    plt.subplot(414)
    for i in xrange(len(P_grad)):
        if i == n//2:
            plt.plot(X0,P_grad[i](X0))
        else:
            plt.plot(X0,P_grad[i](X0),alpha=0.25)
=======
    plt.subplot(513)
    plt.plot(X0,P(X0),'b')
    plt.plot(xx,y4,'or')

    plt.subplot(514)
    for i in xrange(len(P_x)):
        if i == n//2:
            plt.plot(X0,P_x[i](X0))
        else:
            plt.plot(X0,P_x[i](X0),alpha=0.25)
    plt.plot(xx,[1]*n,'or')
    
    plt.subplot(515)
    for i in xrange(len(P_y)):
        if i == n//2:
            plt.plot(X0,P_y[i](X0))
        else:
            plt.plot(X0,P_y[i](X0),alpha=0.25)
    plt.plot(xx,[1]*n,'or')

    plt.show()
        

