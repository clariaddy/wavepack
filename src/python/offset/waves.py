
import os
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

import scipy.signal as signal
import numpy.polynomial.polynomial as poly

import pywt

#local includes
import env
import wavepack as wp
import offset

### input variables and configuration ###
# 
# source data files and names
data_dir        = env.data_dir
plot_script_dir = env.plot_script_dir

data_src  = [ ('CO2.txt','CO2'        ,0,1),    # format = (file_name, arbitrary_data_name, time_column_id, data_column_id)
              ('ATS.txt','Temperature',0,2) ]   # warning: column ids begin from 0

# interpolation parameters
normalize       = True
remove_tendency = True
tendency_order  = 1
N               = 2**10       #resampled points count
timescaling     = 1.0/1000.0 #to kyrs

# CWT parameters

wave_family = wp.WaveletFamily('cmor')
wave        = wave_family.wavelet({'omega0':6.0}) 
#wave_family = wp.WaveletFamily('paul')
#wave        = wave_family.wavelet({'m':10.0})

dj     = 0.01    # 1/dj suboctave per octave
jmin   = None    # if None, default jmin value is computed
jmax   = None    # if None, default jmax value is computed

# XWT parameters
smoothing_kernel = np.mean
smoothing_window = (7,11)
sigma=10.0

# confidense levels
p_level = 0.80   # confidense level probability
monte_carlo_batch_size  = 16
monte_carlo_batch_count = 32

# plot parameters
show_plots = False
save_plots = True
save_dir   = os.getcwd() + '/splines' 
plot_ext = 'png'

time_label  = r'time $(kyrs)$'
data0_label = r'CO2 $(ppmv)$'
data1_label = r'Temperature $(^{\circ}C)$'
udata0_label = r'Unity Normalized CO2'
udata1_label = r'Unity Normalized Temperature'

timedelay_label=r'weighted time delay'
phaseshift_label=r'weighted phaseshift'
data0_color = 'r'
data1_color = 'b'
plottitle_kargs = { 'fontsize':18, 'color':'r' }

plot_scaling = 3.0
plt.rcParams['figure.figsize'] = [plot_scaling*x for x in plt.rcParams['figure.figsize']]
#
### end of configuration ###


### print config ###
print \
"""
=== CONFIGURATION ===
    Inputs:
        data_dir -> {}
        data_src    -> {}
    Interpolation:
        remove_tendency  -> {}
        tendency_order   -> {}
        N (resampled pts)-> {}
        timescaling      -> {}
    CWT parameters:
        wave_family -> {}
        wavelet     -> {}
        dj          -> {}
        jmin        -> {}
        jmax        -> {}
    XWT parameters:
        smoothing_kernel -> {}
        smoothing_window -> {}
    Confidense levels:
        condidense level -> {}
        MT batch size    -> {}
        MT batch  count  -> {}
        MT sample count  -> {}
    Plotting:
        show_plots  -> {}
        save_plots  -> {}
        save_dir    -> {}
====================
""".format(data_dir, data_src, 
           remove_tendency, tendency_order, N, timescaling, 
           wave_family, wave, dj, jmin, jmax,
           smoothing_kernel, smoothing_window,
           p_level, monte_carlo_batch_count, monte_carlo_batch_size, monte_carlo_batch_count*monte_carlo_batch_size,
           show_plots, save_plots, save_dir)

monte_carlo_batch_size*monte_carlo_batch_count

### load input data ###
print '=== DATA PREPROCESSING ==='
print 'Loading data...',
fn0 = data_src[0][1]
fn1 = data_src[1][1]
assert fn0 != fn1, 'Data names should be differents! {} == {}'.format(fn0,fn1)

data = wp.DataSeries(dtype=float)
data.loadFromTxt(data_dir,data_src[0][0], {'t0':data_src[0][2], fn0:data_src[0][3]})
data.loadFromTxt(data_dir,data_src[1][0], {'t1':data_src[1][2], fn1:data_src[1][3]})
data0 = data[fn0]
data1 = data[fn1]
print 'done.'

# scale time and compute tmin and tmax
print 'Scaling time...',
t0 = data['t0']*timescaling
t1 = data['t1']*timescaling
tmin = max(t0[0],  t1[0])
tmax = min(t0[-1], t1[-1])
#wp.plot_script(plot_script_dir,'raw_inputs', globals(), locals())
print 'done'

# interpolation
print 'Interpolating on same timescales...'
interp = wp.TimeSeries(tmin, tmax, N, dtype=float)
interp.interpolate(fn0, t0, data0)
interp.interpolate(fn1, t1, data1)
wp.plot_script(plot_script_dir,'interpolation', globals(), locals())
times = interp['t']
data0 = interp[fn0]
data1 = interp[fn1]
dt=interp.dt
print interp
print

# normalize inputs
if normalize:
    print 'Normalizing inputs...',
    data0 = (data0 - np.mean(data0)) / np.std(data0)
    data1 = (data1 - np.mean(data1)) / np.std(data1)

    # data0s_ = sp.ndimage.filters.gaussian_filter1d(data0, sigma=sigma)
    # data1s_ = sp.ndimage.filters.gaussian_filter1d(data1, sigma=sigma)
    # wp.plot_script(plot_script_dir,'normalization', globals(), locals())
    # wp.plot_script(plot_script_dir,'renormalization_smoothed', globals(), locals())

# remove tendency
if remove_tendency:
    print 'Removing tendency...',
    model0     = np.polyfit(times, data0, tendency_order)
    predicted0 = np.polyval(model0, times)
        
    model1     = np.polyfit(times, data1, tendency_order)
    predicted1 = np.polyval(model1, times)

    model = (model0 + model1)/2.0
    predicted = np.polyval(model, times)
    
    olddata0   = data0
    data0      = data0 - predicted

    olddata1   = data1
    data1      = data1 - predicted
    
    wp.plot_script(plot_script_dir,'tendency_removal', globals(), locals())
    print 'done'

    print '''Tendency fitting with polynomial of order {}:
    {:12s}: fitted polynomial {}
    {:12s}: fitted polynomial {}
    => mean tendency = {}'''.format(tendency_order, fn0, model0, fn1, model1, model)

    if normalize:
        print 'Renormalizing inputs...',
        data0 = (data0 - np.mean(data0)) / np.std(data0)
        data1 = (data1 - np.mean(data1)) / np.std(data1)
        data0s = sp.ndimage.filters.gaussian_filter1d(data0, sigma=sigma)
        data1s = sp.ndimage.filters.gaussian_filter1d(data1, sigma=sigma)
        wp.plot_script(plot_script_dir,'renormalization', globals(), locals())
        print 'done'

print '=========================='


t  = times
X0 = data0
X1 = data1
deg=3
#X0 = (data0 - np.mean(data0)) / np.std(data0)
#X1 = (data1 - np.mean(data1)) / np.std(data1)
#X0 = data0 / np.std(data0)
#X1 = data1 / np.std(data1)
N  = t.size
dL = t[-1]-t[0]

for f in pywt.families():
    print pywt.wavelist(f)
wave = pywt.Wavelet('sym8')
extmode = 'per'
print wave

DWT0 = pywt.wavedec(X0, wave, mode=extmode)
DWT1 = pywt.wavedec(X1, wave, mode=extmode)
dwt_levels = len(DWT0)-1
assert len(DWT0) == len(DWT1)
print 'DWT levels = {}'.format(dwt_levels)

SWT0 = pywt.swt(X0, wave, level=pywt.swt_max_level(N), start_level=0)
SWT1 = pywt.swt(X1, wave, level=pywt.swt_max_level(N), start_level=0)
swt_levels = len(SWT0)
assert len(SWT0) == len(SWT1)
print 'SWT levels = {}'.format(swt_levels)


def find_min_max(P,F0,F1):
    R0 = F0.df.roots(discontinuity=False,extrapolate=False)
    R1 = F1.df.roots(discontinuity=False,extrapolate=False)
    S0 = F0.f(R0)
    S1 = F0.f(R1)
    cond = S0*S1>0.0
    R0 = R0[cond] 
    R1 = R1[cond] 
    S0 = S0[cond]
    S1 = S0[cond]
    A0 = S0*S0
    A1 = S1*S1
    k = np.argsort(A0+A1)[:min(A0.size,P)]
    k = np.sort(k)
    return R0[k],R1[k],(R0[k]+R1[k])/2.0

def time_delay(j,t,X0,X1,deg=3,mode='periodic',tol=1e-4,alpha=1.0,method=2,max_rel_tau=None,callback=None):
    assert X0.size == X1.size and t.size==X0.size, 'size mismatch'
    N = t.size
    Tmin = t[0]
    Tmax = t[-1]
    L = Tmax-Tmin
    
    #rescale
    alpha = np.sqrt(np.sum(X0*X0)/np.sum(X1*X1))
    X1 = alpha*X1

    #interpolate
    F0 = offset.ObjectiveFunction.spline_interp(t,X0,deg)
    F1 = offset.ObjectiveFunction.spline_interp(t,X1,deg)
    
    #find min max
    P = 2**j
    R0,R1,R = find_min_max(P,F0,F1)
    p = R.size
    assert p>=2
    L = t[-1]-t[0]
    Li = (4.0/3.0)* L/p

    fig = plt.figure()
    plt.subplot(311)
    plt.plot(t,F0.f(t),'r',alpha=0.5)
    plt.plot(t,F1.f(t),'b',alpha=0.5)
    plt.plot(R0,F0.f(R0),'or')
    plt.plot(R1,F1.f(R1),'ob')
    plt.plot(R,F0.f(R),'og')
    plt.plot(R,F1.f(R),'og')
    plt.xlim(Tmin,Tmax)
    fig.show()

    optimizer = offset.TimedelayOptimizer(F0,F1,N=N)
    tau = np.empty((p),dtype=float)
    for i in xrange(p):
        tau0 = 0.5*(R1[i]-R0[i])
        tmin = R[i] - Li/2.0
        tmax = R[i] + Li/2.0
        max_tau=None if max_rel_tau is None else max_rel_tau*(tmax-tmin)
        res = optimizer.optimize(tol=tol, max_tau=max_tau,method=method,tau0=tau0,
                options={'disp':False},
                fkargs={'xmin':Tmin,'xmax':Tmax,'mode':'periodic'},
                Ixmin=tmin, Ixmax=tmax, callback=None)
        tau[i]=res.x
        print 'pt={}, x={}, grad={}, Phi(x)={}, msg={}'.format(i, res.x, res.jac if method>0 or max_rel_tau is not None else optimizer.dphi(res.x), res.fun, res.message)

    Tau = offset.ObjectiveFunction.spline_interp(np.concatenate((R-L,R,R+L)),np.concatenate((tau,tau,tau)),deg=min(p-1,3))

    plt.subplot(312)
    plt.plot(t,Tau.f(t))
    plt.plot(R,tau,'ro')
    plt.xlim(Tmin,Tmax)
    
    plt.subplot(313)
    plt.plot(t,F0.f(t),'r',alpha=0.5)
    plt.plot(t,F1.f(t),'b',alpha=0.5)
    plt.plot(t,F0.f(t-0.5*Tau.f(t)), 'r--')
    plt.plot(t,F1.f(t+0.5*Tau.f(t)), 'b--')
    plt.xlim(Tmin,Tmax)
    plt.show()

    return (Tau, alpha, F0, F1)

def iswt(cA,cD,wavelet,level,maxlevel,mode='per',correct_size=0):
    #print 'j={}, P={}, N={}, shape={}, cA.shape={}, cD.shape={}, cAp.shape={}'.format(j,P,N,s,cA.shape,cD.shape,cAp.shape)
    assert cA.size == cD.size
    j = maxlevel-level+1
    N = 2**maxlevel
    P = 2**j
    s = (N/P,P)
    cA = cA.reshape(s).transpose()
    cD = cD.reshape(s).transpose()
    cAp = np.asarray( [pywt.idwt(cA[i],cD[i],wavelet,mode,correct_size)[::2] for i in xrange(P)] )
    return cAp.transpose().ravel()

p=min(2,swt_levels)
P=p+2
fig = plt.figure()
plt.subplot(2,P,1)
plt.title('X0')
plt.plot(t,X0,'r')
plt.subplot(2,P,P+1)
plt.title('X1')
plt.plot(t,X1,'b')
fig.show()
for j in xrange(1,swt_levels+1):
    s = 'level={}'.format(j)
    sa = 'A{}'.format(j)
    sd = 'D{}'.format(j)
    
    A0 = SWT0[j-1][0]
    A1 = SWT1[j-1][0]
    D0 = SWT0[j-1][1]
    D1 = SWT1[j-1][1]
    
    #if j > 1:
        #FD0 = offset.ObjectiveFunction.spline_interp(t,D0,deg)
        #FD1 = offset.ObjectiveFunction.spline_interp(t,D1,deg)
        #D0  = FD0.f(t-0.5*Tau.f(t))
        #D1  = FD1.f(t+0.5*Tau.f(t))
        
    Tau,alpha,FD0,FD1 = time_delay(j,t,D0,D1,deg,tol=1e-4)
    D0n = FD0.f(t-0.5*Tau.f(t))
    D1n = FD1.f(t+0.5*Tau.f(t))
    
    if j==1: 
        A0n_old = A0
        A1n_old = A1
    else:
        A0n_old = A0n
        A1n_old = A1n
    A0n = iswt(A0n_old,D0n,wave,j,swt_levels)
    A1n = iswt(A1n_old,D1n,wave,j,swt_levels)

    if j <= p:
        plt.subplot(2,P,j+1)
        plt.title(sa)
        plt.plot(t,A0, 'r',alpha=0.5)
        plt.plot(t,A1, 'b',alpha=0.5)
        plt.plot(t,A0n_old, 'r--')
        plt.plot(t,A1n_old, 'b--')
        
        plt.subplot(2,P,P+j+1)
        plt.title(sd)
        plt.plot(t,D0, 'r')
        plt.plot(t,alpha*D1, 'b')
        plt.plot(t,D0n, 'r--')
        plt.plot(t,D1n, 'b--')
        plt.draw()

    
plt.subplot(2,P,P)
plt.plot(t,A0,'r')
plt.plot(t,A1,'b')
plt.subplot(2,P,2*P)
plt.plot(t,A0n,'r')
plt.plot(t,A1n,'b')

plt.show()
plt.close('all')
