
import numpy as np
import scipy as sp

import matplotlib.pyplot    as plt
import matplotlib.animation as anim

#local import
import functional, dotprod, cubic

class TimeDelayOptimizer(object):

    def __init__(self):
        self.initial_values_configured = False
        self.optimizer_configured      = False
        self.timedelay_configured      = False
        self.verbose = True

    def configure_initial_values(self,xmin,xmax,X0=None,Y0=None,n=None):
        assert xmin < xmax, 'xmin >= xmax!'
        if n is not None:
            assert n>=4, 'n<4!'
        elif (X0 is not None) or (Y0 is not None):
            if X0 is not None:
                n = X0.size
            elif Y0 is not None:
                n = Y0.size
        else:
            raise ValueError('n,X0 or Y0 not specified!')

        if X0 is None:
            X0 = np.linspace(xmin,xmax,n)
        else:
            assert X0.size==n, 'X0.size != n'
        if Y0 is None:
            Y0 = np.zeros(n)
        else:
            assert Y0.size==n, 'Y0.size != n'

        self.xmin = xmin
        self.xmax = xmax
        self.L    = xmax - xmin

        self.n=n
        self.X0=X0
        self.Y0=Y0

        self.initial_values_configured = True

        return self

    def configure_timedelay(self,
                  tau_min,tau_max,
                  timedelay_spline_mode='elastic',
                  levels=1):

        assert tau_min < tau_max, 'tau_min >= tau_max!'
        assert levels >= 1, 'levels<=0!'

        self.tau_min = tau_min
        self.tau_max = tau_max
        self.timedelay_spline_mode = timedelay_spline_mode
        self.levels = levels

        self.timedelay_configured = True
        return self

    def configure_optimizer(self, gen_grad_Y=True, gen_grad_X=False,
                            eps = 10e-4,
                            epsrel=None, epsabs=None,
                            optimizer='TNC', 
                            dot=dotprod.SobolevDotProduct(0),
                            normalize=True,
                            scaling_factor=None):
        
        valid_optimizers = ['TNC','L-BFGS-B','SLSQP']

        assert eps > 0.0, 'eps<0!'
        assert gen_grad_X or gen_grad_Y, 'gen_grad_X || gen_grad_Y == 0 !'
        assert isinstance(optimizer,str) and optimizer in valid_optimizers, 'optimizer should be one of the following: {}'.format(valid_optimizers)
        assert issubclass(dot.__class__,dotprod.DotProduct), 'dot does not inherit from functional.DotProduct!'
        epsabs = eps if epsabs is None else epsabs
        epsrel = eps if epsrel is None else epsrel

        self.gen_grad_X = gen_grad_X
        self.gen_grad_Y = gen_grad_Y
        
        self.eps=eps
        self.epsabs = epsabs
        self.epsrel = epsrel
        self.optimizer=optimizer
        self.dot = dot
        self.normalize = normalize

        self.do_scale       = (scaling_factor is not None)
        self.optimizer_configured = True

        return self

    def check_initialization(self):
        assert self.initial_values_configured, 'Initial values not configured... Call configure_initial_values!'
        assert self.timedelay_configured     , 'Time delay not configured... Call configure_timedelay!'
        assert self.optimizer_configured     , 'Optimize not configured... Call configure_optimizer!'

        assert (self.X0>=self.xmin).all(), 'X0 < xmin...'
        assert (self.X0<=self.xmax).all(), 'X0 > xmax...'
        assert (self.Y0>=self.tau_min).all(), 'Y0 < tau_min...'
        assert (self.Y0<=self.tau_max).all(), 'Y0 > tau_max...'

        assert (self.X0 == np.sort(self.X0)).all(), 'X0 is not sorted...'
        
    def _integ(self,func):
        return sp.integrate.quad(func=func, a=self.xmin, b=self.xmax, epsabs=self.epsabs,epsrel=self.epsrel)[0]

    def _normalize(self,f0,f1):
        #if self.normalize:
            #I0 = self._integ(self.dot(f0,f0).F)
            #I1 = self._integ(self.dot(f1,f1).F)
        F0,F1 = f0,f1
        return F0,F1
    
    def optimize(self,f0,f1,gen_plot=False):
        self.check_initialization()
        assert isinstance(f0,functional.ObjectiveFunction) and f0.dlevel >= 1, 'f0 is not an ObjectiveFunction or f0.dlevel<1 !'
        assert isinstance(f1,functional.ObjectiveFunction) and f0.dlevel >= 1, 'f1 is not an ObjectiveFunction or f1.dlevel<1 !'
            
        X0 = self.X0
        Y0 = self.Y0
        x0,y0 = X0,Y0
        F0,F1 = self._normalize(f0,f1)
        B = self.dot(F0,F1,merge_vars=False)

        interp = cubic.CubicSplineInterpolator()
        interp.configure(X0,mode=mode,gen_grad_X=self.gen_grad_X)
        (Tau0, Tau0_x, Tau0_y) = interp.reinterpolate(Y0,gen_grad_Y=self.gen_grad_Y)

        #usefull funcs
        _I  = np.vectorize(pyfunc=self._integ)

        def _nPoints(Z):
            assert Z.size % 2 == 0
            n = Z.size / 2
            return n
        def _concat(X,Y):
            Z = np.concatenate((X,Y))
            return Z
        def _split(Z):
            n = _nPoints(Z)
            X = Z[0:n]
            Y = Z[n:2*n]
            return X,Y,n


        def _Tau(Z):
            X,Y,n = _split(Z)
            if self.gen_grad_X:
                return interp.interpolate(X,Y,mode=mode,gen_grad_X=self.gen_grad_X,gen_grad_Y=self.gen_grad_Y)
            else:
                return interp.reinterpolate(Y,gen_grad_Y=self.gen_grad_Y)
        def _correlation(Z):
            (Tau,_,_) = _Tau(Z)
            return lambda x: -B.F((x-0.5*Tau(x), x+0.5*Tau(x)))
        def _grad_correlation(Z):
            (Tau,Tau_x,Tau_y) = _Tau(Z)
            common = lambda x: -0.5*B.dF((-(x-0.5*Tau(x)), x+0.5*Tau(x)))
            def Taux(i): return lambda x: Tau_x[i](x) * common(x)
            def Tauy(i): return lambda x: Tau_y[i](x) * common(x)
            n = _nPoints(Z)
            dx_dist = [ Taux(i) for i in xrange(n) ]
            dy_dist = [ Tauy(i) for i in xrange(n) ]
            return dx_dist, dy_dist
        def _phi(Z):
            return _I(_correlation(Z))
        def _dphi(Z):
            n = _nPoints(Z)
            z = np.zeros(n)
            dx_dist, dy_dist = _grad_correlation(Z)
            if self.gen_grad_X:
                Ix = _I(dx_dist[1:-1])
                Ix = np.concatenate(([0],Ix,[0]))
            else:
                Ix = z
            if self.gen_grad_Y:
                Iy = _I(dy_dist)
            else:
                Iy = z
            return _concat(Ix,Iy)

        #function called by the optimizer to store intermediate values of Z=(X,Y)
        ZZ = []
        def callback(Z):
            X,Y,n = _split(Z)
            if self.verbose:
                print '\tCurrent nodes [{}]!'.format([ '({:+.8f},{:+.8f})'.format(X[i],Y[i]) for i in xrange(n) ])
            ZZ.append(Z)

                
        assert X0.size==Y0.size
        n = X0.size
        DX = X0[1:n]-X0[0:n-1]
        Xbounds = [(X0[ 0]-0.45*DX[  0],X0[ 0]+0.45*DX[ 0])]                         \
                + [(X0[ i]-0.45*DX[i-1],X0[ i]+0.45*DX[ i]) for i in xrange(1,n-1)]  \
                + [(X0[-1]-0.45*DX[ -1],X0[-1]+0.45*DX[-1])]
        Ybounds = [(self.tau_min,self.tau_max)]*n
        
        Z0       = _concat(X0,Y0)
        Zbounds  = _concat(Xbounds,Ybounds)

        callback(Z0)
        res = sp.optimize.minimize(fun=_phi, x0=Z0, jac=_dphi, method=self.optimizer, bounds=Zbounds, tol=self.eps, callback=callback)

        if gen_plot:
            # generate intermediate optimization results
            P = len(ZZ)
            Taus  = [_Tau(zz)[0] for zz in ZZ]
            distances  = [_correlation(zz)  for zz in ZZ]
            ddistances = [(_grad_correlation(zz)) for zz in ZZ]
            phis  = [_phi(zz)  for zz in ZZ]
            dphis = [_dphi(zz) for zz in ZZ]
            dphis_norm2 = [np.dot(df,df) for df in dphis]

            #configure plot animation
            N=1000
            Xp = np.linspace(self.xmin,self.xmax,N)
            alpha = 1.25
            delay = 1.0
            fmin = min(np.min(f0.F(Xp)),np.min(f1.F(Xp)))
            fmax = max(np.max(f0.F(Xp)),np.max(f1.F(Xp)))

            fig, ss = plt.subplots(2,1)

            fixed = [
                #initial delay guess
                ss[0].plot(Xp, Tau0(Xp), 'm--',alpha=0.5,label='Initial delay guess')[0],
                ss[0].plot(x0,y0, 'og')[0],
                #initial functions
                ss[1].plot(Xp, f0.F(Xp), 'r--', alpha=0.5, label='f0(x)')[0],
                ss[1].plot(Xp, f1.F(Xp), 'b--', alpha=0.5, label='f1(x)')[0],
            ]
            animated = [
                #iteration text
                ss[0].text(0.5,0.8,'Iteration 0', fontsize=20, horizontalalignment='center', transform=ss[0].transAxes),
                #current delay guess
                ss[0].plot(Xp, Tau0(Xp), 'm',label='Current delay guess')[0],
                ss[0].plot(x0,y0, 'om')[0],
                #current function reconstruction
                ss[1].plot(Xp, F0.F(Xp-0.5*Tau0(Xp)), 'r', label='f0(x-0.5*tau(x))')[0],
                ss[1].plot(Xp, F1.F(Xp+0.5*Tau0(Xp)), 'b', label='f1(x+0.5*tau(x))')[0],
            ]

            for s in ss: 
                s.set_xlim(xmin,xmax)
                s.legend()
            ss[0].set_ylim(alpha*Tau_min,alpha*Tau_max)
            ss[1].set_ylim(alpha*fmin,alpha*fmax)

            #animation
            def init():
                for p in animated: 
                    p.set_data([],[])
                return animated

            def animate(i):
                X,Y,n = _split(ZZ[i])
                #iteration text
                animated[0].set_text('Iteration {}'.format(i))
                #current delay guess
                animated[1].set_data(Xp,Taus[i](Xp))
                animated[2].set_data(X,Y)
                #current function reconstruction
                animated[3].set_data(Xp, F0.F(Xp-0.5*Taus[i](Xp)))
                animated[4].set_data(Xp, F1.F(Xp+0.5*Taus[i](Xp)))

                return animated

            an = anim.FuncAnimation(fig, animate, frames=P,interval=int(delay*1000), blit=False, repeat=True)
            plt.show()
        
        Xo,Yo,n = _split(res.x)
        return Xo,Yo,(n,res,ZZ)

if __name__ == '__main__':
    # functions to be fitted
    f0   = lambda x: +x*np.sin(x)
    df0  = lambda x: +x*np.cos(x)+np.sin(x)
    d2f0 = lambda x: -x*np.sin(x)+2.0*np.cos(x)
    d3f0 = lambda x: -x*np.cos(x)-2.0*np.sin(x)-np.cos(x)
    F0 = functional.ObjectiveFunction([functional.FunctorTensor([f]) for f in [f0,df0,d2f0,d3f0]])


    f1   = lambda x: +x*np.cos(x)
    df1  = lambda x: -x*np.sin(x)+np.cos(x)
    d2f1 = lambda x: -x*np.cos(x)-2.0*np.sin(x)
    d3f1 = lambda x: +x*np.sin(x)-2.0*np.cos(x)+np.sin(x)
    F1 = functional.ObjectiveFunction([functional.FunctorTensor([f]) for f in [f1,df1,d2f1,d3f1]])

    #objectives
    eps = 1e-6
    gen_grad_X=False
    gen_grad_Y=True

    #domain
    xmin = 0.0
    xmax = 4.0*np.pi
    L = xmax - xmin

    #spline parameters
    n0      = 5      #spline nodes count
    mode = 'elastic' #spline boundary mode
    Tau_max = (xmax-xmin)/5 #max time delay
    Tau_min = -Tau_max
    
    #optimizer
    topt = TimeDelayOptimizer().configure_initial_values(xmin,xmax,n=n0)                          \
                               .configure_timedelay(Tau_min,Tau_max,timedelay_spline_mode=mode)   \
                               .configure_optimizer(gen_grad_Y=gen_grad_Y,gen_grad_X=gen_grad_X,eps=eps)
    Xo,Yo,_ = topt.optimize(F0,F1,gen_plot=True)

    topt.configure_initial_values(xmin,xmax,Xo,Yo)            \
        .configure_optimizer(gen_grad_Y=True,gen_grad_X=True) \
        .optimize(F0,F1,gen_plot=True)



