
import time
import numpy as np
import scipy as sp

import matplotlib.pyplot    as plt
import matplotlib.animation as anim


# my includes
import cubic, functional

# Example 1: Fit a cubic spline Pn(x) to a function f(x) in [xmin,xmax]
#   -spline nodes          Xi = {x_0, ..., x_N} are the paremeters to be found
#   -spline value at nodes Yi = {y_0, ..., y_N} are fixed
#Objective function: phi(Y) = (1/2) * integral(xmin,xmax) [Pn(x;X)-f(x)]^2 dx

n = 5   #nodes count
N = 1000 #plot precision

xmin = 0.0
xmax = 3.0
X0,dx = np.linspace(xmin,xmax,n,retstep=True)
bounds = [(x-0.45*dx,x+0.45*dx) for x in X0]
Xp = np.linspace(xmin,xmax,N)

# function to be fitted
f = lambda x: np.exp(x) + np.cos(10*x)

# spline interpolant
interp = cubic.CubicSplineInterpolator()
interp.configure(X0,mode='elastic',gen_grad_X=True)

# initial guess for nodes values
Y0 = f(X0)
(P0, dP_x, dP_y) = interp.reinterpolate(Y0)

#precision configuration
eps    = 1e-6
epsrel = eps
epsabs = eps

#plot config
ymin = f(xmin) - 2
ymax = f(xmax) + 2
delay = 0.2 #seconds
k0 = 2
k1 = 1


#definition of objective functions
def integ(func):
    return sp.integrate.quad(func=func, a=xmin, b=xmax, epsabs=epsabs,epsrel=epsrel)[0]
I = np.vectorize(integ)

def _P(X):
    return interp.interpolate(X,Y0,gen_grad_X=True,gen_grad_Y=False)
def mse(X):
    (P,P_x,P_y) = _P(X)
    return lambda x: 0.5 * ( P(x) - f(x) )**2
def dmse(X):
    (P,P_x,P_y) = _P(X)
    def dP(i): return lambda x: dP_x[i](x) * (P(x)-f(x))
    return [ dP(i) for i in xrange(1,n-1) ]
def phi(X):
    return I(mse(X))
def dphi(X):
    return np.concatenate(([0.0],I(dmse(X)),[0.0]))

#function called by the optimizer to store intermediate values of X
XX = [X0]
def callback(X):
    print 'Current val {}!'.format(X)
    XX.append(X)


#optimize
res = sp.optimize.minimize(fun=phi, x0=X0, jac=dphi, method='L-BFGS-B', bounds=bounds, tol=eps, callback=callback)

P = len(XX)
steps = range(P)
mses  = [mse(xx)  for xx in XX]
phis  = [phi(xx)  for xx in XX]
dphis = [dphi(xx) for xx in XX]
dphis_norm2 = [np.dot(df,df) for df in dphis]


#plot animation
fig, (s0,s1,s2) = plt.subplots(3,1)
s3 = s2.twinx()
fixed = [
        #target function
        s0.plot(Xp,f(Xp),'r', label='Target function')[0],
        #initial guess
        s0.plot(Xp,P0(Xp),'m--',alpha=0.5,label='Initial guess')[0],
        s0.plot(X0,Y0,'og',alpha=0.75)[0],
        #objective function
        s2.plot(steps, phis, 'b--', label='Phi')[0],
        s2.plot(steps, phis, 'ob')[0],
        s3.plot(steps, dphis_norm2, 'g--', label='||grad(Phi)||^2')[0],
        s3.plot(steps, dphis_norm2, 'og')[0]
]
animated = [
    s0.text(0.5,0.8,'Iteration 0', fontsize=20, horizontalalignment='center', transform=s0.transAxes),
    #current optimal guess
    s0.plot(Xp,P0(Xp),'b',label='Current guess')[0],
    s0.plot(X0,Y0,'or')[0],
    #mean squarred error
    s1.plot(Xp,mse(X0)(Xp),'g',label='Mean Squared Error (MSE)')[0],
    s2.plot([0],[phis[0]], 'or', label='Current iteration')[0],
    s3.plot([0],[dphis_norm2[0]], 'or')[0]
]

s0.set_xlim(xmin,xmax)
s0.set_ylim(ymin,ymax)
s0.legend()

s1.set_xlim(xmin,xmax)
s1.legend(loc=1)

s2.set_yscale('log')
s2.set_xlabel('x')
s2.set_ylabel('Phi(x)')
s2.legend(handles=[fixed[-4],fixed[-2],animated[-2]],loc=1)

s3.set_yscale('log')
s3.set_ylabel('||grad[Phi](x)||^2')

def init():
    for p in animated: 
        p.set_data([],[])
    return animated

def animate(i):
    X = XX[i]
    animated[0].set_text('Iteration {}'.format(i))
    animated[1].set_data(Xp,_P(X)[0](Xp))
    animated[2].set_data(X,Y0)
    animated[3].set_data(Xp,mses[i](Xp))
    animated[4].set_data([i],[phis[i]])
    animated[5].set_data([i],[dphis_norm2[i]])
    return animated

anim = anim.FuncAnimation(fig, animate, frames=P,interval=int(delay*1000), blit=True, repeat=True)
plt.show()

