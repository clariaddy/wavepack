import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import time
import numpy as np
import scipy as sp

import matplotlib.pyplot    as plt
import matplotlib.animation as anim


# my includes
import cubic

# Example 1: Fit a cubic spline Pn(x) to a function f(x) in [xmin,xmax]
#   -spline nodes              Xi = {x_0, ..., x_N}
#    and spline value at nodes Yi = {y_0, ..., y_N} are the paremeters to be found
#Objective function: phi(Y) = (1/2) * integral(xmin,xmax) [Pn(x;[X,Y])-f(x)]^2 dx

n = 10 #nodes count
N = 1000 #plot precision

# function to be fitted
f = lambda x: x*np.cos(4*x)+np.sin(12*x)
#np.exp(x) + np.cos(10*x)

xmin    = 0.0
xmax    = 3.0
Xp      = np.linspace(xmin,xmax,N)
X0,dx   = np.linspace(xmin,xmax,n,retstep=True)
Y0      = 2.0*(0.5-np.random.random(n))
Xbounds = [(x-0.45*dx,x+0.45*dx) for x in X0]
Ybounds = [(np.min(f(Xp)),np.max(f(Xp)))]*n
mode    = 'elastic'

#X0[1:-1] += 0.45*dx*np.random.random(n-2)

Z0 = np.concatenate((X0,Y0))
bounds = Xbounds + Ybounds

# spline interpolant
interp = cubic.CubicSplineInterpolator()
interp.configure(X0,mode=mode,gen_grad_X=True)

# initial guess for nodes values
(P0, dP_x, dP_y) = interp.reinterpolate(Y0,gen_grad_Y=True)

#precision configuration
eps    = 1e-2
epsrel = eps
epsabs = eps

#plot config
ymin = f(xmin) - 2
ymax = f(xmax) + 2
delay = 0.5 #seconds
k0 = 2
k1 = 1


#definition of objective functions
def integ(func):
    return sp.integrate.quad(func=func, a=xmin, b=xmax, epsabs=epsabs,epsrel=epsrel)[0]
I = np.vectorize(pyfunc=integ)

def extract(Z):
    X = Z[0:n]
    Y = Z[n:2*n]
    return X,Y
def _P(Z):
    X,Y = extract(Z)
    return interp.interpolate(X,Y,mode=mode,gen_grad_X=True,gen_grad_Y=True)
def mse(Z):
    (P,dP_x,dP_y) = _P(Z)
    return lambda x: 0.5 * ( P(x) - f(x) )**2
def dmse(Z):
    (P,dP_x,dP_y) = _P(Z)
    def dPx(i): return lambda x: dP_x[i](x) * (P(x)-f(x))
    def dPy(i): return lambda x: dP_y[i](x) * (P(x)-f(x))
    dx_mse = [ dPx(i) for i in xrange(n) ]
    dy_mse = [ dPy(i) for i in xrange(n) ]
    return dx_mse, dy_mse
def phi(Z):
    return I(mse(Z))
def dphi(Z):
    dx_mse, dy_mse = dmse(Z)
    return np.concatenate(([0.0],I(dx_mse[1:-1]),[0.0],I(dy_mse)))


#function called by the optimizer to store intermediate values of X
ZZ = [Z0]
def callback(Z):
    print 'Current vals {}!'.format([ (Z[i],Z[n+i]) for i in xrange(n) ])
    ZZ.append(Z)

#optimize
res = sp.optimize.minimize(fun=phi, x0=Z0, jac=dphi, method='TNC', bounds=bounds, tol=eps, callback=callback)

P = len(ZZ)
steps = range(P)
Ps    = [_P(zz)[0] for zz in ZZ]
mses  = [mse(zz)  for zz in ZZ]
dmses = [(dmse(zz)) for zz in ZZ]
phis  = [phi(zz)  for zz in ZZ]
dphis = [dphi(zz) for zz in ZZ]
dphis_norm2 = [np.dot(df,df) for df in dphis]


#plot animation
fig, (s0,s4,s5,s1,s2) = plt.subplots(5,1)
s3 = s2.twinx()
fixed = [
        #target function
        s0.plot(Xp,f(Xp),'r', label='Target function')[0],
        #initial guess
        s0.plot(Xp,P0(Xp),'m--',alpha=0.5,label='Initial guess')[0],
        s0.plot(X0,Y0,'og',alpha=0.75)[0],
        #objective function
        s2.plot(steps, phis, 'b--', label='Phi')[0],
        s2.plot(steps, phis, 'ob')[0],
        s3.plot(steps, dphis_norm2, 'g--', label='||grad(Phi)||^2')[0],
        s3.plot(steps, dphis_norm2, 'og')[0]
]
animated = [
    s0.text(0.5,0.8,'Iteration 0', fontsize=20, horizontalalignment='center', transform=s0.transAxes),
    #current optimal guess
    s0.plot(Xp,P0(Xp),'b',label='Current guess')[0],
    s0.plot(X0,Y0,'or')[0],
    #mean squarred error
    s1.plot(Xp,mses[0](Xp),'g',label='Mean Squared Error (MSE)')[0],
    s2.plot([0],[phis[0]], 'or', label='Current iteration')[0],
    s3.plot([0],[dphis_norm2[0]], 'or')[0]
] 
s2.legend(handles=[fixed[-4],fixed[-2],animated[-2]],loc=1)

animated += [s4.plot(Xp,dmses[0][0][j](Xp), linestyle='--' if (j==0 or j==n-1) else '-')[0] for j in xrange(n) ]
animated += [s5.plot(Xp,dmses[0][1][j](Xp))[0] for j in xrange(n) ]

s0.set_xlim(xmin,xmax)
s0.set_ylim(ymin,ymax)
s0.legend()

s1.set_xlim(xmin,xmax)
s1.legend(loc=1)
s1.set_ylabel('MSE(x)')

s2.set_yscale('log')
s2.set_xlabel('x')
s2.set_ylabel('Phi(x)')

s3.set_yscale('log')
s3.set_ylabel('||grad[Phi](x)||^2')

s4.set_xlim(xmin,xmax)
s4.set_ylabel('gradX_[MSE]')

s5.set_xlim(xmin,xmax)
s5.set_ylabel('gradY_[MSE]')

def init():
    for p in animated: 
        p.set_data([],[])
    return animated

def animate(i):
    X,Y = extract(ZZ[i])
    animated[0].set_text('Iteration {}'.format(i))
    animated[1].set_data(Xp,Ps[i](Xp))
    animated[2].set_data(X,Y)
    animated[3].set_data(Xp,mses[i](Xp))
    animated[4].set_data([i],[phis[i]])
    animated[5].set_data([i],[dphis_norm2[i]])
    for j in xrange(n):
        animated[6+j].set_data(Xp,dmses[i][0][j](Xp))
        animated[6+n+j-1].set_data(Xp,dmses[i][1][j](Xp))
    for s in [s0,s1,s2,s3,s4,s5]:
        s.relim()
        s.autoscale_view()
    return animated

anim = anim.FuncAnimation(fig, animate, frames=P,interval=int(delay*1000), blit=False, repeat=True)
plt.show()

print
print 'Final cost = {}'.format(phis[-1])

