import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import time
import numpy as np
import scipy as sp

import matplotlib.pyplot    as plt
import matplotlib.animation as anim


# my includes
import cubic, functional

# Example 1: Fit a cubic spline Pn(x) to a function f(x) in [xmin,xmax]
#   -spline nodes          Xi = {x_0, ..., x_N} are fixed
#   -spline value at nodes Yi = {y_0, ..., y_N} are the paremeters to be found
#Objective function: phi(Y) = (1/2) * integral(xmin,xmax) [Pn(x;Y)-f(x)]^2 dx

n = 9    #nodes count
N = 1000 #plot precision

xmin = 0.0
xmax = 3.0
x = np.linspace(xmin,xmax,n)
X = np.linspace(xmin,xmax,N)

# function to be fitted
f = lambda x: np.exp(x)*np.sin(x)+np.cos(10*x)

# spline interpolant
interp = cubic.CubicSplineInterpolator()
interp.configure(x,mode='elastic',gen_grad_X=True)

# initial guess for nodes values
y0 = (f(xmax)-f(xmin)) * np.random.random(n)
(P_y0, dP_x, dP_y) = interp.reinterpolate(y0, gen_grad_Y=True)

#precision configuration
eps    = 1e-3
epsrel = eps
epsabs = eps

#plot config
ymin = f(xmin) - 2
ymax = f(xmax) + 2
delay = 0.2 #seconds
k0 = 2
k1 = 1


#definition of objective functions
def integ(func):
    return sp.integrate.quad(func=func, a=xmin, b=xmax, epsabs=epsabs,epsrel=epsrel)[0]
I = np.vectorize(integ)

def P_y(Y):
    return interp.reinterpolate(Y)[0]
def mse(Y):
    P = P_y(Y)
    return lambda x: 0.5 * ( P(x) - f(x) )**2
def dmse(Y):
    P = P_y(Y)
    def dP(i): return lambda x: dP_y[i](x) * (P(x)-f(x))
    return [ dP(i) for i in xrange(n) ]
def phi(Y):
    return I(mse(Y))
def dphi(Y):
    return I(dmse(Y))

#function called by the optimizer to store intermediate values of Y
YY = [y0]
def callback(Y):
    YY.append(Y)


#optimize
res = sp.optimize.minimize(fun=phi, x0=y0, jac=dphi, method='BFGS',tol=eps, callback=callback)

P = len(YY)
steps = range(P)
phis  = [phi(yy)  for yy in YY]
dphis = [dphi(yy) for yy in YY]
dphis_norm2 = [np.dot(df,df) for df in dphis]


#plot animation
fig, (s0,s1,s2) = plt.subplots(3,1)
s3 = s2.twinx()
fixed = [
        #target function
        s0.plot(X,f(X),'r', label='Target function')[0],
        #initial guess
        s0.plot(X,P_y0(X),'m--',alpha=0.5,label='Initial guess')[0],
        s0.plot(x,y0,'og',alpha=0.75)[0],
        #objective function
        s2.plot(steps, phis, 'b--', label='Phi')[0],
        s2.plot(steps, phis, 'ob')[0],
        s3.plot(steps, dphis_norm2, 'g--', label='||grad(Phi)||^2')[0],
        s3.plot(steps, dphis_norm2, 'og')[0]
]
animated = [
    s0.text(0.5,0.8,'Iteration 0', fontsize=20, horizontalalignment='center', transform=s0.transAxes),
    #current optimal guess
    s0.plot(X,P_y0(X),'b',label='Current guess')[0],
    s0.plot(x,y0,'or')[0],
    #mean squarred error
    s1.plot(X,mse(y0)(X),'g',label='Mean Squared Error (MSE)')[0],
    s2.plot([0],[phis[0]], 'or', label='Current iteration')[0],
    s3.plot([0],[dphis_norm2[0]], 'or')[0]
]

s0.set_xlim(xmin,xmax)
s0.set_ylim(ymin,ymax)
s0.legend()

s1.set_xlim(xmin,xmax)
s1.legend(loc=1)

s2.set_yscale('log')
s2.set_xlabel('x')
s2.set_ylabel('Phi(x)')
s2.legend(handles=[fixed[-4],fixed[-2],animated[-2]],loc=1)

s3.set_yscale('log')
s3.set_ylabel('||grad[Phi](x)||^2')

def init():
    for p in animated: 
        p.set_data([],[])
    return animated

def animate(i):
    Y = YY[i]
    animated[0].set_text('Iteration {}'.format(i))
    animated[1].set_data(X,P_y(Y)(X))
    animated[2].set_data(x,Y)
    animated[3].set_data(X,mse(Y)(X))
    animated[4].set_data([i],[phis[i]])
    animated[5].set_data([i],[dphis_norm2[i]])
    return animated

anim = anim.FuncAnimation(fig, animate, frames=P,interval=int(delay*1000), blit=True, repeat=True)
plt.show()

