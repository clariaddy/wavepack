
import sys, os, time

import numpy as np
import scipy as sp

import matplotlib.pyplot    as plt
import matplotlib.animation as anim

# local includes
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import cubic

# Example 4: Fit a periodic cubic spline Tau(x;(X,Y)) defined on [xmin,xmax] by:
#   -spline nodes              Xi = {x_0, ..., x_N}
#    and spline value at nodes Yi = {y_0, ..., y_N} are the paremeters to be found
# such that Phi(X,Y) = integral(xmin,xmax) ||f1(x + 0.5*Tau(x)) - f0(x - 0.5*Tau(x))||^2 dx is minimized
# ie: find optimal time delay between f1 and f0

N = 1000 #plot precision

# functions to be fitted
f0   = lambda x: +x*np.sin(x)
df0  = lambda x: +x*np.cos(x)+np.sin(x)
d2f0 = lambda x: -x*np.sin(x)+2.0*np.cos(x)
d3f0 = lambda x: -x*np.cos(x)-2.0*np.sin(x)-np.cos(x)

f1   = lambda x: +x*np.cos(x)
df1  = lambda x: -x*np.sin(x)+np.cos(x)
d2f1 = lambda x: -x*np.cos(x)-2.0*np.sin(x)
d3f1 = lambda x: +x*np.sin(x)-2.0*np.cos(x)+np.sin(x)

#objectives
beta,kappa,eta = 1.0,1.0,1.0
eps = 20.0

#domain
xmin = 0.0
xmax = 4.0*np.pi
L = xmax - xmin

#spline parameters
n0      = 6     #spline nodes count
mode = 'clamped' #spline boundary mode

Tau_max = (xmax-xmin)/10 #max time delay
Tau_min = -Tau_max

#optimization parameters
eps     = 1e-2
epsrel  = eps
epsabs  = eps

#ploting parameters
Xp    = np.linspace(xmin,xmax,N)
delay = 1.0 #animation delay in seconds

fmin = min(np.min(f0(Xp)),np.min(f1(Xp)))
fmax = max(np.max(f0(Xp)),np.max(f1(Xp)))
alpha = 1.25

#definition of objective functions
def nPoints(Z):
    assert Z.size % 2 == 0
    n = Z.size / 2
    return n
def concat(X,Y):
    Z = np.concatenate((X,Y))
    return Z
def split(Z):
    n = nPoints(Z)
    X = Z[0:n]
    Y = Z[n:2*n]
    return X,Y,n

def integ(func):
    return sp.integrate.quad(func=func, a=xmin, b=xmax, epsabs=epsabs,epsrel=epsrel)[0]
def integ_n(func):
    return sp.integrate.quad(func=lambda x: func(x)**2, a=xmin, b=xmax, epsabs=epsabs,epsrel=epsrel)[0]
I  = np.vectorize(pyfunc=integ)
In = np.vectorize(pyfunc=integ_n)

K0 = np.sqrt(In(  f1)/In(  f0))
K1 = np.sqrt(In( df1)/In( df0))
K2 = np.sqrt(In(d2f1)/In(d2f0))
K3 = np.sqrt(In(d3f1)/In(d3f0))
F0   = lambda x: K0*  f0(x)
dF0  = lambda x: K1* df0(x)
d2F0 = lambda x: K2*d2f0(x)
d3F0 = lambda x: K3*d3f0(x)
F1   = lambda x:      f1(x)
dF1  = lambda x:     df1(x)
d2F1 = lambda x:    d2f1(x)
d3F1 = lambda x:    d3f1(x)


def _Tau(Z):
    X,Y,n = split(Z)
    return interp.interpolate(X,Y,mode=mode,gen_grad_X=True,gen_grad_Y=True)
def mse(Z):
    (Tau,Tau_x,Tau_y) = _Tau(Z)
    return lambda x: (beta * (   F1(x+0.5*Tau(x)) -   F0(x-0.5*Tau(x)) )**2 
                   + kappa * (  dF1(x+0.5*Tau(x)) -  dF0(x-0.5*Tau(x)) )**2 
                   +   eta * ( d2F1(x+0.5*Tau(x)) - d2F0(x-0.5*Tau(x)) )**2)
def dmse(Z):
    (Tau,Tau_x,Tau_y) = _Tau(Z)
    def Taux(i): return lambda x: Tau_x[i](x) * (    beta  * ( dF1(x+0.5*Tau(x)) + dF0 (x-0.5*Tau(x))) * (  F1(x+0.5*Tau(x)) -   F0(x-0.5*Tau(x)))
                                                  +  kappa * (d2F1(x+0.5*Tau(x)) + d2F0(x-0.5*Tau(x))) * ( dF1(x+0.5*Tau(x)) -  dF0(x-0.5*Tau(x)))
                                                  +    eta * (d3F1(x+0.5*Tau(x)) + d3F0(x-0.5*Tau(x))) * (d2F1(x+0.5*Tau(x)) - d2F0(x-0.5*Tau(x)))
                                                )
    def Tauy(i): return lambda x: Tau_y[i](x) * (    beta  * ( dF1(x+0.5*Tau(x)) + dF0 (x-0.5*Tau(x))) * (  F1(x+0.5*Tau(x)) -   F0(x-0.5*Tau(x)))
                                                  +  kappa * (d2F1(x+0.5*Tau(x)) + d2F0(x-0.5*Tau(x))) * ( dF1(x+0.5*Tau(x)) -  dF0(x-0.5*Tau(x)))
                                                  +    eta * (d3F1(x+0.5*Tau(x)) + d3F0(x-0.5*Tau(x))) * (d2F1(x+0.5*Tau(x)) - d2F0(x-0.5*Tau(x)))
                                                )
    n = nPoints(Z)
    dx_mse = [ Taux(i) for i in xrange(n) ]
    dy_mse = [ Tauy(i) for i in xrange(n) ]
    return dx_mse, dy_mse
def phi(Z):
    return I(mse(Z))
def dphi(Z):
    dx_mse, dy_mse = dmse(Z)
    #return np.concatenate(([0.0],I(dx_mse[1:-1]),[0.0],I(dy_mse)))
    X,Y,n = split(Z)
    return np.concatenate(([0.0]*n,I(dy_mse)))


# optimization
ZZ = []
#function called by the optimizer to store intermediate values of Z=(X,Y)
def callback(Z):
    n = nPoints(Z)
    print 'Current vals {}!'.format([ (Z[i],Z[n+i]) for i in xrange(n) ])
    ZZ.append(Z)

#optimize
x0 = np.linspace(xmin,xmax,n0)
y0 = np.zeros(n0) + 0.5*Tau_max*(2.0*np.random.random(n0)-1.0)

interp = cubic.CubicSplineInterpolator()
interp.configure(x0,mode=mode,gen_grad_X=True)
(Tau0, Tau0_x, Tau0_y) = interp.reinterpolate(y0,gen_grad_Y=True)

X0=x0
Y0=y0
for i in xrange(2):
    assert X0.size==Y0.size
    n = X0.size
    Dx = L/n
    DX = X0[1:n]-X0[0:n-1]
    Xbounds = [(X0[ 0]-0.45*DX[  0],X0[ 0]+0.45*DX[ 0])]                         \
            + [(X0[ i]-0.45*DX[i-1],X0[ i]+0.45*DX[ i]) for i in xrange(1,n-1)]  \
            + [(X0[-1]-0.45*DX[ -1],X0[-1]+0.45*DX[-1])]
    Ybounds = [(Tau_min,Tau_max)]*n
    
    Z0       = concat(X0,Y0)
    Zbounds  = concat(Xbounds,Ybounds)
    callback(Z0)
    res = sp.optimize.minimize(fun=phi, x0=Z0, jac=dphi, method='TNC', bounds=Zbounds, tol=eps, callback=callback)

    Z_opt = res.x
    X_opt,Y_opt,n = split(Z_opt)
    (Tau_opt,_,_) = _Tau(Z_opt)
    DX = X_opt[1:n]-X_opt[0:n-1]
    X0 = []
    for i,dx in enumerate(DX):
        x = X_opt[i]
        X0.append(x)
        if dx > 0.5*Dx:
            X0.append(x+0.5*dx)
    X0.append(xmax)
    X0 = np.asarray(X0)
    Y0 = Tau_opt(X0)




# generate intermediate optimization results
P = len(ZZ)
steps = range(P)
Taus  = [_Tau(zz)[0] for zz in ZZ]
mses  = [mse(zz)  for zz in ZZ]
dmses = [(dmse(zz)) for zz in ZZ]
phis  = [phi(zz)  for zz in ZZ]
dphis = [dphi(zz) for zz in ZZ]
dphis_norm2 = [np.dot(df,df) for df in dphis]

#configure plot animation
fig, ss = plt.subplots(2,1)

fixed = [
    #initial delay guess
    ss[0].plot(Xp, Tau0(Xp), 'm--',alpha=0.5,label='Initial delay guess')[0],
    ss[0].plot(x0,y0, 'og')[0],
    #initial functions
    ss[1].plot(Xp, f0(Xp), 'r--', alpha=0.5, label='f0(x)')[0],
    ss[1].plot(Xp, f1(Xp), 'b--', alpha=0.5, label='f1(x)')[0],
]
animated = [
    #iteration text
    ss[0].text(0.5,0.8,'Iteration 0', fontsize=20, horizontalalignment='center', transform=ss[0].transAxes),
    #current delay guess
    ss[0].plot(Xp, Tau0(Xp), 'm',label='Current delay guess')[0],
    ss[0].plot(x0,y0, 'om')[0],
    #current function reconstruction
    ss[1].plot(Xp, F0(Xp-0.5*Tau0(Xp)), 'r', label='f0(x-0.5*tau(x))')[0],
    ss[1].plot(Xp, F1(Xp+0.5*Tau0(Xp)), 'b', label='f1(x+0.5*tau(x))')[0],
]

for s in ss: 
    s.set_xlim(xmin,xmax)
    s.legend()
ss[0].set_ylim(alpha*Tau_min,alpha*Tau_max)
ss[1].set_ylim(alpha*fmin,alpha*fmax)

#animation
def init():
    for p in animated: 
        p.set_data([],[])
    return animated

def animate(i):
    X,Y,n = split(ZZ[i])
    #iteration text
    animated[0].set_text('Iteration {}'.format(i))
    #current delay guess
    animated[1].set_data(Xp,Taus[i](Xp))
    animated[2].set_data(X,Y)
    #current function reconstruction
    animated[3].set_data(Xp, F0(Xp-0.5*Taus[i](Xp)))
    animated[4].set_data(Xp, F1(Xp+0.5*Taus[i](Xp)))

    return animated

anim = anim.FuncAnimation(fig, animate, frames=P,interval=int(delay*1000), blit=False, repeat=True)
plt.show()
