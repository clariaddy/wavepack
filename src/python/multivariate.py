# -*- coding: utf-8 -*-

import sys, os, trace

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
import scipy.signal as signal
from scipy import fftpack
from scipy.stats.stats import pearsonr 
from numpy.linalg import norm
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
#local includes
import env
import wavepack as wp

### input variables and configuration ###
# 
# source data files and names
data_dir        = env.data_dir
plot_script_dir = env.plot_script_dir

data_src  = [ ('CO2.txt','CO2',0,1),    # format = (file_name, arbitrary_data_name, time_column_id, data_column_id)
              ('ATS.txt','ATS',0,2),('CH4.txt','CH4',0,1)]   # warning: column ids begin from 0

# interpolation parameters
remove_tendency = True
filtered_process= True
tendency_order  = 1
P               = 10
N               = 2**P     #resampled points count
timescaling     = 1.0/1000.0 #to kyrs

# CWT parameters

wave_family = wp.WaveletFamily('cmor')
wave        = wave_family.wavelet({'omega0':6.0}) 

#wave_family = wp.WaveletFamily('paul')
#wave        = wave_family.wavelet({'m':6.0})
psizero     =wave.psi_0(0)
dj     = 0.05    # 1/dj suboctave per octave
spo    = int(np.floor(1.0/dj))
jmin   = None    # if None, default jmin value is computed
jmax   = None    # if None, default jmax value is computed


# XWT parameters
beta=0.4
smoothing_kernel = np.mean
ws=int(np.ceil(beta/dj))
wt=2**(P-10) # P>=10
smoothing_window = (ws+(ws%2==0),wt+(wt%2==0))


# confidense levels
p_level = 0.80   # confidence level probability
monte_carlo_batch_size  = 16
monte_carlo_batch_count = 32

# plot parameters
show_plots = False
save_plots = True
save_dir   = os.getcwd() + '/out' 
plot_ext = 'png'

time_label  = r'time $(kyrs)$'
data0_label = r'CO2 $(ppmv)$'
data1_label = r'Temperature $(^{\circ}C)$'
udata0_label = r'Unity Normalized CO2'
udata1_label = r'Unity Normalized Temperature'

timedelay_label=r'weighted time delay'
phaseshift_label=r'weighted phaseshift'
data0_color = 'r'
data1_color = 'b'
plottitle_kargs = { 'fontsize':18, 'color':'r' }

plot_scaling = 3.0
plt.rcParams['figure.figsize'] = [plot_scaling*x for x in plt.rcParams['figure.figsize']]
#
### end of configuration ###


### print config ###
print \
"""
=== CONFIGURATION ===
    Inputs:
        data_dir -> {}
        data_src    -> {}
    Interpolation:
        remove_tendency  -> {}
        tendency_order   -> {}
        N (resampled pts)-> {}
        timescaling      -> {}
    CWT parameters:
        wave_family -> {}
        wavelet     -> {}
        dj          -> {}
        jmin        -> {}
        jmax        -> {}
    XWT parameters:
        smoothing_kernel -> {}
        smoothing_window -> {}
    Confidense levels:
        condidense level -> {}
        MT batch size    -> {}
        MT batch  count  -> {}
        MT sample count  -> {}
    Plotting:
        show_plots  -> {}
        save_plots  -> {}
        save_dir    -> {}
====================
""".format(data_dir, data_src, 
           remove_tendency, tendency_order, N, timescaling, 
           wave_family, wave, dj, jmin, jmax,
           smoothing_kernel, smoothing_window,
           p_level, monte_carlo_batch_count, monte_carlo_batch_size, monte_carlo_batch_count*monte_carlo_batch_size,
           show_plots, save_plots, save_dir)

monte_carlo_batch_size*monte_carlo_batch_count

### load input data ###
print '=== DATA PREPROCESSING ==='
print 'Loading data...',
fn0 = data_src[0][1]
fn1 = data_src[1][1]
fn2 = data_src[2][1]
assert fn0 != fn1, 'Data names should be differents! {} == {}'.format(fn0,fn1)
assert fn1 != fn2, 'Data names should be differents! {} == {}'.format(fn1,fn2)
assert fn0 != fn2, 'Data names should be differents! {} == {}'.format(fn0,fn2)
data = wp.DataSeries(dtype=float)
data.loadFromTxt(data_dir,data_src[0][0], {'t0':data_src[0][2], fn0:data_src[0][3]})
data.loadFromTxt(data_dir,data_src[1][0], {'t1':data_src[1][2], fn1:data_src[1][3]})
data.loadFromTxt(data_dir,data_src[2][0], {'t2':data_src[2][2], fn2:data_src[2][3]})
data0 = data[fn0]
data1 = data[fn1] 
data2 = data[fn2]
print 'done.'

# scale time and compute tmin and tmax
print 'Scaling time...',
t0 = data['t0']*timescaling
t1 = data['t1']*timescaling
t2 = data['t2']*timescaling
tmin = max(t0[0],  t1[0], t2[0])
tmax = min(t0[-1], t1[-1],t2[-1])
wp.plot_script(plot_script_dir,'raw_inputs', globals(), locals())
print 'done'

host = host_subplot(211, axes_class=AA.Axes)
plt.subplots_adjust(right=0.75)
par1 = host.twinx()
#par2 = host.twinx()

offset = 60
#new_fixed_axis = par2.get_grid_helper().new_fixed_axis
#par2.axis["right"] = new_fixed_axis(loc="right",axes=par2,offset=(offset, 0))

#par2.axis["right"].toggle(all=True)

#host.set_xlim(0, 2)
#host.set_ylim(0, 2)

host.set_xlabel("time(kyrs)")
host.set_ylabel("CO2(ppmv)")
par1.set_ylabel("ATS($^0 C$)")
#par2.set_ylabel("CH4(ppbv)")

p1, = host.plot(t0, data0, label="CO2(ppmv)")
p2, = par1.plot(t1, data1, label="ATS($^0 C$)")
#p3, = par2.plot(t2, data2, label="CH4(ppbv)")

host.legend()

host.axis["left"].label.set_color(p1.get_color())
par1.axis["right"].label.set_color(p2.get_color())
#par2.axis["right"].label.set_color(p3.get_color())
plt.xlim(tmin,tmax)
plt.draw()
plt.grid()
plt.show()

    #plt.savefig("Test")



print 'Interpolating on same timescales...'
interp = wp.TimeSeries(tmin, tmax, N, dtype=float)
interp.interpolate(fn0, t0, data0)
interp.interpolate(fn1, t1, data1)
interp.interpolate(fn2, t2, data2)
wp.plot_script(plot_script_dir,'interpolation', globals(), locals())
print interp
print

# normalize inputs
print 'Normalizing inputs...',
times = interp['t']
data0 = interp[fn0]
data0 = (data0 - np.mean(data0)) / np.std(data0)
data1 = interp[fn1]
data1 = (data1 - np.mean(data1)) / np.std(data1)
data2 = interp[fn2]
data2 = (data2 - np.mean(data2)) / np.std(data2)


data0s3 = sp.ndimage.filters.gaussian_filter1d(data0, sigma=8.0)
data1s3 = sp.ndimage.filters.gaussian_filter1d(data1, sigma=8.0)
#data2_s = sp.ndimage.filters.gaussian_filter1d(data2, sigma=8.0)
# wp.plot_script(plot_script_dir,'normalization', globals(), locals())
# wp.plot_script(plot_script_dir,'renormalization_smoothed', globals(), locals())
print 'done'

# remove tendency
if remove_tendency:
    print 'Removing tendency...',
    model0     = np.polyfit(times, data0, tendency_order)
    predicted0 = np.polyval(model0, times)
        
    model1     = np.polyfit(times, data1, tendency_order)
    predicted1 = np.polyval(model1, times)

    model2     = np.polyfit(times, data2, tendency_order)
    predicted2 = np.polyval(model2, times)
        


    model = (model0 + model1 +model2)/3.0
    predicted = np.polyval(model, times)
    
    olddata0   = data0
    data0      = data0 - predicted

    olddata1   = data1
    data1      = data1 - predicted

    olddata2   = data2
    data2      = data2 - predicted


    #plt.xcorr(data0,data1, usevlines=True, maxlags=None,normed=True,lw=2)
    #plt.acorr(data1,usevlines=True, maxlags=None,normed=True,lw=2)
    #plt.show()
    
    wp.plot_script(plot_script_dir,'tendency_removal', globals(), locals())
    print 'done'

    print '''Tendency fitting with polynomial of order {}:
    {:12s}: fitted polynomial {}
    {:12s}: fitted polynomial {}
    => mean tendency = {}'''.format(tendency_order, fn0, model0, fn1, model1, model)

    print 'Renormalizing inputs...',
    data0 = (data0 - np.mean(data0)) / np.std(data0)
    data1 = (data1 - np.mean(data1)) / np.std(data1)
    data0s1 = sp.ndimage.filters.gaussian_filter1d(data0, sigma=2.0)
    data1s1 = sp.ndimage.filters.gaussian_filter1d(data1, sigma=2.0)
    data0s2 = sp.ndimage.filters.gaussian_filter1d(data0, sigma=4.0)
    data1s2 = sp.ndimage.filters.gaussian_filter1d(data1, sigma=4.0)
    wp.plot_script(plot_script_dir,'renormalization', globals(), locals())
    print 'done'

data_p0=data0
data_p1=data1
sigpow0 =np.sum(data0**2)/N
sigpow1 =np.sum(data1**2)/N

donne0 =data0
donne1 =data1

# estimate signals parameters as if th2y where AR1 like
print 'Estimating AR1 signal parameters...'
alpha_0,mu2_0,sigma_e_0 = wp.estimate_AR1_parameters(data0)
alpha_1,mu2_1,sigma_e_1 = wp.estimate_AR1_parameters(data1)
mean_0 = np.mean(data0)
mean_1 = np.mean(data1)
sigma_0 = np.std(data0)
sigma_1 = np.std(data1)
msg= '\t{:12s}: mean={:.3f}, sigma={:.3f}, alpha={:.3f}, mu2={:.3f}, sigma_e={:.3f}'
print msg.format(fn0, mean_0, sigma_0, alpha_0, mu2_0, sigma_e_0)
print msg.format(fn1, mean_1, sigma_1, alpha_1, mu2_1, sigma_e_1)


# extend signals with zeroes on the boundaries
print 'Extending signals with zeros...',
interp[fn0] = data0
interp[fn1] = data1
ext = interp.extend(ext='zeros')
print 'done'
print ext

# get back extended signals and parameters
times = interp['t']
data0 = ext[fn0]
data1 = ext[fn1]

dt    = ext.dt      # = (tmax - tmin)/(N-1)
L     = ext.L       # = tmax - tmin
C     = ext.C       # = (tmax + tmin)/2.0

print '=========================='


### Compute CWTs ###
print
print '=== DATA ANALYSIS ==='

flambda  = wave.fourier_wavelength()
efolding = wave.efolding(attenuation=np.exp(-2))
smin = 2.0*dt                        
smax = L /(2.0*efolding) * (1.0/2.0) 
pmin = wave.periods(smin)
pmax = wave.periods(smax)
print """Using:
    mother wavelet     ->  {}
    Fourier wavelength ->  {:.3f}
    e-folding          ->  {:.3f}
    smin               ->  {:.3f}
    smax               ->  {:.3f}
    pmin               ->  {:.3f}
    pmax               ->  {:.3f}
    dj                 ->  {:.3f}
    subfreq per octave -> ~{}
""".format(wave,flambda,efolding,smin,smax,pmin,pmax,dj,int(np.round(1.0/dj)))

#TODO take into account jmin and jmax from parameters
print 'Generating scales...\t',
jmax = np.log2(smax/smin)/dj
j = np.arange(jmax+1)

scales  = smin * 2.0**(j*dj)
S = scales.size

periods = wave.periods(scales)
freqs   = 1.0/periods
print 'done'

print 'Computing CWTs...\t',

CWT0 = wp.swt(data0, wave, scales, dt=dt, normalization='Kaiser')
CWT0 = CWT0[:,N/2:N+N/2]  

CWT1 = wp.swt(data1, wave, scales, dt=dt, normalization='Kaiser')
CWT1 = CWT1[:,N/2:N+N/2] 
print 'done'

print 'Computing Power Spectrums...\t',
WP0 = np.abs(CWT0)**2
WP1 = np.abs(CWT1)**2
print 'done'

print 'Computing Arguments...\t',
A0   = np.angle(CWT0, deg=False)
A1   = np.angle(CWT1, deg=False)
print 'done'

print 'Computing Cross Wavelet Spectrum...\t',
XWT = CWT0 * np.conj(CWT1)
WPX = np.abs(XWT)**2
AX = np.angle(XWT, deg=False)
AX1 =np.arctan2(np.imag(XWT),np.real(XWT))
CWT2P = np.abs(XWT)**2 / (WP0*WP1)
CWTP  = np.sqrt(CWT2P)
print 'done'
print 'Computing Smoothed Crosswavelet Coherence or mean squared coherence...\t',
#TODO check scale^(-1) scaling
print 'windows for smoothing'
smooth = lambda X: wp.fft_smooth(X, window=smoothing_window)
XWTS   = smooth(XWT)
CWT2 = (np.abs(smooth(XWT))**2) / (smooth(WP0)*smooth(WP1))
CWT  = np.sqrt(CWT2)
AXS    = np.angle(XWTS, deg=False)
AX1S =np.arctan2(np.imag(XWTS),np.real(XWTS))




# cone of influence
print 'Computing Cone of Influence (COI)...\t',
coi = dt*flambda/efolding * np.concatenate( ( [1e-15], np.arange((N+1)/2-1), np.flipud(np.arange(0, N/2-1)), [1e-15]) )
coi_mask = np.ones(shape=CWT.shape, dtype=float) * scales[:,None]
coi_mask = (coi_mask <= coi[None,:])
print 'done'
print 'Computing global weighted wavelet power spectrum ...\t'
# smooth0
C           = CWT*coi_mask
T           = np.sum(C*periods[:,None], axis=0) / np.sum(C, axis=0)
WP0_g=np.sum(WP0*C,axis=1)/ np.sum(C, axis=1)
WP1_g=np.sum(WP1*C,axis=1)/ np.sum(C, axis=1)
print 'Computing wavelet scale average of time series ...\t'
WP0sc=WP0/scales[:,None] # scale averages
WP1sc=WP1/scales[:,None]
#ycorr = np.fft.ifft(np.fft.fft(data0)*np.conj(np.fft.fft(data1)))/(norm(data0)*norm(data1))
WP0_savg=np.sum(WP0sc*C,axis=0)/ np.sum(C, axis=0)
WP1_savg=np.sum(WP1sc*C,axis=0)/ np.sum(C, axis=0)
WP0_savgs=np.sum(WP0sc*C,axis=1)/ np.sum(C, axis=1)
WP1_savgs=np.sum(WP1sc*C,axis=1)/ np.sum(C, axis=1)

print 'Computing time delays...\t',
print 'time delay original'
CP          =CWTP*coi_mask
TP           = np.sum(CP*periods[:,None], axis=0) / np.sum(CP, axis=0)
MCA, MCSD   = wp.mean_circular_angle(AX1, deg=False, weights=None)
for i in xrange(1,N):
    if (MCA[i]-MCA[i-1])>np.pi:
        MCA[i]=MCA[i]-np.pi
    elif(MCA[i]-MCA[i-1])<-np.pi:
        MCA[i]=MCA[i]+np.pi
time_delays     = (MCA /np.pi)*TP
time_delays_max = ((MCA+MCSD)/np.pi)*TP
time_delays_min = ((MCA-MCSD)/np.pi)*TP
time_delays2    = np.sum(CP*AX1/np.pi*periods[:,None], axis=0) / np.sum(CP, axis=0)
time_delays     = sp.ndimage.filters.gaussian_filter1d(time_delays, sigma=8.0)
time_delays2    = sp.ndimage.filters.gaussian_filter1d(time_delays2, sigma=3.0)
print 'done'
# method 2 with smoothing XWT
print 'smooth'
C          =CWT*coi_mask
T           = np.sum(C*periods[:,None], axis=0) / np.sum(C, axis=0)
MCAS, MCSDS   = wp.mean_circular_angle(AX1S, deg=False, weights=None)
for i in xrange(1,N):
    if (MCAS[i]-MCAS[i-1])>np.pi:
        MCAS[i]=MCAS[i]-np.pi
    elif(MCAS[i]-MCAS[i-1])<-np.pi:
        MCAS[i]=MCAS[i]+np.pi
time_delaysS     = (MCAS /np.pi)*T
time_delays_maxS = ((MCAS+MCSDS)/np.pi)*T
time_delays_minS = ((MCAS-MCSDS)/np.pi)*T
time_delays2S    = np.sum(C*AX1S/np.pi*periods[:,None], axis=0) / np.sum(C, axis=0)
time_delaysS     = sp.ndimage.filters.gaussian_filter1d(time_delaysS, sigma=8.0)
err_ =np.abs(time_delays2-time_delays2S)
print 'done'

print 'done'

print 'phase diagram'
arrows = (100,40) #arrow count in time axis and scale axis
step   = (N//arrows[0],S//arrows[1])
X,Y = np.meshgrid(times[::step[0]],scales[::step[1]])
AXq = (AX*CWT)[::step[1],::step[0]].ravel()
X = X.ravel()
Y = Y.ravel()
U = np.cos(AXq)
V = np.sin(AXq)
plot1 = plt.figure()
plt.yscale('log')
q=plt.quiver(X,Y,U,V,width=1e-3,scale=100,pivot='mid')
ax=plt.axis([tmin,tmax,pmin,pmax])
plt.grid(True)
plt.title('Quive Plot, Dynamic Colours')



wp.plot_script(plot_script_dir,'global_spectrum', globals(), locals())
wp.plot_script(plot_script_dir,'average_scale',globals(), locals())
wp.plot_script(plot_script_dir,'time_delay', globals(), locals())
wp.plot_script(plot_script_dir,'time_delay_smooth', globals(), locals())
#wp.plot_script(plot_script_dir,'time_comparison', globals(), locals())
#wp.plot_script(plot_script_dir,'time_comparison1', globals(), locals())
#wp.plot_script(plot_script_dir,'weighted_time_delay', globals(), locals())
#wp.plot_script(plot_script_dir,'error_comparisons', globals(), locals())

print 'done'
# data reconstruction
omeg0   =A0*freqs[:,None]
psi_hatdata0 = np.sqrt((2.0*np.pi*smax)/dt) * wave.psi_hat_0(smax*omeg0)
psi_hatdata0=np.array(psi_hatdata0, dtype=complex)
Wdelta0 =np.sum(np.conj(psi_hatdata0),axis =1)/N
Cdelta0 =(dj*np.sqrt(dt)/(psizero))*np.sum(np.real(Wdelta0)/np.sqrt(scales),axis=0)
Cda0 = CWT0/np.sqrt(scales)[:,None]
dataCWT0 = ((dj*np.sqrt(dt))/(Cdelta0*psizero))*np.sum(np.real(Cda0),axis=0)

omeg1   =A1*freqs[:,None]
psi_hatdata1 = np.sqrt((2.0*np.pi*smax)/dt)* wave.psi_hat_0(smax*omeg1)
psi_hatdata1=np.array(psi_hatdata1, dtype=complex)
Wdelta1 =np.sum(np.conj(psi_hatdata1),axis =1)/N
Cdelta1 =(dj*np.sqrt(dt)/(psizero))*np.sum(np.real(Wdelta1)/np.sqrt(scales),axis=0)
Cda1 = CWT1/np.sqrt(scales)[:,None]
dataCWT1 = ((dj*np.sqrt(dt))/(Cdelta1*psizero))*np.sum(np.real(Cda1),axis=0)

recpow0 =np.sum(WP0,axis=0)/S 
recpow0 = np.sum(recpow0)/N

recpow1 =np.sum(WP1,axis=0)/S 
recpow1 = np.sum(recpow1)/N

aaa0      =sigpow0/recpow0
aaa1      =sigpow1/recpow1

dataCWT0 =(dataCWT0-np.mean(dataCWT0)/np.std(dataCWT0))*aaa0
dataCWT1 =(dataCWT1-np.mean(dataCWT1)/np.std(dataCWT1))*aaa1

donne0 =(donne0-np.mean(donne0)/np.std(donne0))
donne1 =(donne1-np.mean(donne1)/np.std(donne1))

wp.plot_script(plot_script_dir,'reconstructed_signals', globals(), locals())

print '====================='
print
print '==== STATISTICAL ANALYSIS ===='

# WP significant levels
print 'Computing WP Significant Level...\t'
WP_significant_level = wp.WP_significant_level(wave, p_level)

Wk  = 2.0*np.pi*np.fft.fftfreq(N)/dt
Ps0 = wp._Ps(scales,wave,alpha_0,Wk,dt)
Ps1 = wp._Ps(scales,wave,alpha_1,Wk,dt)
WP0s = WP0 / (sigma_0**2 * Ps0[:,None])
WP1s = WP1 / (sigma_1**2 * Ps1[:,None])
print '   level={:5f}'.format(WP_significant_level)
print

# XWT significant level
print 'Estimating Crosswavelet Significant Level with Monte Carlo simulations...\t'

print '    == AR1 input models =='
msg= '        {:12s} -> mean={:.4f}, sigma={:.4f}, alpha={:.4f}, mu2={:.4f}, sigma_e={:.4f}'
print msg.format(fn0, mean_0, sigma_0, alpha_0, mu2_0, sigma_e_0)
print msg.format(fn1, mean_1, sigma_1, alpha_1, mu2_1, sigma_e_1)

print '    == MonteCarlo parameters =='
print '''        Batch size   -> {}
        Batch count  -> {}
        Sample count -> {}'''.format(monte_carlo_batch_size, monte_carlo_batch_count, monte_carlo_batch_size*monte_carlo_batch_count)
print '    == Simulation =='
msg = '        computing batch {:3d}/{:3d}...\t'

B = monte_carlo_batch_size
sv = np.zeros(shape=S, dtype=CWT.dtype)
for k in xrange(1,monte_carlo_batch_count+1):
    print msg.format(k,monte_carlo_batch_count),

    # generate signals
    MT_D0 = wp.gen_AR1(alpha_0, N, M=B, m=0.0, sigma_e=sigma_e_0)
    MT_D1 = wp.gen_AR1(alpha_1, N, M=B, m=0.0, sigma_e=sigma_e_1)

    # extend singals with zeros
    Z = np.zeros(shape=(B,N/2), dtype=float)
    MT_D0 = np.concatenate( (Z,MT_D0,Z), axis=1 )
    MT_D1 = np.concatenate( (Z,MT_D1,Z), axis=1 )

    # compute CWTs
    MT_CWT0 = wp.swt_many(MT_D0, wave, scales, dt=dt, normalization='Kaiser')
    MT_CWT1 = wp.swt_many(MT_D1, wave, scales, dt=dt, normalization='Kaiser')
    
    MT_CWT0 = MT_CWT0[...,N/2:N+N/2]  
    MT_CWT1 = MT_CWT1[...,N/2:N+N/2]  

    # power spectrums
    MT_WP0 = np.abs(MT_CWT0)**2
    MT_WP1 = np.abs(MT_CWT1)**2
    
    # compute cross wavelet spectrum and crosswavelet coherence or mean squared coherence
    smooth = lambda X: wp.fft_smooth_many(X, window=smoothing_window)
    MT_XWT = MT_CWT0 * np.conj(MT_CWT1)
    MT_CWT = np.abs(smooth(MT_XWT)) / np.sqrt(smooth(MT_WP0)*smooth(MT_WP1))

    assert p_level>=0.0 and p_level<=1.0
    X = np.sort(np.transpose(MT_CWT*coi_mask,(1,2,0)).reshape(S,B*N), axis=1)
    idx = p_level*(B*N-1)
    idx_upper = min(B*N-1, int(np.ceil(idx)))
    idx_down  = max(0,     int(np.floor(idx)))
    alpha = np.mod(idx, 1.0)
    sv += (1.0-alpha)*X[:,idx_down] + alpha*X[:,idx_upper]
    
    #Save plots
    # fig = plt.figure()
    # figname = 'batch_'+str(k)+'_power_spectrums'
    # plot_wp = lambda X,title,subplotid,**kargs: wp.plotWaveletPower(times,periods,X,tmin,tmax,smin,smax,pmin,pmax,coi=coi,title=title,subplotid=subplotid, **kargs)
    # plot_wp(MT_WP0[0],fn0+' Wavelet Power Spectrum', 221)
    # plot_wp(MT_WP1[0],fn1+' Wavelet Power Spectrum', 222)
    # plot_wp(np.abs(MT_XWT[0])**2,'Crosswavelet Power Spectrum', 223)
    # plot_wp(MT_CWT[0]*coi_mask,'Crosswavelet Coherence', 224, plt_fun=lambda x:x)
    # wp.plot(fig,save_dir,figname,plot_ext,show_plots,save_plots,add_order_id=False)
    
    print 'done'

sv /= monte_carlo_batch_count
CWTs = CWT / sv[:,None]

print '=============================='

### plot everything
fig = plt.figure()
figname = 'power_spectrums'

plot_wp = lambda X,title,subplotid,**kargs: wp.plotWaveletPower(times,periods,X,tmin,tmax,smin,smax,pmin,pmax,coi=coi,title=title,subplotid=subplotid, **kargs)
plot_wp(WP0,fn0+' Wavelet Power Spectrum', 221, level_var=WP0s, levels=[WP_significant_level])
plot_wp(WP1,fn1+' Wavelet Power Spectrum', 222, level_var=WP1s, levels=[WP_significant_level])
plot_wp(WPX,'Crosswavelet Power Spectrum', 223, level_var=CWTs, levels=[1.0])
plot_wp(CWT*coi_mask,'Crosswavelet Coherence', 224, level_var=CWTs, levels=[1.0], plt_fun=lambda x:x)
wp.plot(fig,save_dir,figname,plot_ext,show_plots,save_plots)
wp.plot_script(plot_script_dir,'global_spectrum', globals(), locals())
