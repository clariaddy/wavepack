import os
import sys
import math
import numpy as np
import matplotlib.pyplot as plt
import openpyxl as pyxl
from scipy import interpolate



# load data from excel sheet-----------------------------------------------------------------------------------------  
def load_excel_data(folder,filename,worksheet_name,columns_types):
    filepath = folder + '/' + filename
    workbook = pyxl.load_workbook(filepath)
    worksheet = workbook[worksheet_name]
    if(worksheet == None):
        print "Could not find '" + worksheet_name + "' in file '" + filepath + "' !"
        sys.exit(1)
    
    #extract values
    extracting_values = False
    column_ids = {}
    data = {}
    for current_row in worksheet.iter_rows():
        if extracting_values:
            for column_name in columns_types:
                column_id = column_ids[column_name]
                current_value = columns_types[column_name](current_row[column_id].value)
                data[column_name].append(current_value)
        else:
            if current_row[0].value == None: #skip empty lines
                continue
            else: #first non empty line, extract column positions according to input column names
                k=0
                for col in current_row:
                    cell = col.value
                    if cell in columns_types:
                        column_ids[cell] = k
                        data[cell] = []
                    k = k+1
                if len(column_ids) != len(columns_types):
                    print "Could not find all requested columns !"
                    print "Asked for '" + str(columns_types) + "' but only found " + str(column_ids) + " !"
                    sys.exit(1)
                extracting_values = True
    return data
#--------------------------------------------------------------------------------------------------------------------



def prepare_data(data_folder, file_names, use_custom_timestep=False, custom_timestep=0.0, plot_figures=True):

    # load data from files
    file_path = data_folder+'/'+file_names[0]
    print "Loading data from '",file_path,"'..."
    data = np.loadtxt(file_path)
    age1, record1 = data[:,1].tolist(), data[:,2].tolist()
    len1 = len(age1)
    assert len(age1) == len(record1), "Length mismatch in file '"+file_path+"' !"
    print "\tSuccessfully loaded",len1,"data."
    print "\tTmin=",min(age1),", Tmax=",max(age1)


    file_path = data_folder+'/'+file_names[1]
    print "Loading data from '",file_path,"'..."
    data = np.loadtxt(file_path)
    age2, record2 = data[:,1].tolist(), data[:,2].tolist()
    len2 = len(age2)
    assert len(age2) == len(record2), "Length mismatch in file '"+file_path+"' !"
    print "\tSuccessfully loaded",len2,"data."
    print "\tTmin=",min(age2),", Tmax=",max(age2)

    min_age=max( age1[0] , age2[0]  )
    max_age=min( age1[-1], age2[-1] )
    print "Common time interval: [", min_age,",",max_age,"]"


#Search for the common age interval
# case of no common interval
    if (min_age > max_age):
        sys.exit("No common age interval for the two records")
# else remove extra data
    else:
        while age1[1] <= min_age:
            del age1[0]
            del record1[0]
            len1 -= 1
        while age1[-2] >= max_age:
            del age1[-1]
            del record1[-1]
            len1 -= 1
        while age2[1] > min_age:
            del age2[0]
            del record2[0]
            len2 -= 1
        while age2[-2] >= max_age:
            del age2[-1]
            del record2[-1]
            len2 -= 1
        assert(len(age1)==len1)
        assert(len(age2)==len2)


#case of index problems
    print "Checking for same values..."

    i=0
    while i<len(age1):
        same_ages_ids = [ j for j in xrange(i+1,len1) if age1[i]==age1[j] ]
        for j in reversed(same_ages_ids):
            print "\twarning: data number",j,"in '",file_names[0],"' was already sampled at t=",age1[i],"data number",i," and is ignored."
            del age1[j]
            del record1[j]
            len1 -= 1
        i+= 1
    assert(len(age1)==len1)
    age1    = np.array(age1)
    record1 = np.array(record1)

    i=0
    while i<len(age2):
        same_ages_ids = [ j for j in xrange(i+1,len2) if age2[i]==age2[j] ]
        for j in reversed(same_ages_ids):
            print "\twarning: data number",j,"in '",file_names[1],"' was already sampled at t=",age2[i],"data number",i," and is ignored."
            del age2[j]
            del record2[j]
            len2 -= 1
        i+= 1
    assert(len(age2)==len2)
    age2    = np.array(age2)
    record2 = np.array(record2)
            

#--------------------------------------------------------------------------------------------------------
#--------------------RE-SAMPLING THE DATA ON A COMMON SCALE'--------------16/03/2016
#----------------------------------------------------------------------------------------------------------

    print 'Re-sampling the data on a common scale...'

    if use_custom_timestep:
        print "\tusing custom timestep."
        timestep = custom_timestep
    else:
        print "\tusing mean timestep."

        dt1 = age1[1:] - age1[0:-1]
        mean_agestep1 = np.mean(dt1)
        print '\tmean age step for',file_names[0],'is', mean_agestep1
        
        dt2 = age2[1:] - age2[0:-1]
        mean_agestep2 = np.mean(dt2)
        print '\tmean age step for',file_names[1],'is', mean_agestep2

        timestep = min(mean_agestep1, mean_agestep2)
        print '\tusing new dt=',timestep,'.'    


#---------------------------------------------------------------------------------------------------
# working on interpolation and plotting curves figure-----------------17/03/2016--------------------------
#----------------------------------------------------------------------------------------------------------

#interpolation
    time = np.arange(min_age,max_age,timestep,dtype=float)
    new_record1=interpolate.pchip_interpolate(age1, record1, time, der=0, axis=0)
    new_record2=interpolate.pchip_interpolate(age2, record2, time, der=0, axis=0)

#plotting figures
    if plot_figures:
        plt.subplot(211)
        plt.plot(age1, record1, 'b.-',label ="Original data")
        plt.ylabel('CO2 measurement')
        plt.plot(time, new_record1, 'y-',label ="re-sampled data")
        plt.ylabel('CO2 measurement')
        plt.xlim(min_age,max_age)
        plt.legend()
        plt.subplot(212)
        plt.plot(age2, record2, label ="Original data")
        plt.plot(time, new_record2, 'r',label ="re-sampled data")
        plt.xlabel('age (kyr)')
        plt.xlim(min_age,max_age)
        plt.ylabel('Temperature')
        plt.legend()
        plt.show()

    cc_record=np.corrcoef([new_record1,new_record2])
    print "Actual correlation:"
    print cc_record

    return timestep,time,[np.array(new_record1), np.array(new_record2)]


