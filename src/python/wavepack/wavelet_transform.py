
import numpy as np
import matplotlib.pyplot as plt

def swt(data,wave,scales,dt,dtype=float,normalization=None):
    scales = np.array(scales,dtype=dtype)
    x = np.array(data, dtype=dtype)
    X = np.fft.fft(x)

    n = x.size
    w = 2.0*np.pi*np.fft.fftfreq(n)/dt
    
    #print n
    #print dt
    #print "wmin = Wo/smax = ", 6.0/scales[-1]
    #print "wmax = Wo/smin = ", 6.0/scales[0]
    #print w
    #plt.plot(w,np.abs(wave.psi_hat(w,scales[0],dt)))
    #plt.plot(w,np.abs(wave.psi_hat(w,scales[-1],dt)))
    #plt.plot(w,np.log2(np.abs(X)))
    #plt.plot(w,np.abs(wave.psi_hat(w,scales[0],dt)*X))
    #plt.plot(w,np.abs(wave.psi_hat(w,scales[-1],dt)*X))
    #plt.show()

    if scales.size==1:
        C = np.zeros(n, dtype=complex)
    else:
        C = np.zeros((scales.size,n), dtype=complex)
    i = 0
    for s in scales:
        psi_hat = wave.psi_hat(w,s,dt)
        c = np.fft.ifft(X * np.conj(psi_hat))
        if scales.size==1:
            C = c
        else:
            C[i,:] = c
        i+=1

    return C

def swt_many(x,wave,scales,dt,dtype=float,normalization=None):

    assert x.ndim == 2
    assert scales.ndim == 1

    M = x.shape[0] #samples count
    N = x.shape[1] #samples length
    S = scales.size
    
    X = np.fft.fft(x, axis=1)
    W = 2.0*np.pi*np.fft.fftfreq(N)/dt

    C = np.empty((M,S,N), dtype=complex)
    for i in xrange(S):
        s = scales[i]
        psi_hat = wave.psi_hat(W,s,dt)
        C[:,i,:] = np.fft.ifft(X * np.conj(psi_hat)[None,:], axis=1)

    return C
