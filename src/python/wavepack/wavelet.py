import math
import numpy as np
import scipy.special as sp
import matplotlib.pyplot as plt
import numpy.polynomial.polynomial as poly

class WaveletFamily:
    name=None

    def __init__(self, name):
        if name not in self.families():
            print "WaveletFamily name should be one of the following:",self.families()
            return

        self.name=name

    def parameters(self):
        if(self.name == 'cmor'):
            return { 'omega0':'Time/frequency tradeoff (float >=6.0)' }
        elif self.name == 'paul':
            return { 'm':'order (float >=1.0)' }
        elif self.name == 'dog':
            return { 'm':'order (int >= 0)' }
        else:
            raise NotImplementedError('Not implemented yet')

    def wavelet(self,parameters):
        if self.name == 'cmor':
            return CMorletWavelet(parameters)
        elif self.name == 'paul':
            return PaulWavelet(parameters)
        elif self.name == 'dog':
            return DerivativeOfGaussiansWavelet(parameters)
        else:
            raise NotImplementedError('Not implemented yet')

    def plot(self,count=4,parameters=None,shape=None,plotTime=True,plotFreq=False,attenuation=np.exp(-2.0)):

        if parameters is None:
            parameters = self.demo_parameters(count)
        count=len(parameters)

        assert plotTime or plotFreq
        if shape==None:
            shape = (int(plotTime)+int(plotFreq),count)
       
        f = plt.figure()
        plt.title('The ' + self.fname() + ' Family' )

        x = np.linspace(0.0,2.0*np.pi,100)
        for k,p in zip(range(1,count+1),parameters):
            wave = self.wavelet(p)

            if(plotTime):
                subplot = plt.subplot(shape[0],shape[1],k)
                wave.plotTime(subplot=subplot)
           
            if(plotFreq):
                subplot = plt.subplot(shape[0],shape[1],plotTime*count+k)
                wave.plotFreq(subplot=subplot)

    def demo_parameters(self,count):
        if self.name == 'cmor':
            Wo_min = 6.0
            Wo_max = 18.0 
            params = np.linspace(Wo_min,Wo_max,count)
            return [{'omega0':p} for p in params]
        elif self.name == 'paul':
            m_min = 1.0
            m_max = 50.0
            params = np.floor(np.linspace(m_min,m_max,count))
            return [{'m':p} for p in params]
        elif self.name == 'dog':
            m_min = 1.0
            m_max = 10.0
            params = np.floor(np.linspace(m_min,m_max,count))
            return [{'m':int(p)} for p in params]
        else:
            raise NotImplementedError('Not implemented yet')

    def fname(self):
        return self.families_full()[self.name]

    @staticmethod
    def families():
        return ['cmor','paul', 'dog', 'haar', 'db', 'sym', 'coif', 'bior', 'rbio', 'dmey']

    @staticmethod
    def families_full():
        return {
                'cmor':'Complex Morlet',
                'paul':'Paul', 
                'dog' :'Derivative Of Gaussian (DOG)',
                'haar':'Haar', 
                'db':'Daubechies', 
                'sym':'Symlets', 
                'coif':'Coiflets', 
                'bior':'BiorSplines', 
                'rbio':'ReverseBior', 
                'dmey':'DMeyer'
            }


class Wavelet:
    
    #scaling functions
    def scaling(s,dt,config='Kaiser'):
        if config=='Kaiser':
            return np.sqrt(dt)
        elif config=='Torrence':
            return np.sqrt(dt/s)
        else:
            print 'Error: Unknown scaling!'
            raise Exception('Runtime error!')
        return 'Torrence'
    
    def psi(self,t,s,dt):
        return np.sqrt(dt/s) * self.psi_0(t/s)
    def psi_hat(self,omega,s,dt):
        return np.sqrt((2.0*np.pi*s)/dt) * self.psi_hat_0(s*omega)
        #return self.psi_hat_0(s*omega)

    def plotTime(self, N=1001, plot_parameters=True, attenuation=np.exp(-2.0),subplot=None):
        if subplot is None:
            subplot = plt.subplot(1,1,1)

        mu       = self.localization()
        sigma    = np.sqrt(self.dispersion())
        Tau      = self.efolding(attenuation)
        flambda  = self.fourier_wavelength()
        
        xmin  = mu - 6.0*sigma
        xmax  = mu + 6.0*sigma
        L = xmax - xmin

        x = np.linspace(xmin,xmax,N)
        F = self.psi_0(x)
        Fm = np.abs(F)
       
        # compute and plot values
        if self.type() == 'complex':
            Fr = np.real(F)
            Fi = np.imag(F)
            subplot.plot(x,Fr, color='b', label='$\Re(\Psi_0)(t/s)$', linestyle='-')
            subplot.plot(x,Fi, color='g', label='$\Im(\Psi_0)(t/s)$', linestyle='-')
            Fmax = np.max((Fm,Fr,Fi))
            Fmin = np.min((Fm,Fr,Fi))
        else:
            Fmax = np.max((F,Fm))
            Fmin = np.min((F,Fm))
            subplot.plot(x,F, color='b', label='$\Psi_0(t/s)$')
        subplot.plot(x,Fm, color='r', label='$|\Psi_0(t/s)|$')
        
        H = Fmax - Fmin
        ymin = Fmin - 0.10*H
        ymax = Fmax + 0.10*H

        #plot vertical lines
        T = [mu, mu+flambda, mu+Tau]
        formulas = [r'$\Psi_{\mu}^s=0$', r'$\lambda_s=\frac{2\pi}{\widehat{\Psi}_{\mu}} \cdot s$', r'$\tau_s=\sqrt{2} \cdot s$']
        colors = ['k','m','m']
        linestyles = ['-','--','--']

        for k in xrange(len(T)):
            subplot.plot([T[k],T[k]],[ymin,ymax], color=colors[k], linestyle=linestyles[k])
        for k in xrange(len(T)):
            subplot.text(T[k]+0.005*L, np.abs(self.psi_0(T[k]))+0.03*H, formulas[k], color='k', fontsize=15)
        
        #horizontal lines
        subplot.plot([xmin,xmax],[0.0,0.0], 'k')

        #plot parameters
        if plot_parameters:
            params = self.plot_parameters
            params_str = "".join(['$'+k+' = '+str(params[k])+'$\n' for k in params])
            subplot.text(xmin+0.05*L,ymax-0.05*H, params_str[:-1], fontsize=15,
                horizontalalignment='left',
                verticalalignment='top',
                bbox={'facecolor':'red', 'alpha':0.5, 'pad':5})

        #axis
        subplot.set_xlim([xmin,xmax])
        subplot.set_ylim([ymin,ymax])
        subplot.set_xlabel('t/s')
        subplot.set_ylabel('$\Psi_0(t/s)$')
        subplot.legend(loc=3,ncol=1,borderaxespad=0.0)
        

    def plotFreq(self, N=1001,subplot=None):
        if subplot is None:
            subplot = plt.subplot(1,1,1)
        
        mu    = self.localization_hat()
        sigma = np.sqrt(self.dispersion_hat())
        
        Wmin = mu - 5.0*sigma
        Wmax = mu + 5.0*sigma
        xmin = Wmin
        xmax = Wmax
        L = xmax - xmin

        w = np.linspace(Wmin,Wmax,N)
        F = self.psi_hat_0(w)
        P = np.abs(F)**2

        Pmin = np.min(P)
        Pmax = np.max(P)
        H = Pmax - Pmin
        ymin = Pmin - 0.05*H
        ymax = Pmax + 0.10*H
       
        #plot power spectrum
        subplot.plot(w,P,color='r')

        #plot vertical lines
        T = [mu]
        formulas = [r'$\widehat{\psi}_{\mu}^s = \frac{2\pi}{\lambda} \cdot s$']
        colors = ['b']
        linestyles = ['--']
        for k in xrange(len(T)):
            subplot.plot([T[k],T[k]],[ymin,ymax], color=colors[k], linestyle=linestyles[k])
        for k in xrange(len(T)):
            subplot.text(T[k]+0.01*L, np.abs(self.psi_hat_0(T[k]))**2.0+0.01*H, formulas[k], color='k', fontsize=20)

        #plot horizontal lines
        subplot.plot([xmin,xmax],[0.0,0.0], 'k')
        #plt.plot([mu-3.0*sigma,mu+3.0*sigma],[Pmin-0.05*H,Pmin-0.05*H])
        
        #axis
        subplot.set_xlim([xmin,xmax])
        subplot.set_ylim([ymin,ymax])
        subplot.set_xlabel('$s\omega$')
        subplot.set_ylabel('$|\widehat{\Psi}_0(s\omega)|^2$')

    def periods(self,scales):
        return self.fourier_wavelength() * scales

class CMorletWavelet(Wavelet):
    def __init__(self,parameters):
        #check input parameters
        if 'omega0' not in parameters:
            print 'Error: Complex Morlet Wavelet should be created with the parameter "omega0".'
            raise Exception('Runtime error!')
        else:
            omega0 = float(parameters['omega0'])
    
        if(omega0 < 6.0):
            print 'Error: Complex Morlet Wavelet does not satisfy zero mean property with omega_0 < 6 !.'
            raise Exception('Runtime error!')

        self.omega0 = omega0
        self.plot_parameters = {'\omega_0':omega0}

    def name(self):
        return 'Complex Morlet'
    def family(self):
        return WaveletFamily('cmor')
    def type(self):
        return 'complex'
    
    #wavelet fonction and its fourier counterpart
    def psi_0(self,t):
        return (np.pi**(-1.0/4.0)) * np.exp(-(t*t)/2.0) * np.exp(1j*self.omega0*t)
    def psi_hat_0(self,omega):
        return (np.pi**(-1.0/4.0)) *  np.array(omega>0.0,dtype=float) * np.exp(-1.0/2.0*(self.omega0 - omega)**2.0) 

    # localization and dispersion to plot 
    def localization(self,s=1.0):
        return 0.0
    def dispersion(self,s=1.0):
        return 1.0/2.0 * (1.0/s**2)
    def localization_hat(self,s=1.0):
        return self.omega0 * s
    def dispersion_hat(self,s=1.0):
        return 1.0/2.0 * s**2.0

    #chatacteristic times
    def fourier_wavelength(self,s=1.0):
        Wo = self.omega0
        return (4.0*np.pi) / (Wo + np.sqrt(2+Wo*Wo)) * s
    def efolding(self,s=1.0,attenuation=np.exp(-2.0)):
        assert attenuation > 0.0 and attenuation < 1.0
        return np.sqrt(-np.log(attenuation))

    def __str__(self):
        return "Wavelet<"+self.name()+" with omega0="+str(self.omega0)+">"




class PaulWavelet(Wavelet):
    def __init__(self,parameters):
        #check input parameters
        if 'm' not in parameters:
            print 'Error: Paul Wavelet should be created with the parameter "m".'
            raise Exception('Runtime error!')
        else:
            m = float(parameters['m'])
        
        self.m = m
        self.plot_parameters = {'m':m}

        self.Cm = (1.0j**m) * (1.0/np.sqrt(np.pi)) * (2.0**(self.m)*sp.gamma(m+1) / np.sqrt(sp.gamma(2.0*m+1.0)))
        self.Cm_hat = 2.0**m / np.sqrt(m * sp.gamma(2.0*m))

    def name(self):
        return 'Paul'
    def family(self):
        return WaveletFamily('paul')
    def type(self):
        return 'complex'
    
    #wavelet fonction and its fourier counterpart
    def psi_0(self,t):
        return self.Cm * (1.0 - 1.0j*t)**(-(self.m+1.0))
    def psi_hat_0(self,omega):
        H = np.array(omega>0.0,dtype=float)
        return self.Cm_hat * H * (omega**self.m) * np.exp(-H*omega)

    # localization and dispersion to plot 
    def localization(self,s=1.0):
        return 0.0
    def dispersion(self,s=1.0):
        return (1.0/s**2.0) * 1.0/(2.0*self.m-1.0)
    def localization_hat(self,s=1.0):
        return s*(2.0*self.m + 1.0)/2.0
    def dispersion_hat(self,s=1.0):
        return (s**2) * (2.0*self.m+1.0)/4.0

    #chatacteristic times
    def fourier_wavelength(self,s=1.0):
        return 2.0*np.pi / self.localization_hat(s)
    def efolding(self,s=1.0,attenuation=np.exp(-2.0)):
        assert attenuation > 0.0 and attenuation < 1.0
        return np.sqrt(attenuation**(-1.0/(self.m+1.0)) - 1.0)

    def __str__(self):
        return "Wavelet<"+self.name()+" with m="+str(self.m)+">"   
    
class DerivativeOfGaussiansWavelet(Wavelet):
    def __init__(self,parameters):
        #check input parameters
        if 'm' not in parameters:
            print 'Error: DOG Wavelet should be created with the parameter "m".'
            raise Exception('Runtime error!')
        else:
            m = int(parameters['m'])
            if m < 0:
                print 'Error: DOG Wavelet should be created with a positive parameter "m".'
                raise Exception('Runtime error!')
        
        Cm =     (-1.0)**(m+1) / np.sqrt(sp.gamma(m+0.5))
        Cm_hat = (1.0j)**m     / np.sqrt(sp.gamma(m+0.5))
        
        Cm2     = 1.0 / sp.gamma(m+0.5)
        Cm_hat2 = Cm2
        
        Pm    = self._compute_poly(m)
        Pm2   = Pm*Pm
        
        Pm_hat  = poly.Polynomial(np.eye(1,m+1,m,dtype=float)[0])
        Pm_hat2 = Pm_hat*Pm_hat

        mu         = Cm2     * self._moments(Q=Pm2,a=1.0,k=1,mode='inf/inf',x0=0.0)
        sigma2     = Cm2     * self._moments(Q=Pm2,a=1.0,k=2,mode='inf/inf',x0=mu)
        mu_hat     = Cm_hat2 * self._moments(Q=Pm_hat2,a=1.0,k=1,mode='0/inf',x0=0.0)
        sigma_hat2 = Cm_hat2 * self._moments(Q=Pm_hat2,a=1.0,k=2,mode='0/inf',x0=mu_hat)
        print "m=",m," mu_hat=",mu_hat

        self.m               = m
        self.plot_parameters = {'m':m}
        self.Cm              = Cm
        self.Cm_hat          = Cm_hat
        self.Cm2             = Cm2
        self.Cm_hat2         = Cm_hat2
        self.Pm              = Pm
        self.Pm2             = Pm2
        self.Pm_hat          = Pm_hat
        self.Pm_hat2         = Pm_hat2
        self.mu              = mu
        self.sigma2          = sigma2
        self.mu_hat          = mu_hat
        self.sigma_hat2      = sigma_hat2

    
    def name(self):
        return 'Derivative Of Gaussian (DOG)'
    def family(self):
        return WaveletFamily('dog')
    def type(self):
        return 'real'
    
    def psi_0(self,t):
        return self.Cm * self.Pm(t) * np.exp(-1.0/2.0*(t**2.0))
    def psi_hat_0(self,omega):
        return self.Cm_hat * np.array(omega>0.0,dtype=float) * self.Pm_hat(omega) * np.exp(-1.0/2.0*(omega**2.0))

    # localization and dispersion to plot 
    def localization(self,s=1.0):
        return self.mu    / (s)
    def dispersion(self,s=1.0):
        return self.sigma2 / (s**2.0)
    def localization_hat(self,s=1.0):
        return self.mu_hat    * (s)
    def dispersion_hat(self,s=1.0):
        return self.sigma_hat2 * (s**2.0)

    #chatacteristic times
    def fourier_wavelength(self,s=1.0):
        return 2.0*np.pi / self.localization_hat(s)
    def efolding(self,s=1.0,attenuation=np.exp(-2.0)):
        return self.localization() + 4.0*self.dispersion()

    def __str__(self):
        return "Wavelet<"+self.name()+" with m="+str(self.m)+">"
    
    
    #return Pm(x) such that d^m [ exp(-ax^2) ] /dx^m = Pm(x) exp(-ax^2) with a>0
    def _compute_poly(self,k,a=1.0/2.0):
        if k==0:
            return poly.Polynomial(np.array([1], dtype=float))
        else:
            P = self._compute_poly(k-1)
            return P.deriv() - 2.0*a*poly.Polynomial(np.array([0,1], dtype=float)) * P


    #compute integral from 
    #    -infty to +infty if mode=='infty/infty'
    # OR      0 to +infty if mode=='0/infty'
    # of
    #   (x-x0)^k Q(x) exp(-ax^2) dx where Q(x) is a polynomial in x and a>0
    # by default x0=0.0 and Q(x)=1.0
    def _moments(self,a,k,mode,x0=0,Q=poly.Polynomial(np.array([1],dtype=float))):
        P = poly.Polynomial([ sp.binom(k,i)*(-x0)**(k-i) for i in xrange(k+1) ])
        return self._intPoly(P*Q,a,mode)

    #compute integral from [-infty|0] to +infty of Q(x) exp(-ax^2) dx where Q(x) is a polynomial in x and a>0
    def _intPoly(self,Q,a,mode):
        I = 0.0
        for k in xrange(Q.coef.size):
            I += Q.coef[k] * self._intMonomial(k,a,mode)
        return I

    #compute integral from [-infty,0] to +infty of x^k exp(-ax^2) dx where k>=0 and a>0
    #the formulas are obtained by IPP on odd or even monomials
    def _intMonomial(self,k,a,mode):
        a=float(a) 

        if k%2==0: 
            p = k/2
            if mode == 'inf/inf':
                I = sp.gamma(p+0.5) / (a**p) * 1.0/(1.0*np.sqrt(a))
            elif mode == '0/inf':
                I = sp.gamma(p+0.5) / (a**p) * 1.0/(2.0*np.sqrt(a))
            else:
                raise Exception('Unknown mode!')
        else:
            p = (k-1)/2
            if mode == 'inf/inf':
                I = 0.0
            elif mode == '0/inf':
                I = sp.gamma(p+1.0) / (a**p) * 1.0/(2.0*a)
            else:
                raise Exception('Unknown mode!')

        return I
