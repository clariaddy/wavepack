import numpy as np
import scipy as sp

import scipy.signal

#INPUT:
#   CWT: Matrix of shape (P,N) 
#       -P is the number of scales
#       -N is the number of times
#   window (H,W): size of the smoothing window on each axes (should be odd)
#       -H: window height = number of scales smoothed
#       -W: window width  = numver of times smoothed
#   filter: (optional) default = np.mean
#       - array function applied to the data inside the window
#OUTPUT:
#   SCWT: Smoothed matrix
#
def smooth(CWT, window, kernel=np.mean):

    #check inputs
    assert (CWT.ndim == 2), 'Provided CWT is not a matrix!'
    P = CWT.shape[0] #number of scales
    N = CWT.shape[1] #number of times
    
    #window size should be odd
    H = window[0]
    W = window[1]
    assert H%2==1 and W%2==1, 'Smoothing winsow size is not odd!'
    assert H <= P, 'Smoothing window height >= Scales count !' 
    assert W <= N, 'Smoothing window width  >= Times  count !' 
    
    SCWT = np.zeros(CWT.shape, dtype=CWT.dtype)
    for i in xrange(P):
        imin = max(0, i-H//2)
        imax = min(P-1, i+H//2)
        for j in xrange(N):
            jmin = max(0, j-W//2)
            jmax = min(N-1, j+W//2)
            
            SCWT[i,j] = kernel(CWT[imin:imax+1,jmin:jmax+1])

    return SCWT

def fft_smooth(CWT, window):
    assert (CWT.ndim == 2), 'Provided CWT is not a matrix!'
    
    kernel = np.ones(shape=window, dtype=CWT.dtype)
    kernel /= kernel.size

    #return sp.signal.convolve2d(CWT, kernel, mode='same', boundary='symm')
    return sp.signal.fftconvolve(CWT, kernel, mode='same')

def fft_smooth_many(CWTs, window, CWT_axis=0, S_axis=1, T_axis=2):
    assert (CWTs.ndim == 3), 'Provided CWT is not list of matrix!'
    
    kernel = np.ones(shape=window, dtype=CWTs.dtype)
    kernel /= kernel.size

    P = CWTs.shape[CWT_axis] #number of CWTs
    view = np.transpose(CWTs, axes=(CWT_axis,S_axis,T_axis))
    for i in xrange(P):
        view[i,:,:] = sp.signal.fftconvolve(view[i,:,:], kernel, mode='same')

    return CWTs

#test function
if __name__ == '__main__':
    
    #create matrix
    shape = (5,5)
    CWT = np.round(np.random.rand(*shape),2)
    print 'CWT:\n',CWT,'\n'
    
    #mean smoothing over scales
    ws = (3,1)
    S = smooth(CWT,ws)
    print 'Mean over scales:\n',np.round(S,2),'\n'
    
    #mean smoothing over times
    ws = (1,3)
    S = smooth(CWT,ws)
    print 'Mean over times:\n',np.round(S,2),'\n'
    
    #mean smoothing over scales and times
    ws = (3,3)
    S = smooth(CWT,ws)
    print 'Mean over scales and times:\n',np.round(S,2),'\n'


    #max smoothing over time and scales
    ws = (3,3)
    S = smooth(CWT,ws,np.max)
    print 'MAX over scales and times:\n',np.round(S,2),'\n'

    
    ## FFT smoothing
    A = np.asarray(range(1,4*3*2+1),dtype=float).reshape(2,3,4)
    ws = (3,3) 
    print A

    print smooth(A[0],ws)
    print fft_smooth(A[0], ws)

    print smooth(A[1],ws)
    print fft_smooth_many(A, ws)[1]


