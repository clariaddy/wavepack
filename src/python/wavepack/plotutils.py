import os
import numpy as np

import matplotlib
import matplotlib.path as mpath
import matplotlib.pyplot as plt

def plot_script(plot_folder, plot_script_name, _globals, _locals):
    path = plot_folder + '/' + plot_script_name + '.py'
    with open(path, 'r') as f:
         script = f.read()
    f.closed
    exec script in _globals,_locals 

def plot(fig,save_dir,file_name,ext,show_plots,save_plots,add_order_id=True):
    if not hasattr(plot, 'sid'):
        setattr(plot, 'sid', 0)
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)

    if save_plots:
        if add_order_id:
            file_name = str(plot.sid)+'_'+file_name
            plot.sid+=1
        path = save_dir + '/' + file_name + '.' + ext
        
        fig.savefig(path) 
    if show_plots:
        fig.show()
    else:
        plt.close(fig)

def plotWaveletPower(t,period, WP,
       tmin, tmax, smin, smax, pmin, pmax,
        coi=None,
        subplotid=111,
        xlabel='Time (kyrs)',
        ylabel='periods (yrs)',
        title='Power Spectrum',
        cmap='coolwarm',
        contour = True,
        plt_fun = np.log2,
        level_var=None,
        levels=None):
        
        plt1 = plt.subplot(subplotid)
        plt1.set_yscale('log', basey=2, subsy=None)

        #plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.title(title)

        plt.xlim(tmin,tmax)
        plt.ylim(pmin,pmax)

        #power spectrum
        if contour:
            plt.contourf(t, period, plt_fun(WP), 10) 
        else:
            plt.imshow(plt_fun(WP), extent=[tmin,tmax,pmax,pmin], aspect='auto', interpolation='bicubic',cmap=cmap)

        #cone of influence
        if coi is not None:
            coi_path  = zip(t,coi) + [(tmax,smin),(tmax,smax+1.0),(tmin,smax+1.0),(tmin,smin),(t[0],coi[0])]
            coi_path  = mpath.Path(coi_path, [mpath.Path.MOVETO]+[mpath.Path.LINETO]*(len(t)+3)+[mpath.Path.CLOSEPOLY])
            coi_patch = matplotlib.patches.PathPatch(coi_path, facecolor='k',alpha=0.40)

            plt1.add_patch(coi_patch)
            plt.plot(t,coi,'b--',linewidth=2)
        
        #significant level
        if level_var is not None:
            assert levels is not None, 'Parameter error!'
            plt.contour(t, period, level_var, line_width=4, levels=levels, color='k')
            #print 'level_var => min={} max={} level={}'.format(np.min(level_var), np.max(level_var), levels)


        #set y-axis to kyears
        ay = plt.gca().yaxis
        formater = matplotlib.ticker.FuncFormatter(lambda x,pos: '%1.0f' % (x*1e3))
        ay.set_major_formatter(formater)
        plt1.invert_yaxis()

        return plt1
