
import numpy as np
from scipy import interpolate

import data as data_loader

class DataSeries: 
    # data should be a dictionnary with time serie names as keys and numpy arrays as values
    def __init__(self, data=None, dtype=float):
        self.dtype = dtype
        self.dic  = {} if data == None else data
    
    def data(self):
        return self.dic

    def loadFromTxt(self, file_dir, file_name, column_id_names,
            comments='#', delimiter=None, converters=None, skiprows=0, usecols=None, unpack=False, ndmin=0):
        file_path = file_dir + '/' + file_name
        data = np.loadtxt(fname=file_path, dtype=self.dtype, 
                comments=comments, delimiter=delimiter, skiprows=skiprows,
                usecols=usecols, unpack=unpack, ndmin=ndmin)
        for k in column_id_names:
            self.dic[k] = data[:,column_id_names[k]]

    def loadFromSheet(self, file_dir, file_name, sheet_name, columns_types):
        d = data_loader.load_excel_data(file_dir, file_name, sheet_name, columns_types)
        for k in d:
            self.dic[k] = np.array(d[k], dtype=self.dtype)

    def __getitem__(self, name):
        return self.dic[name]
    def __setitem__(self, name, data):
        self.dic[name] = data

class TimeSeries():

    def __init__(self, tmin, tmax, N, dtype=float):
        if (tmin > tmax):
            sys.exit("No common age interval for the two records")
        elif (N <= 0):
            sys.exit("N should be a positive integer!")
        
        self.tmin = tmin
        self.tmax = tmax
        self.N = N
        self.dtype = dtype
    
        self.L  = self.tmax - self.tmin
        self.C  = (self.tmax + self.tmin)/2.0
        self.dt = self.L / (self.N-1)
        self.dic={ 't': np.linspace(self.tmin,self.tmax,self.N) }

    @staticmethod
    def fromT(t):
        tmin = t[0]
        tmax = t[-1]
        N = t.size
        return TimeSeries(tmin,tmax,N,dtype=t.dtype)

    def interpolate(self,name,time,values):
        t = time.tolist()
        S = values.tolist()
        n = len(S)
        
        assert len(t) == len(S), "Length mismatch!"
        assert t[0]  <= self.tmin, "t[ 0]="+str(t[ 0])+"should be <= tmin="+str(self.tmin)+"!"
        assert t[-1] >= self.tmax, "t[-1]="+str(t[-1])+"should be >= tmax="+str(self.tmax)+"!"
        
        #remove extra data
        while t[1] <= self.tmin:
            del t[0]
            del S[0]
            n -= 1
        while t[-2] >= self.tmax:
            del t[-1]
            del S[-1]
            n -= 1
        assert(len(t)==n)

        #case of index problems
        i=0
        while i<len(t):
            same_t_ids = [ j for j in xrange(i+1,n) if t[i]==t[j] ]
            for j in reversed(same_t_ids):
                print "\twarning: data number",j,"in interpolation of '",name,"' was already sampled at t=",t[i],"data number",i," and is ignored."
                del t[j]
                del S[j]
                n -= 1
            i+= 1
        assert(len(t)==n)
        
        t = np.array(t,dtype=self.dtype)
        S = np.array(S,dtype=self.dtype)
        self.dic[name] = interpolate.pchip_interpolate(t, S, self.dic['t'], der=0, axis=0)

    def nextPow2(self):
        N = self.N
        n=1
        p=0
        while(n<N):
            n*=2
            p+=1
        return p,n

    def extend(self,ext):
        p,n = self.nextPow2()
        assert p>=1
        
        dt = self.dt
        tmin = self.tmin - (n/2)*dt
        tmax = self.tmax + (n/2 + n-self.N)*dt
        N = 2*n
        padded_tm = TimeSeries(tmin,tmax,N)

        if ext=='zeros':
            for k in self.dic:
                if k=='t':
                    continue
                old = self.dic[k].copy()
                padded_tm.dic[k] = np.concatenate( (
                    np.zeros(n/2,dtype=self.dtype),
                    old,
                    np.zeros(n-self.N + n/2,dtype=self.dtype)
                    ) )
        elif ext=='repeat':
            for k in self.dic:
                if k=='t':
                    continue
                old = self.dic[k].copy()
                padded_tm.dic[k] = np.concatenate( (
                    np.asarray([old[0]]*(n/2)),
                    old,
                    np.asarray([old[-1]]*(n/2))
                    ) )
        else:
            assert False, "Not implemented yet!"

        return padded_tm
            

    def __getitem__(self, name):
        return self.dic[name]
    def __setitem__(self, name, data):
        self.dic[name] = data

    def __str__(self):
        s = "TimeSerie:"
        s += "\n\tN    = " + str(self.N)
        s += "\n\tTmin = " + str(self.tmin)
        s += "\n\tTmax = " + str(self.tmax)
        s += "\n\tdt   = " + str(self.dt)
        s += "\n\tL    = " + str(self.L)
        s += "\n\tkeys = " + str(self.dic.keys())
        return s


            

