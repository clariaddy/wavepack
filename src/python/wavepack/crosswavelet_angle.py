
import numpy as np
import matplotlib.pyplot as plt

#input: 
#   A: Angle of crosswavelet transform of two signals in radians
#     *Matrix of shape (P,N) 
#       -P is the number of scales
#       -N is the number of times
#   deg: (optional) default=True
#        Outputs MCA and MCSD in degree if True, else in radian
#output: 
#   MCA:  Mean circular angle for each time
#      *Vector of size (N)
#   MCSD: Mean circular standard deviation for each time
#      *Vector of size (N)
def mean_circular_angle(A, deg=True, weights=None):
    
    #check inputs
    assert (A.ndim == 2), 'Provided A is not a matrix!'
    
    #precompute sizes
    P = A.shape[0] #number of scales
    N = A.shape[1] #number of times

    if weights == None:
        weights = np.ones(A.shape, dtype=A.dtype)

    # compute the circular mean angle over all scales
    X = np.sum(weights*np.cos(A), axis=0)
    Y = np.sum(weights*np.sin(A), axis=0)
    MCA = np.arctan2(Y,X)
    
#The power spectral value for any frequency intensity
    R = np.sqrt(X*X + Y*Y) / np.sum(weights, axis=0)

    # compute mean circular standard deviation 
    MCSD = np.sqrt(-2.0*np.minimum(np.log(R),0.0))
    
    # Special case R/P == 1.0 + epsilon => log can be computed positive ~= +1e-16
    #eps = np.finfo(float).eps
    #arr = np.asarray([1.0+k*eps for k in xrange(-3,4)])
    #print np.log(arr)
    #print np.minimum(0.0,np.log(arr))
    # np.minimum is used to bring those small positive values to 0.0
    # convert to degrees if necessary
    if deg:
        MCA  *= 180.0 / np.pi
        MCSD *= 180.0 / np.pi

    # check outputs
    assert (MCA.size == N) and (MCSD.size == N), 'Error: Output values length mismatch!'

    return MCA,MCSD


#test function
if __name__ == '__main__':

    # generate complex matrix
    P = 10
    N = 19
    shape = (P,N)
    
    t = np.linspace(-np.pi,np.pi,N)
    Re = np.ones(shape) * np.cos(t)[None,:]
    Im = np.ones(shape) * np.sin(t)[None,:]
    CWT = 1.0*Re + 1.0j*Im

    # compute cross wavelet angle and standard deviation
    MCA, MCSD = mean_circular_angle(CWT, deg=True)

    # plot MCA +/- standard deviation
    plt.figure()

    plt.plot(t,MCA)
    #plt.plot(t,MCA+MCSD)
    #plt.plot(t,MCA-MCSD)

    plt.xlim([min(t),max(t)])
    plt.ylim([-180.0,+180.0])
    
    plot_deg = [-180.0 + k*30.0 for k in xrange(13)]
    plt.yticks(plot_deg, [str(x) for x in plot_deg], color='red')

    plt.show()

