import numpy as np
import scipy as sp
from scipy.stats import chi2
import wavepack as wp


morlet = wp.WaveletFamily('cmor')
paul = wp.WaveletFamily('paul')
wave = paul.wavelet({'m':10.0})

flambda  = wave.fourier_wavelength()

           
dofmin = 2          # Minimum degrees of freedom
f0 = 6.          # Wave number
cdelta = 0.776  # Reconstruction factor
gamma = 2.32    # Decorrelation factor for time averaging
deltaj0 = 0.60  # Factor for scale averaging

def ar1(x):
    """
    PARAMETERS
        x (array like) :
            Univariate time series

    RETURNS
        g (float) :
            Estimate of the lag-one autocorrelation.
        a (float) :
            Estimate of the noise variance [var(x) ~= a**2/(1-g**2)]
        mu2 (foat) :
            Estimated square on the mean of a finite segment of AR(1)
            noise, mormalized by the process variance.
    """
    x = np.asarray(x)
    N = x.size
    xm = x.mean()
    x = x - xm

    # Estimates the lag zero and one covariance
    c0 = x.transpose().dot(x) / N
    c1 = x[0:N-1].transpose().dot(x[1:N]) / (N - 1)

    # According to A. Grinsteds' substitutions
    B = -c1 * N - c0 * N**2 - 2 * c0 + 2 * c1 - c1 * N**2 + c0 * N
    A = c0 * N**2
    C = N * (c0 + c1 * N - c1)
    D = B**2 - 4 * A * C

    if D > 0:
        g = (-B - D**0.5) / (2 * A)
    else:
        raise Warning ('Cannot place an upperbound on the unbiased AR(1). '
            'Series is too short or trend is to large.')

    # According to Allen & Smith (1996), footnote 4
    mu2 = -1 / N + (2 / N**2) * ((N - g**N) / (1 - g) -
        g * (1 - g**(N - 1)) / (1 - g)**2)
    c0t = c0 / (1 - mu2)
    a = ((1 - g**2) * c0t) ** 0.5
    

    return g, a, mu2


def ar1_spectrum(freqs, ar1=0.) :
    """Lag-1 autoregressive theoretical power spectrum

    PARAMETERS
        ar1 (float) :
            Lag-1 autoregressive correlation coefficient.
        freqs (array like) :
            Frequencies at which to calculate the theoretical power
            spectrum.

    RETURNS
        Pk (array like)  AR(1) power spectrum  as it has been given in article Torrence and Compo:
            Theoretical discrete Fourier power spectrum of noise signal.

    """
    # According to a post from the MadSci Network available at
    #
    # which for an AR1 model reduces to
    #
    freqs = np.asarray(freqs)
    #Pk = (1 - ar1 ** 2) / np.abs(1 - ar1 * np.exp(-2 * np.pi * 1j * freqs)) ** 2

    # Theoretical discrete Fourier power spectrum of the noise signal following
    # Gilman et al. (1963) and Torrence and Compo (1998), equation 16.
    N = len(freqs)
    Pk = (1 - ar1 ** 2) / (1 + ar1 ** 2 - 2 * ar1 * np.cos(2 * np.pi * freqs / N))

    return Pk