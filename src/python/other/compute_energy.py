import sys
import numpy as np
import openpyxl as pyxl
import matplotlib.pyplot as plt

# Load data from worksheet 'worksheet_name' from file 'folder'/'filename'.
# 'columns_types' is a dictionary containing all requested column names as keys
# and column types as values. Columns names are matched against the first non empty line columns values.
def load_data(folder,filename,worksheet_name,columns_types):
    filepath = folder + '/' + filename
    workbook = pyxl.load_workbook(filepath)
    worksheet = workbook[worksheet_name]
    if(worksheet == None):
        print "Could not find '" + worksheet_name + "' in file '" + filepath + "' !"
        sys.exit(1)
    
    #extract values
    extracting_values = False
    column_ids = {}
    data = {}
    for current_row in worksheet.iter_rows():
        if extracting_values:
            for column_name in columns_types:
                column_id = column_ids[column_name]
                current_value = columns_types[column_name](current_row[column_id].value)
                data[column_name].append(current_value)
        else:
            if current_row[0].value == None: #skip empty lines
                continue
            else: #first non empty line, extract column positions according to input column names
                k=0
                for col in current_row:
                    cell = col.value
                    if cell in columns_types:
                        column_ids[cell] = k
                        data[cell] = []
                    k = k+1
                if len(column_ids) != len(columns_types):
                    print "Could not find all requested columns !"
                    print "Asked for '" + str(columns_types) + "' but only found " + str(column_ids) + " !"
                    sys.exit(1)
                extracting_values = True
    return data

#Compute ISI
def compute_isi(beta, omega, omega_threshold):

    assert len(beta)==len(omega), "Beta and Omega are not of same size !"
    S=0.0
    omega_threshold=380
    for i in xrange(len(beta)):
        wi = omega[i]
        if(wi>=omega_threshold):
            S = (wi*86,400)
    return S
#-------------------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    data = load_data('../data', 'insolation.xlsx', 'Feuille1', {'time':int,'insolation':float})
    
    time = np.array(data['time'])
    insolation = np.array(data['insolation'])

    min_insolation  = np.min(insolation)
    max_insolation  = np.max(insolation)
    mean_insolation = np.mean(insolation)
    std_insolation  = np.std(insolation)

    #standardized_insolation = (insolation - mean_insolation)/std_insolation

    print "Insolation values:"
    print "\tinsolation:",S
    print "\tmin: ", min_insolation
    print "\tmax: ", max_insolation
    print "\tmean: ", mean_insolation
    print "\tstandard deviation: ", std_insolation

    #plt.plot(time, standardized_insolation)
    plt.plot(time,insolation)
    plt.xlabel('time')
    plt.ylabel('insolation')
    plt.show()
