
import sys
import math
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import scipy as sp
from scipy import signal
import pylab as pl
import pywt
# git library

from wavelets import WaveletAnalysis
from wavelets import Ricker
#my modules 
import env
import data
import waveletfun
import wavebasis

# user variables
data_folder = env.data_folder
file_names = [ 'CO2.txt', 'temperature.txt' ]
use_custom_timestep = False
custom_timestep = 1.0

# other constants
pi = math.pi
# load and prepare data
dt,time,data = data.prepare_data(data_folder, file_names, use_custom_timestep, custom_timestep)
data =[data[0],data[1]]
n = data[0].size
assert data[0].size == data[1].size, "Size mismatch!"

#some important measurement
data_min  = [np.min(data[0])  , np.min(data[1])]
data_max  = [np.max(data[0])  , np.max(data[1])]
data_mean = [np.mean(data[0]) , np.mean(data[1])]
data_std  = [np.std(data[0])  , np.std(data[1])]

base2 = 0
N = 1
while N < n:
	base2 = base2+1
	N = 2 ** base2
print time.size
x = [ data[0] - data_mean[0], data[1] - data_mean[1] ]
x = [np.concatenate( (x[0] , np.zeros(N-n,dtype=float))), np.concatenate( (x[1], np.zeros(N-n,dtype=float)) )]
# we compute FFT of the real padded time series of x
print "Computing fft...",
X_tilde = [ np.fft.fft(x[0]), np.fft.fft(x[1]) ] 
#TODO check factor 2
#k = np.fft.fftfreq(n, d=dt/(2.0*pi))
p = N/2
kplus  = np.array( [ i      for i in xrange(0,p+1) ], dtype=float)
kminus = np.array( [ -(p-i) for i in xrange(1,p)   ], dtype=float)
k = np.concatenate(( kplus, kminus))
k *= (2.0*pi)/(N*dt) 	    
print k.size 
aa, period, per =waveletfun.abc_new(dt, 1,10, 80)

#build wavelets

J=len(aa)
scale= aa
print scale.size
#wave : Output is transformed in wavelet of Yy
k0=5.4
#through all scales computes transform and Inverse FFT of the product (FT(daughter).FT(Y)) give the convolution product  in  time frequences
param=-1
mother = 'DOG'

convolution1=np.zeros(shape=(J,N), dtype=complex)
convolution2=np.zeros(shape=(J,N), dtype=complex) # define the wavelet array
# wavelet transform
for j in xrange(J):

	daughter , fourier_factor, coi , dofmin = wavaletfun.wave_bases(mother, k, scale[j], param)
	convolution1[j,:]=np.fft.ifft(X[0]*daughter)
	convolution2[j,:]=np.fft.ifft(X[1]*daughter)
    #period
	period = fourier_factor * scale
	#Coi
	#coi=coi*dt*np.concatenate((np.insert(np.arange((n+1)/2.-1),[0],[1E-5]),np.insert(np.flipud(np.arange(0,n/2.-1)),[-1],[1E-5])))
	X_tilde=[convolution1,convolution2

#dim=power2[None,:]
#fig = plt.subplots()
#T, S =np.meshgrid(time,dim)
#plt.contourf(T,S,power1, 10)
	#ax.set_yscale('log2')
#fig.savefig('temp_wavelet_power_spectrum.pdf')
