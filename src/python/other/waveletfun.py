import sys
import math
from scipy.special._ufuncs import gammainc, gamma
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.axes_grid import make_axes_locatable
import matplotlib.axes as maxes
import numpy as np
import scipy as sp
from scipy import optimize
import pylab as pl
#my modules 
import data


#--------------------------------------------------------------------------------------------
# travail --------code 14/03/2016-------------------------------------------
#---------------------------------------------------------------------------------------------------------------

# dt = sampling step for the y record to be transformed
# exp1,exp2 = scale range from 2^exp1 to 2^exp2
# inter = scale resolution 2^exp1/inter,..., 2^(exp1+inter-1)/inter
# returns: aa,period,per
#*******************************************
# example for exp1=1, exp2=10, inter=80
# scales will range FROM 2^1 /80,..., 2^(1 +80-1)/80 = 2^ 1/80,...,2^1
#                     TO 2^10/80,..., 2^(10+(80-1)/80) = 2^10/80,...,2^10
def abc_new(dt, exp1, exp2, inter):
    dt = float(dt)

    aa = np.array([ 2**(exp+i/float(inter)) for i in xrange(0,inter+1) for exp in xrange(exp1,exp2) ], 
          dtype=float)

    k0=5.4;
    omega0 = 1.0/2.0 * (k0 + np.sqrt(2.0+k0*k0)) * (1/aa)

    period = (1.0/omega0) * (2*math.pi)
    per = period*dt;

    return aa, period, per
#wavelet basis

def wave_bases(mother, k, scale, param):
	nk = len(k)
	kplus = np.array(k > 0., dtype=float)

	if mother == 'MORLET':  #-----------------------------------  Morlet

		if param == -1:
			param = 6.

		k0 = np.copy(param)
		expnt = -(scale * k - k0) ** 2 / 2. * kplus
		norm = np.sqrt(scale * k[1]) * (np.pi ** (-0.25)) * np.sqrt(nk)  # total energy=N   [Eqn(7)]
		daughter = norm * np.exp(expnt)
		daughter = daughter * kplus  # Heaviside step function
		fourier_factor = (4 * np.pi) / (k0 + np.sqrt(2 + k0 ** 2))  # Scale-->Fourier [Sec.3h]
		coi = fourier_factor / np.sqrt(2)  # Cone-of-influence [Sec.3g]
		dofmin = 2  # Degrees of freedom
	elif mother == 'PAUL':  #--------------------------------  Paul
		if param == -1:
			param = 4.
		m = param
		expnt = -scale * k * kplus
		norm = np.sqrt(scale * k[1]) * (2 ** m / np.sqrt(m*np.prod(np.arange(1, (2 * m))))) * np.sqrt(nk)
		daughter = norm * ((scale * k) ** m) * np.exp(expnt) * kplus
		fourier_factor = 4 * np.pi / (2 * m + 1)
		coi = fourier_factor * np.sqrt(2)
		dofmin = 2
	elif mother == 'DOG':  #--------------------------------  DOG
		if param == -1:
			param = 2.
		m = param
		expnt = -(scale * k) ** 2 / 2.0
		norm = np.sqrt(scale * k[1] / gamma(m + 0.5)) * np.sqrt(nk)
		daughter = -norm * (1j ** m) * ((scale * k) ** m) * np.exp(expnt)
		fourier_factor = 2 * np.pi * np.sqrt(2. / (2 * m + 1))
		coi = fourier_factor / np.sqrt(2)
		dofmin = 1
	else:
		print 'Mother must be one of MORLET, PAUL, DOG'

	return daughter , fourier_factor, coi,dofmin

if __name__ == '__main__':
    pi = math.pi

    # load data into numpy array
    print "Loading data from file...",
    data       = data.load_excel_data('../../data', 'insolation.xlsx', 'Feuille1', {'age': int,'measure':float})
    age       = np.array(data['age'],dtype=float)
    measure = np.array(data['measure'],dtype=float)
    print " done"
    
    # check if loaded data sizes match and if there is at least 1 data
    assert(age.size==measure.size and age.size>=1)
    n = measure.size
    
    # compute mean dt
    time_dt = age[1:n] - age[0:n-1]
    dt = np.mean(time_dt)

    #compute some constants
    measure_min  = np.min(measure)
    measure_max  = np.max(measure)
    measure_std  = np.std(measure)
    measure_mean = np.mean(measure)
       
    # Pad insolation with zeroes if 'n' is not a power of two (needed for the fft),
    # so we look for 'base2' and 'N' such that N=2**base2 >= n.
    base2 = 0

    N = 1
    while N < n:
        base2 = base2+1
        N = 2 ** base2
   
    x = measure - measure_mean                      # substract mean insolation to get the time serie
    x = np.concatenate( (x, np.zeros(N-n,dtype=float)) )  # complete with N-n zeroes
    assert(x.size == N)
    print "\tGot n=",n,"input data, completed with",N-n,"zeros such that new size N=",N,"is a power of two."
        
    # Build fft wavenumber array
    # (i.e) a symmetric phase with respect to 0 of type [0,1,...,p,-(p-1),-(p-2),...,-1]*(2*pi)/(N*dt) with p=E(N/2) = N/2
    p = N/2
    kplus  = np.array( [ i      for i in xrange(0,p+1) ], dtype=float)
    kminus = np.array( [ -(p-i) for i in xrange(1,p)   ], dtype=float)
    k = np.concatenate(( kplus, kminus))
    k *= (2.0*pi)/(N*dt)       
    # we compute FFT of the real padded time series of 'x' and name it 'X'
    print "Computing fft...",
    X = np.fft.fft(x) 
    assert(X.size == N)
    print " done"
    

    #--------------------------------------------------------------------------------------------
    # travail --------code 21/03/2016-------------------------------------------
    #---------------------------------------------------------------------------------------------------------------
    
    #input variables
    aa, period, per = abc_new(dt, 1, 10, 30)
    J=len(aa)
    scale= aa   
    # wave : Output is transformed in wavelet of Y
    X_tilde=np.zeros(shape=(J,N), dtype=complex) # define the wavelet array
    kn=len(k)
    k0=5.4
    # through all scales computes transform
    param=-1
    mother='MORLET'
    lag1 = 0.72  # lag-1 autocorrelation for red noise background
    Y= x
    time = np.arange(Y.size) * dt + min(age)  
    xlim = ([np.min(time), np.max(time)])
    for j in xrange(J):
    	daughter , fourier_factor, coi , dofmin = wave_bases(mother, k, scale[j], param)
    	X_tilde[j,:] = np.fft.ifft(X * daughter)  # wavelet transform
        
        print X_tilde.shape
#Power = (np.abs(X_tilde))**2  # compute wavelet power spectrum
#sig95 = power / sig95  # where ratio > 1, power is significant
print time.shape , Power.shape, period.shape

#dof = n - scale  # the -scale corrects for padding at edges
#global_signif = wave_signif(variance, dt=dt, scale=scale, sigtest=1, lag1=lag1, dof=dof, mother=mother)

plt.subplot(211)
plt.plot(age,measure)
plt.xlim(xlim[:])
plt.xlabel('Time (year)')
plt.ylabel('Air content volume')
plt.title('air content  (seasonal)')
plt.hold(True)


plt3 = plt.subplot(212)
levels = [0.0625, 0.125, 0.25, 0.5, 1, 2, 4, 8, 16]
CS = plt.contourf(time, period, np.log2(Power),len(levels))  #*** or use 'contour'
im = plt.contourf(CS, levels=np.log2(levels))
plt.xlabel('Time (kyear)')
plt.ylabel('Period (kyears)')
plt.title('Wavelet Power Spectrum (in base 2 logarithm)')
plt.xlim(xlim[:])
plt3.set_yscale('log', basey=2, subsy=None)
plt.ylim([np.min(period), np.max(period)])
ax = plt.gca().yaxis
ax.set_major_formatter(matplotlib.ticker.ScalarFormatter())
plt3.ticklabel_format(axis='y', style='plain')
plt3.invert_yaxis()
# set up the size and location of the colorbar
divider = make_axes_locatable(plt3)
cax = divider.append_axes("bottom", size="5%", pad=0.5)
plt.colorbar(im, cax=cax, orientation='horizontal')
plt.tight_layout()

plt.show()