# -*- coding: utf-8 -*-

import sys, os, trace

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
import scipy.signal as signal
from scipy import fftpack
from numpy.linalg import norm
#local includes
import env
import wavepack as wp

### input variables and configuration ###
# 
# source data files and names
data_dir        = env.data_dir



plot_script_dir = env.plot_script_dir

data_src  = [ ('CO2.txt','CO2'        ,0,1),    # format = (file_name, arbitrary_data_name, time_column_id, data_column_id)
              ('ATS.txt','Temperature',0,2) ]   # warning: column ids begin from 0

# interpolation parameters
remove_tendency = True
filtered_process= True
tendency_order  = 1
P               = 10
N               = 2**P     #resampled points count
timescaling     = 1.0/1000.0 #to kyrs

# CWT parameters

wave_family = wp.WaveletFamily('cmor')
wave        = wave_family.wavelet({'omega0':6.0}) 
psi0 =wave.psi_0(0)
Morlet =wave
# wave_family = wp.WaveletFamily('paul')
# wave        = wave_family.wavelet({'m':10.0})

dj     = 0.01    # 1/dj suboctave per octave
spo    = int(np.floor(1.0/dj))
jmin   = None    # if None, default jmin value is computed
jmax   = None    # if None, default jmax value is computed


# XWT parameters
beta=0.9
smoothing_kernel = np.mean
ws=int(np.ceil(beta/dj))
wt=1
smoothing_window = (ws+(ws%2==0),wt+(wt%2==0))

# confidense levels
p_level = 0.80   # confidense level probability
monte_carlo_batch_size  = 16
monte_carlo_batch_count = 32

# plot parameters
show_plots = False
save_plots = True
save_dir   = os.getcwd() + '/out' 
plot_ext = 'png'

time_label  = r'time $(kyrs)$'
data0_label = r'CO2 $(ppmv)$'
data1_label = r'Temperature $(^{\circ}C)$'
udata0_label = r'Unity Normalized CO2'
udata1_label = r'Unity Normalized Temperature'

timedelay_label=r'weighted time delay'
phaseshift_label=r'weighted phaseshift'
data0_color = 'r'
data1_color = 'b'
plottitle_kargs = { 'fontsize':18, 'color':'r' }

plot_scaling = 3.0
plt.rcParams['figure.figsize'] = [plot_scaling*x for x in plt.rcParams['figure.figsize']]
#
### end of configuration ###


### print config ###
print \
"""
=== CONFIGURATION ===
    Inputs:
        data_dir -> {}
        data_src    -> {}
    Interpolation:
        remove_tendency  -> {}
        tendency_order   -> {}
        N (resampled pts)-> {}
        timescaling      -> {}
    CWT parameters:
        wave_family -> {}
        wavelet     -> {}
        dj          -> {}
        jmin        -> {}
        jmax        -> {}
    XWT parameters:
        smoothing_kernel -> {}
        smoothing_window -> {}
    Confidense levels:
        condidense level -> {}
        MT batch size    -> {}
        MT batch  count  -> {}
        MT sample count  -> {}
    Plotting:
        show_plots  -> {}
        save_plots  -> {}
        save_dir    -> {}
====================
""".format(data_dir, data_src, 
           remove_tendency, tendency_order, N, timescaling, 
           wave_family, wave, dj, jmin, jmax,
           smoothing_kernel, smoothing_window,
           p_level, monte_carlo_batch_count, monte_carlo_batch_size, monte_carlo_batch_count*monte_carlo_batch_size,
           show_plots, save_plots, save_dir)

monte_carlo_batch_size*monte_carlo_batch_count

### load input data ###
print '=== DATA PREPROCESSING ==='
print 'Loading data...',
fn0 = data_src[0][1]
fn1 = data_src[1][1]
assert fn0 != fn1, 'Data names should be differents! {} == {}'.format(fn0,fn1)

data = wp.DataSeries(dtype=float)
data.loadFromTxt(data_dir,data_src[0][0], {'t0':data_src[0][2], fn0:data_src[0][3]})
data.loadFromTxt(data_dir,data_src[1][0], {'t1':data_src[1][2], fn1:data_src[1][3]})
data0 = data[fn0]
data1 = data[fn1]
print 'done.'

# scale time and compute tmin and tmax
print 'Scaling time...',
t0 = data['t0']*timescaling
t1 = data['t1']*timescaling
tmin = max(t0[0],  t1[0])
tmax = min(t0[-1], t1[-1])
print 'done'

# interpolation
print 'Interpolating on same timescales...'
interp = wp.TimeSeries(tmin, tmax, N, dtype=float)
interp.interpolate(fn0, t0, data0)
interp.interpolate(fn1, t1, data1)
print interp
print

# normalize inputs
print 'Normalizing inputs...',
times = interp['t']
data0 = interp[fn0]
data0 = (data0 - np.mean(data0)) / np.std(data0)

data1 = interp[fn1]
data1 = (data1 - np.mean(data1)) / np.std(data1)
print 'done'

# remove tendency
if remove_tendency:
    print 'Removing tendency...',
    model0     = np.polyfit(times, data0, tendency_order)
    predicted0 = np.polyval(model0, times)
        
    model1     = np.polyfit(times, data1, tendency_order)
    predicted1 = np.polyval(model1, times)

    model = (model0 + model1)/2.0
    predicted = np.polyval(model, times)
    
    olddata0   = data0
    data0      = data0 - predicted

    olddata1   = data1
    data1      = data1 - predicted

    
    print 'done'

    print '''Tendency fitting with polynomial of order {}:
    {:12s}: fitted polynomial {}
    {:12s}: fitted polynomial {}
    => mean tendency = {}'''.format(tendency_order, fn0, model0, fn1, model1, model)

    print 'Renormalizing inputs...',
    data0 = (data0 - np.mean(data0)) / np.std(data0)
    data1 = (data1 - np.mean(data1)) / np.std(data1)


# extend signals with zeroes on the boundaries
print 'Extending signals with zeros...',
interp[fn0] = data0
interp[fn1] = data1
ext = interp.extend(ext='zeros')
print 'done'
print ext

# get back extended signals and parameters
times = interp['t']
data0 = ext[fn0]
data1 = ext[fn1]

dt    = ext.dt      # = (tmax - tmin)/(N-1)
L     = ext.L       # = tmax - tmin
C     = ext.C       # = (tmax + tmin)/2.0

# ycorr = np.fft.ifft(np.fft.fft(data0)*np.conj(np.fft.fft(data1)))/(norm(data0)*norm(data1))
# plt.plot(times,np.real(ycorr))
# plt.show()
# sys.exit(1)

print 'Computing residuals between smoothed data:'
if filtered_process:
    filtered_sigmas      = []
    filtered_datas       = []
    filtered_residuals   = []
    filtered_corelations = []
    filtered_correlation_residuals=[]
    plt.figure()
    for i in xrange(0,P+1):
        filtered_sigma  = 2**i
        filtered_data_0 = sp.ndimage.filters.gaussian_filter1d(data0, sigma=filtered_sigma)
        filtered_data_1 = sp.ndimage.filters.gaussian_filter1d(data1, sigma=filtered_sigma)
    
        filtered_residual_0 = filtered_data_0 - data0
        filtered_residual_1 = filtered_data_1 - data1

        filtered_corelation = np.corrcoef(x=filtered_data_0, y=filtered_data_1)
        filtered_corelation = filtered_corelation[0,1] #extract Cov(x,y)

        filtered_corelation_resid = np.corrcoef(x=filtered_residual_0, y=filtered_residual_1)
        filtered_corelation_resid = filtered_corelation_resid[0,1] #extract Cov(x,y)

        print '\tsigma={:4d} => corr={:.4f}'.format(filtered_sigma,filtered_corelation)
        filtered_sigmas               .append(filtered_sigma)
        filtered_datas                .append((filtered_data_0,filtered_data_1)) 
        filtered_residuals            .append((filtered_residual_0,filtered_residual_1))
        filtered_corelations          .append(filtered_corelation)
        filtered_correlation_residuals.append(filtered_corelation_resid)

        pp0 = plt.subplot((P+1)//2,2,i)
        pp0.set_title('sigma={:4d} => corr={:.4f}-->corr_res={:.4f}'.format(filtered_sigmas[i],filtered_corelations[i],filtered_correlation_residuals[i]))
        plt.plot(filtered_datas[i][0],'r',label='filtered X0')
        plt.plot(filtered_datas[i][1],'b',label='filtered X1')
        plt.subplots_adjust(hspace = 0.5)
        plt.legend()
print '=========================='
### Compute CWTs ###
print
print '=== DATA ANALYSIS ==='

flambda  = wave.fourier_wavelength()
efolding = wave.efolding(attenuation=np.exp(-2))
smin = 2.0*dt                        
smax = L /(2.0*efolding) * (1.0/2.0) 
pmin = wave.periods(smin)
pmax = wave.periods(smax)

#TODO take into account jmin and jmax from parameters
print 'Generating scales...\t',
jmax = np.log2(smax/smin)/dj
j = np.arange(jmax+1)

scales  = smin * 2.0**(j*dj)
S = scales.size

periods = wave.periods(scales)
freqs   = 1.0/periods

time_delay_first =[]
time_delay_second=[]
peak_error       =[]
print 'done'
for i in xrange(0,P+1):
    print 'Computing CWTs...\t',
    CWT0 = wp.swt(filtered_datas[i][0], wave, scales, dt=dt, normalization='Kaiser')
    CWT0 = CWT0[:,N/2:N+N/2]  

    CWT1 = wp.swt(filtered_datas[i][1], wave, scales, dt=dt, normalization='Kaiser')
    CWT1 = CWT1[:,N/2:N+N/2] 
    print 'done'

    print 'Computing Power Spectrums...\t',
    WP0 = np.abs(CWT0)**2
    WP1 = np.abs(CWT1)**2
    print 'done'

    print 'Computing Arguments...\t',
    A0 = np.angle(CWT0, deg=False)
    A1 = np.angle(CWT1, deg=False)
    print 'done'

    print 'Computing Cross Wavelet Spectrum...\t',
    XWT = CWT0 * np.conj(CWT1)
    WPX = np.abs(XWT)**2
    AX = np.angle(XWT, deg=False)
    print 'done'

    print 'Computing Smoothed Crosswavelet Coherence or mean squared coherence...\t',
    #TODO check scale^(-1) scaling
    smooth = lambda X: wp.fft_smooth(X, window=smoothing_window)
    CWT2 = (np.abs(smooth(XWT))**2) / (smooth(WP0)*smooth(WP1))
    AXS  =  np.angle(smooth(XWT), deg=False)
    A0S  =  np.angle(smooth(CWT0), deg=False)
    A1S  =  np.angle(smooth(CWT1), deg=False)
    CWT  = np.sqrt(CWT2)
    print 'done'

    # cone of influence
    print 'Computing Cone of Influence (COI)...\t',
    coi = dt*flambda/efolding * np.concatenate( ( [1e-15], np.arange((N+1)/2-1), np.flipud(np.arange(0, N/2-1)), [1e-15]) )
    coi_mask = np.ones(shape=CWT.shape, dtype=float) * scales[:,None]
    coi_mask = (coi_mask <= coi[None,:])
    print 'done'
    print 'Computing global weighted wavelet power spectrum ...\t'
    C           = CWT*coi_mask

    T           = np.sum(C*(periods)[:,None], axis=0) / np.sum(C, axis=0)

    print 'computing time delay'
    print 'first method'

    MCA, MCSD = wp.mean_circular_angle(AXS, deg=False, weights=None)
    for i in xrange(1,N):
        if (MCA[i]-MCA[i-1])>np.pi:
            MCA[i]=MCA[i]-np.pi
        elif(MCA[i]-MCA[i-1])<-np.pi:
            MCA[i]=MCA[i]+np.pi
    time_delays = (MCA /np.pi)*T
    time_delays_max = ((MCA+MCSD)/np.pi)*T
    time_delays_min = ((MCA-MCSD)/np.pi)*T
    time_delays2 = np.sum(C*AXS/np.pi*(periods)[:,None], axis=0) / np.sum(C, axis=0)
    time_delays2f = np.sum(C*AXS/np.pi*(periods)[:,None], axis=1) / np.sum(C, axis=1)

    time_delays = sp.ndimage.filters.gaussian_filter1d(time_delays, sigma=1.0)
    time_delays_max = sp.ndimage.filters.gaussian_filter1d(time_delays_max, sigma=1.0)
    time_delays_min = sp.ndimage.filters.gaussian_filter1d(time_delays_min, sigma=1.0)
    time_delays2 = sp.ndimage.filters.gaussian_filter1d(time_delays2, sigma=1.0) 
    time_delays2f = sp.ndimage.filters.gaussian_filter1d(time_delays2f, sigma=1.0)
    print 'second method'

    # MCA0, MCSD0= wp.mean_circular_angle(A0S, deg=False, weights=None)
    # MCA1, MCSD1= wp.mean_circular_angle(A1S, deg=False, weights=None)
    # AX_s =A0S-A1S
    # phasediff=MCA0 - MCA1
    # mean_std=(MCSD0+MCSD1)/2
    # time_delays_diff = (phasediff /(2*np.pi))*T
    # time_delays_max_diff = ((phasediff+mean_std)/(2*np.pi))*T
    # time_delays_min_diff = ((phasediff-mean_std)/(2*np.pi))*T
    # time_delays2_diff = np.sum(C*AX_s/(2*np.pi)*(periods)[:,None], axis=0) / np.sum(C, axis=0)
    # time_delays2_difff = np.sum(C*AX_s/(2*np.pi)*(periods)[:,None], axis=1) / np.sum(C, axis=1)

    # time_delays_diff = sp.ndimage.filters.gaussian_filter1d(time_delays_diff, sigma=1.0)
    # time_delays_max_diff = sp.ndimage.filters.gaussian_filter1d(time_delays_max_diff, sigma=1.0)
    # time_delays_min_diff = sp.ndimage.filters.gaussian_filter1d(time_delays_min_diff, sigma=1.0)
    # time_delays2_diff = sp.ndimage.filters.gaussian_filter1d(time_delays2_diff, sigma=1.0)
    # time_delays2_difff = sp.ndimage.filters.gaussian_filter1d(time_delays2_difff, sigma=1.0)
    # print 'time delay done'

    peak_max = max(time_delays)
    peak_min = min(time_delays)

    print 'listed time delays'
    peak_error        .append((peak_min,peak_max))
    time_delay_first  .append((time_delays,time_delays2,time_delays2f,time_delays_max,time_delays_min))
    # time_delay_second .append((time_delays_diff,time_delays2_diff,time_delays2_difff,time_delays_max_diff,time_delays_min_diff))

    print 'done'
    
   


plt.figure()
plt.plot(times,time_delay_first[0][1]*1000,'r')
plt.plot(times,time_delay_first[1][1]*1000,'b')
plt.plot(times,time_delay_first[2][1]*1000,'y')
plt.plot(times,time_delay_first[3][1]*1000,'g')
plt.legend()
# plt.figure()
# plt.plot(times,time_delay_second[0][1]*1000,'r')
# plt.plot(times,time_delay_second[1][1]*1000,'b')
# plt.plot(times,time_delay_second[2][1]*1000,'y')
# plt.plot(times,time_delay_second[3][1]*1000,'g')
# plt.figure()
# plt.plot(periods,time_delay_second[0][2]*1000,'r',label='$\sigma=1$')
# plt.plot(periods,time_delay_second[1][2]*1000,'b',label='$\sigma=2$')
# plt.plot(periods,time_delay_second[2][2]*1000,'y',label='$\sigma=4$')
# plt.plot(periods,time_delay_second[3][2]*1000,'g',label='$\sigma=8$')
# plt.legend()
plt.figure()
plt.plot(periods,time_delay_first[0][2]*1000,'r',label='$\sigma=1$')
plt.plot(periods,time_delay_first[1][2]*1000,'b',label='$\sigma=2$')
plt.plot(periods,time_delay_first[2][2]*1000,'y',label='$\sigma=4$')
plt.plot(periods,time_delay_first[3][2]*1000,'g',label='$\sigma=8$')
plt.legend()
plt.show()
# plt.figure()
# plt.plot(times,time_delay_first[0][1]*1000,'r',label='$\sigma=1$')
# plt.plot(times,time_delay_first[1][1]*1000,'b',label='$\sigma=2$')
# plt.plot(times,time_delay_first[2][1]*1000,'y',label='$\sigma=4$')
# plt.plot(times,time_delay_first[3][1]*1000,'g',label='$\sigma=8$')
# plt.legend()
