function [aa,period,per]=abc_new(dt,exp1,exp2,inter)
% dt = sampling step for the y record to be transformed
% exp1,exp2 = scale range from 2^exp1 to 2^exp2
% inter = scale resolution 2^exp1/inter,..., 2^(exp1+inter-1)/inter
% example for exp1=1, exp2=10, inter=80
% *******************************************
% scales will range FROM 2^1/80,..., 2^(1+80-1)/80 = 2^1/80,...,2^1
% TO 2^10/80,..., 2^(10+80-1)/80 = 2^10/80,...,2^10
clear aa;
j=0;
k0=5.4;
for m=exp1:exp2-1;
   jj=inter-1;
   for n=0:jj;
      a=2^(m+n/inter);
      j=j+1;
      aa(j)=a;
   end;
end;
a=2^exp2;
aa(j+1)=a;
omega0=1/2*(k0./aa+sqrt(2+k0*k0)./aa);
period=1./omega0*2*pi;
aa=aa';
period=period';
per=period*dt;
disp('WAVELET FUNCTIONS DEFINITION');
disp('*****************************');
disp(['Total number of scales ' num2str(length(aa))])
disp(['1st scale 2^' num2str(exp1)])
disp(['Last scale 2^' num2str(exp2)])
disp(['Scale resolution 2^(exp/' num2str(inter) ')'])
disp(['Lower period = ' num2str(per(1))]);
disp(['Upper period = ' num2str(per(end))]);
disp('Strike a key to continue '); 
pause



  



