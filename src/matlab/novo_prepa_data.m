function [newstep,time,new_record1,new_record2,age1,record1,age2,record2,workpath,inpath,outpath]=...
    novo_prepa_data(iscrossw);
clf;
close all;
disp('DATA PREPARATION FOR WAVELET ANALYSIS')
disp('*************************************')
novo_chemin;
disp(workpath)
disp(inpath)
iscrossw=1;
if iscrossw==1
disp('DATA RECORD 1')
disp('*************')
disp('  => File name must be age_record1.txt');
disp('  => It must contain 2 columns: 1) age (!!!! kyr !!!!) 2) record1 data');
disp('Age scale must be in kyr ')
disp('*************************')

filename='age_record1.txt';	
filein=[inpath filename];
data=load(filein);
age1=data(:,2);		
record1=data(:,3);	 

disp('DATA RECORD 2')
disp('*************')
disp('  => File name must be age_record2.txt');
disp('  => It must contain 2 columns: 1) age (!!!! kyr !!!!) 2) record2 data');
disp('Age scale must be in kyr ')
disp('*************************')

filename='age_record2.txt';	
filein=[inpath filename];
data=load(filein);
age2=data(:,2);		
record2=data(:,3);

% Search for the common age interval
minAge=max([age2(1),age1(1)]);
maxAge=min([age2(end),age1(end)]);

if (minAge > maxAge)
    disp('No common age interval for the 2 records')
    stop
end

% If index problem (2 equal ages) 
for ii=1:length(age1)
ind=find(age1(1:ii-1)==age1(ii));
if ~isempty(ind)
    disp('PB WITH AGE SCALE OF RECORD1')
    disp(['2 equal ages for index = ' num2str(ii)])
    pause
end   
ind=find(age1(ii+1:end)==age1(ii));
if ~isempty(ind)
    disp('PB WITH AGE SCALE OF RECORD1')
    disp(['2 equal ages for index = ' num2str(ii)])
    pause
end
end

for ii=1:length(age2)
ind=find(age2(1:ii-1)==age2(ii));
if ~isempty(ind)
    disp('PB WITH AGE SCALE OF RECORD2')
    disp(['2 equal ages for index = ' num2str(ii)])
    pause
end   
ind=find(age2(ii+1:end)==age2(ii));
if ~isempty(ind)
    disp('PB WITH AGE SCALE OF RECORD2')
    disp(['2 equal ages for index = ' num2str(ii)])
    pause
end
end


disp('RE-SAMPLING THE DATA ON A COMMON SCALE')
disp('**************************************')


disp('=> "ENTER" to use as new sampling step the mean age step')
disp('=> "1" to enter a different sampling step ')
rep=input( '        => answer?  ');
disp(' ')
if isempty(rep)
    %Mean age step search
    dim=length(age1);
    dim=dim-1;
    agestep1(1:dim)=age1(2:dim+1)-age1(1:dim);
    mean_agestep1=mean(agestep1);
    disp(['Mean age step for record1 is ',num2str(mean_agestep1)])

    dim=length(age2);
    dim=dim-1;
    agestep2(1:dim)=age2(2:dim+1)-age2(1:dim);
    mean_agestep2=mean(agestep2);
    disp(['Mean age step for record2 is ',num2str(mean_agestep2)])

disp(' ')
disp('  ** "1" for the largest mean age step')
disp('  ** "2" for the lowest mean age step')
whichstep=input('        => answer?  ')
    if whichstep==1
            newstep=max([mean_agestep1 mean_agestep2])
    else
            newstep=min([mean_agestep1 mean_agestep2])
    end
else
    newstep=input('Enter new sampling step ("0.25" for 0.25kyr) => ');
end
time=[minAge:newstep:maxAge];

new_record1=interp1(age1,record1,time,'PCHIP');
new_record2=interp1(age2,record2,time,'PCHIP');

disp('"ENTER" to look at re-sampled records ');
disp('"1" else');
rep=input( '        => answer?  ');
if isempty(rep)
figure(1)
subplot(2,1,1)
plot(age2,record2,'b-')
hold on;
plot(time,new_record2,'c--')
ylabel('Record2')
xlim([minAge maxAge])
legend('original','re-sampled');

subplot(2,1,2)
plot(age1,record1,'r-')
hold on;
plot(time,new_record1,'y--')
ylabel('Record1')
xlabel('Age (kyr BP)')
xlim([minAge maxAge])
legend('original','re-sampled');
disp('Strike a key to continue ');
pause  
clf;
end

cc_record=corrcoef(new_record1,new_record2);

disp('Correlation coefficient between record1/record2 after re-sampling: ')
disp(cc_record)
disp('Strike a key to continue ');
pause

else %iscrossw==1
    
disp('DATA RECORD')
disp('***********')
disp('  => File name must be age_record1.txt');
disp('  => It must contain 2 columns: 1) age (!!!! kyr !!!!) 2) record1 data');
disp('Age scale must be in kyr ')
disp('*************************')

filename='age_record1.csv';	
filein=[inpath filename];
data=load(filein);
age1=data(:,1);		
record1=data(:,2);	

%not useful; just to avoid huge modification in the program
age2=age1;		
record2=record1;

% If index problem (2 equal ages) 
for ii=1:length(age1)
    ind=find(age1(1:ii-1)==age1(ii));
    if ~isempty(ind)
    disp('PB WITH AGE SCALE OF RECORD1')
    disp(['2 equal ages for index = ' num2str(ii)])
    pause
    end   
    ind=find(age1(ii+1:end)==age1(ii));
    if ~isempty(ind)
    disp('PB WITH AGE SCALE OF THE RECORD')
    disp(['2 equal ages for index = ' num2str(ii)])
    pause
    end
end

disp('RE-SAMPLING THE DATA ON A REGULAR GRID')
disp('**************************************')

disp('=> "ENTER" to use as new sampling step the mean age step')
disp('=> "1" to enter a different sampling step ')
rep=input( '        => answer?  ');
disp(' ')

if isempty(rep)
    %Mean age step search
    dim=length(age1);
    dim=dim-1;
    agestep1(1:dim)=age1(2:dim+1)-age1(1:dim);
    newstep=mean(agestep1);
    disp(['Mean age step for the record is ',num2str(newstep)])
else
    newstep=input('Enter new sampling step ("0.25" for 0.25kyr) => ');
end
minAge=age1(1);
maxAge=age1(end);

time=[minAge:newstep:maxAge];

new_record1=interp1(age1,record1,time,'pchip');
new_record2=interp1(age2,record2,time,'pchip');

figure(1)
plot(age1,record1,'r-')
hold on;
plot(time,new_record1,'y--')
ylabel('Record')
xlabel('Age (kyr BP)')
xlim([minAge maxAge])
legend('original','re-sampled');
disp('Strike a key to continue ');
pause  
clf;

    
end


