clear all;
clf;
close all;

iscrossw=0;
%input series are prepared in "prepa_data.m"
[newstep,time,new_record1,new_record2,age1,record1,age2,record2,workpath,inpath,outpath]=...
    novo_prepa_data(iscrossw);
dt=newstep;


    yage=age1;
    yrec=record1;
    y=new_record1(:);
    disp('"1" figure with error envelop')
    disp('"enter" else')
    error_process=input(' => type your answer ')
    if ~isempty(error_process)
        relerr=input('type relative uncertainty - 0.015 for 1.5% - ')
        yrec_sup=(1+relerr)*record1;
        yrec_inf=(1-relerr)*record1;
    end   

  
    clear age1;
    clear record1;
    clear new_record1;
    
    
X1=min(time);
X2=max(time);
%Scales interval definition:
%from 2^exp1 to 2^exp2 with a 2^exp/inter resolution
exp1=1;
exp2=10; %exp2=3;
inter=80; %inter=200;

%Wavelets are built in "abc_fun.m"
[aa,period,per]=abc_new(dt,exp1,exp2,inter);

%The delay and wavelets and cross-wavelet transform are computed in "retard_fun.m"
n_data=length(y);
[wave,J,n1,y]=conv_fft_abc_new(dt,y,aa);
zcw1=abs(wave);

disp('WAVELET PLOT');
disp('************');
ylab={};
yTck=[];
disp('Strike ')
disp('  => "ENTER" to plot the 23,41 and 100kyr period on a 1 to 130kyr Y-axis')
disp('  => "1" to define your own values ')
chxper=input(' => answer? ');
if ~isempty(chxper)
    go_on=[];
    ii=1;
    while isempty(go_on)
    P(ii)=input(['Type the period you wish to plot (nb ' num2str(ii) '): ']);
    [temp,INDper(ii)]=min(abs(per-P(ii)));
    ylab(ii)={num2str(P(ii))};
    yTck=[yTck;INDper(ii)];
    ii=ii+1;
    go_on=input('"ENTER" to display one more period else strike "1" ');
    end
    %Les bornes pour l'axe y des periodes
    Upper=input(['Type the upper period on Y axis (ex: 130 for 130kyr)']);
    Lwper=input(['Type the lower period on Y axis (ex: 1 for 1kyr) ']);
    [temp,iup]=min(abs(per-Upper));
    [temp,ilw]=min(abs(per-Lwper));
else
    P(1)=23;
    P(2)=41;
    P(3)=100;
    Upper=140;
    Lwper=10;
    for ii=1:3
    ylab(ii)={num2str(P(ii))};
    [temp,INDper(ii)]=min(abs(per-P(ii)));
    yTck=[yTck;INDper(ii)];
    end
    [temp,iup]=min(abs(per-Upper));
    [temp,ilw]=min(abs(per-Lwper));

end
    

if iup<ilw
    temp=iup;
    iup=ilw;
    ilw=temp;
end
clear temp;

zmax=-Inf;
zmin=Inf;
for jj=ilw:iup
    iszmax=max(zcw1(:,jj));
    if iszmax>zmax
    zmax=iszmax;
    end
end
for jj=ilw:iup
    iszmin=min(zcw1(:,jj));
    if iszmin<zmin
    zmin=iszmin;
    end
end

yreverse=input('"ENTER" for Y-axis in reverse order else strike "1" ');

figure(1)

subplot(2,1,1)
plot(yage,y,'r')
if isempty(yreverse)
set(gca,'YDir','reverse')
end
ylabel('Standardized data','FontSize',12);
%text(10,30,'data re-built with wavelet coefficients')

subplot(2,1,2)
dim=length(zcw1(:,1));
contourf(time,[1:dim],zcw1(1:dim,:))
view(0,-90)
xlabel('Time (kyr BP)','FontSize',12);
set(gca,'XLim',[X1 X2],'YLim',[ilw iup])
set(gca,'yTick',yTck,'yTickLabel',ylab)
ylabel('Period (kyr)','FontSize',12);

figure(2)
subplot(2,1,1)
plot(yage,yrec,'r')
set(gca,'XLim',[X1 X2])
if isempty(yreverse)
set(gca,'YDir','reverse')
end
%set(gca,'xTick',yTck,'xTickLabel',[])
ylabel('Original data','FontSize',12);

subplot(2,1,2)
dim=length(zcw1(:,1));
contourf(time,period, zcw1(:,1:dim))
view(0,-90)
xlabel('Time (kyr BP)','FontSize',12);
set(gca,'XLim',[X1 X2],'YLim',[ilw iup])
set(gca,'yTick',yTck,'yTickLabel',ylab)
ylabel('Period (kyr)','FontSize',12);


if error_process
figure(3)
subplot(2,1,1)
hl1 = line(yage,yrec,'Color','r');
hold on;
hl1_sup = line(yage,yrec_sup,'Color','r');
hl1_inf = line(yage,yrec_inf,'Color','r');
hold off;
ax1 = gca;
set(ax1,'XColor','r','YColor','r')
if isempty(yreverse)
set(ax1,'YDir','reverse')
end
set(ax1,'Color','none')
       set(ax1,'XLim',[X1 X2])
%ylabel('Standardized original data','FontSize',12);
%text(10,30,'Re-sampled data')

subplot(2,1,2)
dim=length(zcw1(:,1));
contourf(time(:),[1:dim],zcw1(1:dim,:))
view(0,-90)
xlabel('Time (kyr BP)','FontSize',12);
set(gca,'XLim',[X1 X2],'YLim',[ilw iup])
set(gca,'yTick',yTck,'yTickLabel',ylab)
ylabel('Period (kyr)','FontSize',12);
end

disp('You can save the figures using local graphic menu "save" or "print"')