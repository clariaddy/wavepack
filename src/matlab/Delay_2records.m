clear;
clf;
close all;

disp('TIME DELAY WITH CROSS-WAVELETS TRANSFORM')
disp('****************************************')

%input series are prepared in "prepa_data.m"
[newstep,time,new_record1,new_record2,age1,record1,age2,record2,workpath,inpath,outpath]= novo_prepa_data;
dt=newstep;

disp('"1" for cross-wavelet between record1 and record2')
disp('"2" for cross-wavelet between record1 and -record2')
rep=input(' => type your choice ')
if (rep==1)
    y1=new_record1(:);
    y2=new_record2(:);
elseif (rep==2)
    y1=new_record1(:);
    y2=-new_record2(:);  
end

%Scales interval definition:
%from 2^exp1 to 2^exp2 with a 2^exp/inter resolution
exp1=1;
exp2=10;    %exp2=3;
inter=80;   %inter=200;


%Wavelets are built in "abc_fun.m"
[aa,period,per]=abc_new(dt,exp1,exp2,inter);

n_data=length(y1);

disp('Band-filter options')
disp('*******************')
Upper=input('TYPE the lower limit (kyr) for the band filter (ex: "36" for a 41k-band filter) => ');
Lwer=input('TYPE its upper limit (kyr) => ');

disp('Your choice is:')
disp(['UPPER PERIOD = ' num2str(Lwer)])
disp(['LOWER PERIOD = ' num2str(Upper)])
go_on=input('"ENTER" if it''s OK "1" to change ');
if ~isempty(go_on)
    Lwer=input('UPPER PERIOD? ');
    Upper=input('LOWER PERIOD? ');
end    
[temp,iup]=min(abs(per-Upper));
[temp,ilw]=min(abs(per-Lwer));
%iup
%ilw
aa=aa(iup:ilw);
per=per(iup:ilw);

%The wavelet and cross-wavelet transforms are performed from 36.9 and 46.3
[cross_wave,delay,t,convolution1,convolution2,y1,y2]=retard_new(dt,y1,y2,aa,per);
n_dilat=length(aa);
% y1 and y2 filtered in the [lower,upper] band and standardized
y1_filtr=sum(real(convolution1(1:n_dilat,:)));
y1_filtr=(y1_filtr-mean(y1_filtr))/std(y1_filtr);
y2_filtr=sum(real(convolution2(1:n_dilat,:)));
y2_filtr=(y2_filtr-mean(y2_filtr))/std(y2_filtr);


disp('GRAPHIC OUTPUT')
disp('**************')
X1=min(time);
X2=max(time);
disp('X-axis limits on graphics will be: ')
disp(['Xmin= ' num2str(X1) ' - Xmax= ' num2str(X2)])
disp('=> "ENTER" to accept this option')
disp('=> "1" to change')
rep=input( '        => answer?  ');
if ~isempty(rep)
    X1=input('Type Xmin= ');
    X2=input('Type Xmax= ');
end

    figure(1)
    subplot(2,1,1);
    plot(time,y1_filtr);
    hold on;
    plot(time,y2_filtr,'r');
    axis([X1 X2 -Inf Inf]);
    ylabel('Standardized data','FontSize',12);
    text(10,30,'re-built data with wavelet coefficients','FontSize',8)
    
    subplot(2,1,2);
    plot(time,delay);
    axis([X1 X2 -Inf Inf]);
    ylabel('Time delay (kyr)','FontSize',12);
    xlabel('Time (kyr BP)','FontSize',12);
    
    figure(2)
    subplot(2,1,1);
    plot(time,y1);
    hold on;
    plot(time,y2,'r');
    axis([X1 X2 -Inf Inf]);
    ylabel('Standardized data','FontSize',12);
    text(10,30,'original data','FontSize',8)
    
    subplot(2,1,2);
    plot(time,delay);
    axis([X1 X2 -Inf Inf]);
    ylabel('Time delay (kyr)','FontSize',12);
    xlabel('Time (kyr BP)','FontSize',12);
    
disp('You can save the figures using local graphic menu "save" or "print"')

disp('ASCII FILE OUTPUT')
disp('*****************')
filename=['Delay_filterRecord_' num2str(Lwer) '-' num2str(Upper)];
filin=[outpath filename '.txt']
disp(filin)
data = [time(:),y1_filtr(:),y2_filtr(:),delay(:)];
save(filin,'data','-ascii');

disp('Correlation coefficient')
disp('1) filtered record1 and record2');
ccV=corrcoef(y1_filtr,y2);
disp(ccV)
disp('2) record1 and filtered record2');
ccV=corrcoef(y1,y2_filtr);
disp(ccV)
disp('3) filtered record1 and filtered record2');
ccV=corrcoef(y1_filtr,y2_filtr);
disp(ccV)

