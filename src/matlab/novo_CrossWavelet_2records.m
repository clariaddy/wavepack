clear all;
clf;
close all;

iscrossw=1;
%input series are prepared in "prepa_data.m"
[newstep,time,new_record1,new_record2,age1,record1,age2,record2,workpath,inpath,outpath]=novo_prepa_data(iscrossw);
dt=newstep;

disp('"1" for cross-wavelet between record1 and record2')
disp('"2" for cross-wavelet between record1 and -record2')
rep=input(' => type your choice ')
if (rep==1)
    y1=new_record1(:);
    y2=new_record2(:);
elseif (rep==2)
    y1=new_record1(:);
    y2=-new_record2(:);  
end

X1=min(time);
X2=max(time);

%Scales interval definition:
%from 2^exp1 to 2^exp2 with a 2^exp/inter resolution
exp1=1;
exp2=10; %exp2=3;
inter=80; %inter=200;

%Wavelets are built in "abc_fun.m"
[aa,period,per]=abc_new(dt,exp1,exp2,inter);

%The delay and wavelets and cross-wavelet transform are computed in "retard_fun.m"
n_data=length(y1);
[cross_wave,delay,t,convolution1,convolution2,y1,y2]=retard_new(dt,y1,y2,aa,per);
zcw1=abs(cross_wave);

%The signals are re-built and standardized
n_dilat=length(aa);
y1_filtr=sum(real(convolution1(1:n_dilat,:)));
y1_filtr=(y1_filtr-mean(y1_filtr))/std(y1_filtr);
y2_filtr=sum(real(convolution2(1:n_dilat,:)));
y2_filtr=(y2_filtr-mean(y2_filtr))/std(y2_filtr);

disp('CROSS-WAVELET PLOT');
disp('******************');
ylab={};
yTck=[];
disp('Strike ');
disp('  => "ENTER" to plot the 23,41 and 100kyr period on a 1 to 130kyr Y-axis');
disp('  => "1" to define your own values ');
chxper=input(' => answer? ');
if ~isempty(chxper)
    go_on=[];
    ii=1;
    while isempty(go_on)
    P(ii)=input(['Type the period you wish to plot (nb ' num2str(ii) '): ']);
    [temp,INDper(ii)]=min(abs(per-P(ii)));
    ylab(ii)={num2str(P(ii))};
    yTck=[yTck;INDper(ii)];
    ii=ii+1;
    go_on=input('"ENTER" to display one more period else strike "1" ');
    end
    %Les bornes pour l'axe y des periodes
    Upper=input(['Type the upper period on Y axis (ex: 130 for 130kyr) ']);
    Lwper=input(['Type the lower period on Y axis (ex: 1 for 1kyr) ']);
    [temp,iup]=min(abs(per-Upper));
    [temp,ilw]=min(abs(per-Lwper));
else
    P(1)=23;
    P(2)=41;
    P(3)=100;
    Upper=140;
    Lwper=10;
    for ii=1:3
    ylab(ii)={num2str(P(ii))};
    [temp,INDper(ii)]=min(abs(per-P(ii)));
    yTck=[yTck;INDper(ii)];
    end
    [temp,iup]=min(abs(per-Upper));
    [temp,ilw]=min(abs(per-Lwper));

end
    

if iup<ilw
    temp=iup;
    iup=ilw;
    ilw=temp;
end
clear temp;

zmax=-Inf;
zmin=Inf;
for jj=ilw:iup
    iszmax=max(zcw1(jj,1:end));
    if iszmax>zmax
    zmax=iszmax;
    end
end
for jj=ilw:iup
    iszmin=min(zcw1(jj,1:end));
    if iszmin<zmin
    zmin=iszmin;
    end
end

figure(1)

subplot(2,1,1)
plot(time,y1);
hold on;
plot(time,y2,'r');
axis([X1 X2 -Inf Inf])
ylabel('Standardized data','FontSize',12);
%text(10,30,'Re-sampled data')

subplot(2,1,2)
dim=length(zcw1(:,1));
contourf(time(:),[1:dim],zcw1(1:dim,:))
view(0,-90)
xlabel('Time (kyr BP)','FontSize',12);
set(gca,'XLim',[X1 X2],'YLim',[ilw iup])
set(gca,'yTick',yTck,'yTickLabel',ylab)
ylabel('Period (kyr)','FontSize',12);

figure(2)

subplot(2,1,1)
plot(time,y1_filtr);
hold on;
plot(time,y2_filtr,'r');
axis([X1 X2 -Inf Inf])
ylabel('Standardized data','FontSize',12);
disp('LABEL "data re-built with wavelet coefficients" on fig.2 ? ');
figlab=input('  => "ENTER" to put the label, "1" else ');
if isempty(figlab)
    figure(2);
    gtext('data re-built with wavelet coefficients')
else
    figure(2);
end

subplot(2,1,2)
dim=length(zcw1(:,1));
contourf(time(:),[1:dim],zcw1(1:dim,:))
view(0,-90)
xlabel('Time (kyr BP)','FontSize',12);
set(gca,'XLim',[X1 X2],'YLim',[ilw iup])
set(gca,'yTick',yTck,'yTickLabel',ylab)
ylabel('Period (kyr)','FontSize',12);


disp('You can save the figures using local graphic menu "save" or "print"')