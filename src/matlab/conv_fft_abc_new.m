function [wave,J,n1,y]=conv_fft_abc_new(dt,y,aa)
%y: record of interest
%n1: original dimension of the y serie before padding
%dt: y sampling step
%aa: wavelet scales (output of "abc_new.m")
%wave: wavelet transform of y record
% J,n1: dimension of the array wave = scale,time
disp('A WAVELET TRANSFORM IS NOW PERFORMED')
disp('************************************')
load age_record1.txt
y=age_record1(:,3);
y=y';
% il faut absolument normaliser sinon il y a un escalier entre
% l'ondelette de moyenne nulle et le saut du signal de 0 a ...
% aux deux bords!!!!
y=(y-mean(y))/std(y);
k0=5.4;
%
dt=1;
n1=length(y);
%on construit x par zero-padding a partir de y
%on cherche base2 tel que 2^base2 > n1 la longueur de la serie y
base2=fix(log(n1)/log(2)+0.4999);
if(2^base2-n1 < 0) base2=base2+1;
   end;
x=[y,zeros(1,2^base2-n1)];
y=y';
n=length(x);
disp(['Original length of the input record: ' num2str(n1)])
disp(['New length after "zero-padding" : ' num2str(n)])
% on construit une "phase" symetrique p/r a 0
% de type [0,1,...,p,-(p-1),-(p-2),...,1]*(2*pi)/(n*dt) avec p=E(n/2)
k=[1:fix(n/2)];
k=k.*((2.*pi)/(n*dt));
k=[0., k, -k(fix((n-1)/2):-1:1)];
% Variable "f" FFT du signal y en entree apres padding
f=fft(x);
%aa st les echelles d''interet definies en sortie de abc.m
[aa, period, per] = abc_new(1.0, 1, 2, 3)
scale=aa;
J=length(aa);
%wave: sortie trnsformee en ondelettes de y
wave=zeros(J,n);
wave=wave+sqrt(-1)*wave;
%
nn=length(k);
for a1=1:J;
expnt=-(scale(a1).*k - k0).^2/2.*(k > 0.);
norm=sqrt(scale(a1)*k(2))*(pi^(-0.25))*sqrt(nn);
daughter=norm*exp(expnt);
daughter=daughter.*(k>0.);
wave(a1,:)=ifft(f.*daughter)/sqrt(scale(a1)); %FFT INVERSE du produit des TF(ondelettes filles).TF(y)
% Ce qui donne un produit de convolution dans l'espace des tps?
end;
[p,q]=size(wave);
%disp(['Taille intermediaire de la transformee en ondelettes de y: ' num2str(p) 'X' num2str(q)]);
wave=real(wave(1:J,1:n1));
disp(['Size of the wavelet transform array: ' num2str(J) 'X' num2str(n1)]);
[p,q]=size(daughter);
%disp(['Taille de TF(ondelettes daughters) : ' num2str(p) 'X' num2str(q)]);
disp('Strike a key to continue '); 
pause



  



