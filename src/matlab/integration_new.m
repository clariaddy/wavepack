function [cross_wave,t,phase]=integration_new(convolution1,convolution2,period,n1)
clear w ww c wc z t im re phase brol truc;  
clear AAAA BBBB delay_time_period;
cross_wave=convolution1.*conj(convolution2);
%%%cross_wave=convolution1.*convolution2;
frequence=1./period;
c=abs(cross_wave);
%
%c=c.*c;
%
for i=1:n1;
   brol(i)=sum(frequence'*c(:,i));
   truc(i)=sum(c(:,i));
end;
t=brol./truc;
t=1./t;
for j=1:n1;
   re(j)=sum(real(cross_wave(:,j)));
   im(j)=sum(imag(cross_wave(:,j)));
end;
phase=atan2(im,re);
AAAA=angle(cross_wave);
BBBB=diag(period);
delay_time_period=BBBB*AAAA/(2*pi);

   
   




  



